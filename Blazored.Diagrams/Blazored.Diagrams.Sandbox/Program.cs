using System.Reflection;
using Blazored.Diagrams;
using Blazored.Diagrams.Sandbox;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(_ => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddScoped<HtmlRenderer>();
builder.Services.AddBlazoredDiagrams(options =>
{
    options.SerializerOptions.Assemblies.Add(Assembly.GetExecutingAssembly());
    options.SerializerOptions.JsonSerializerOptions.WriteIndented = false;
    options.SerializerOptions.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
});
builder.Services.AddMudServices();
await builder.Build().RunAsync();