using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.Organigram;

public class OrganigramNode : Node<OrganigramNodeComponent>
{
    public string Job = "Unknown";
}