using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Links;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.ResultNode;

public class ResultNodeBehaviour : IBehaviour
{
    private readonly ResultInputPort _port;
    private OperatorNode.OperatorNode? _operatorNode;

    public ResultNodeBehaviour(ResultInputPort port)
    {
        _port = port;
        _port.OnIncomingLinkAdded += OnIncomingLinkAdded;
        _port.OnIncomingLinkRemoved += OnIncomingLinkRemoved;
    }

    private void OnIncomingLinkRemoved(ILink arg1)
    {
        if (_operatorNode != null && _operatorNode.OutputPort.Id.Equals(arg1.SourcePort.Id))
        {
            UnsubscribeFromNode();
            if (_port.Parent != null) _port.Parent.NumberOutput = null;
            _port.NotifyRedraw();
        }
    }

    private void UnsubscribeFromNode()
    {
        if (_operatorNode != null) _operatorNode.OnNumberChanged -= OnResultChanged;
    }

    private void OnIncomingLinkAdded(ILink arg1)
    {
        if (arg1.SourcePort.Parent is OperatorNode.OperatorNode opNode)
        {
            _operatorNode = opNode;
            opNode.OnNumberChanged += OnResultChanged;
            _port.NotifyRedraw();
        }
    }

    private void OnResultChanged(decimal? number)
    {
        if (_port.Parent != null) _port.Parent.NumberOutput = number;
        _port.NotifyRedraw();
    }

    public void Dispose()
    {
        UnsubscribeFromNode();
        _port.OnIncomingLinkAdded -= OnIncomingLinkAdded;
        _port.OnIncomingLinkRemoved -= OnIncomingLinkRemoved;
    }

    public bool IsEnabled { get; set; } = true;
}