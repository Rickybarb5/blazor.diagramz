using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.NumberNode;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.ResultNode;

public class ResultNode : Node<ResultNodeComponent>, INumberOutput
{
    public ResultInputPort InputPort => (ResultInputPort)Ports.First(x => (x.GetType() == typeof(ResultInputPort)));
    private decimal? _numberOutput;

    public decimal? NumberOutput
    {
        get => _numberOutput;
        set
        {
            _numberOutput = value;
            NotifyNumberChanged();
        }
    }

    public event Action<decimal?>? OnNumberChanged;

    public void NotifyNumberChanged()
    {
        OnNumberChanged?.Invoke(NumberOutput);
    }

    public ResultNode()
    {
        var inputPort = new ResultInputPort
        {
            Position = PortPosition.Left,
            Alignment = PortAlignment.Center,
        };

        var exitPort = new NumberNodeOutputPort()
        {
            Position = PortPosition.Right,
            Alignment = PortAlignment.Center,
        };

        Ports.Add(inputPort);
        Ports.Add(exitPort);
        Behaviours.Add(new ResultNodeBehaviour(inputPort));
    }
}