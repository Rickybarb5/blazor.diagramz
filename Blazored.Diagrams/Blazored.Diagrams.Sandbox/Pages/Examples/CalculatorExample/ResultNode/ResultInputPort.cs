using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.ResultNode;

public class ResultInputPort : Port<DefaultPortComponent>
{
    private ResultNode? _portParent;

    public new ResultNode? Parent
    {
        get => _portParent;
        set
        {
            if (value != _portParent)
            {
                _portParent = value;
                NotifyPortParentChanged();
            }

            RefreshPosition();
        }
    }

    public override bool CanCreateLink()
    {
        return false;
    }

    public override bool CanConnectTo(IPort port)
    {
        if (port.IncomingLinks.Count == 0 && port is OperatorNode.OperatorNode)
        {
            return true;
        }

        return base.CanConnectTo(port);
    }
}