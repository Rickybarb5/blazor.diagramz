namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.OperatorNode;

public enum Operator
{
    Add,
    Subtract,
    Multiply,
    Divide,
}