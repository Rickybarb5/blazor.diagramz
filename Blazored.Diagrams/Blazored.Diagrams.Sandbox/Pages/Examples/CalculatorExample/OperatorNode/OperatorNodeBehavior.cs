using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Links;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.OperatorNode;

public class OperatorNodeBehavior : IBehaviour
{
    private readonly OperatorNode _operatorNode;

    public OperatorNodeBehavior(OperatorNode operatorNode)
    {
        _operatorNode = operatorNode;
        _operatorNode.OnOperatorChanged += OnOperatorChanged;
        EnableBehaviour();
    }


    private void OnOperatorChanged(Operator obj)
    {
        Calculate();
    }

    private void Calculate()
    {
        var numbers = _operatorNode.InputPort
            .IncomingLinks
            .Select(x => x.SourcePort.Parent)
            .OfType<INumberOutput>()
            .Select(x => x.NumberOutput)
            .Where(x => x.HasValue)
            .ToList();
        Calculate(numbers);
    }

    private void OnLinkAttached(ILink arg1)
    {
        if (arg1.SourcePort.Parent is INumberOutput nn)
        {
            nn.OnNumberChanged += Calculate;
            Calculate();
        }

        _operatorNode.NotifyRedraw();
    }

    private void OnLinkDetached(ILink arg1)
    {
        if (arg1.SourcePort.Parent is INumberOutput nn)
        {
            nn.OnNumberChanged -= Calculate;
            Calculate();
        }

        _operatorNode.NotifyRedraw();
    }

    private void Calculate(decimal? obj)
    {
        Calculate();
    }

    private void Calculate(List<decimal?> numbers)
    {
        if (numbers.Count < 2)
        {
            return;
        }

        try
        {
            var operationResult = _operatorNode.Operator switch
            {
                Operator.Add => numbers.Sum(),
                Operator.Subtract => numbers.Aggregate((total, next) => total - next),
                Operator.Multiply => numbers.Aggregate((total, next) => total * next),
                Operator.Divide => numbers.Aggregate((total, next) => (total / next)),
                _ => throw new ArgumentOutOfRangeException(nameof(_operatorNode.Operator))
            };
            _operatorNode.NumberOutput = operationResult;
        }
        catch (Exception e)
        {
            _operatorNode.NotifyError(e.Message);
        }

        _operatorNode.NotifyRedraw();
    }

    public (decimal? firstNumber, decimal? secondNumber) GetNumbers()
    {
        var numbers = _operatorNode.InputPort
            .IncomingLinks
            .Select(x => x.SourcePort.Parent)
            .OfType<INumberOutput>()
            .Select(x => x.NumberOutput)
            .ToList();

        return (numbers.ElementAtOrDefault(0), numbers.ElementAtOrDefault(1));
    }

    public void Dispose()
    {
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
    }

    private void EnableBehaviour()
    {
        _operatorNode.InputPort.OnIncomingLinkAdded += OnLinkAttached;
        _operatorNode.InputPort.OnIncomingLinkRemoved += OnLinkDetached;
        Calculate();
    }
}