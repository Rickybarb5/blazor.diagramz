using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.OperatorNode;

public class OperatorInputPort : Port<DefaultPortComponent>
{
    public override bool CanConnectTo(IPort port)
    {
        return port.Parent is INumberOutput && IncomingLinks.Count < 2;
    }

    public override bool CanCreateLink()
    {
        return false;
    }
}