using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.NumberNode;

public class NumberNodeOutputPort : Port<DefaultPortComponent>
{
    public override bool CanCreateLink()
    {
        return OutgoingLinks.Count < 1;
    }
}