using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.CalculatorExample.NumberNode;

public class NumberNode : Node<NumberNodeComponent>, INumberOutput
{
    public event Action<decimal?> OnNumberChanged;

    private decimal? _numberOutput;

    public decimal? NumberOutput
    {
        get => _numberOutput;
        set
        {
            _numberOutput = value;
            NotifyNumberChanged();
        }
    }

    public void NotifyNumberChanged()
    {
        OnNumberChanged?.Invoke(NumberOutput);
    }

    public NumberNode()
    {
        var exitPort = new NumberNodeOutputPort()
        {
            Position = PortPosition.Right,
        };

        Ports.Add(exitPort);
    }
}