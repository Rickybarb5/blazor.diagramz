using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Nodes;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Sandbox.Pages.Examples.DragAndDrop;

public class DragAndDropBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;
    private bool _isDragging;
    private bool _isEnabled = true;
    private bool _isDragActive;

    public IPosition? Model;

    public DragAndDropBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    public void Dispose()
    {
        DisableBehaviour();
    }

    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value != _isEnabled)
            {
                _isEnabled = value;
                if (value)
                    EnableBehaviour();
                else
                    DisableBehaviour();
            }
        }
    }

    private void EnableBehaviour()
    {
        _diagram.OnPointerMove += OnPointerMove;
        _diagram.OnPointerUp += OnPointerUp;
    }

    private void DisableBehaviour()
    {
        _diagram.OnPointerMove -= OnPointerMove;
        _diagram.OnPointerUp -= OnPointerUp;
    }

    public void StartDrag()
    {
        _isDragActive = true;
    }

    public void EndDrag()
    {
        _isDragActive = false;
        Model = null;
    }

    private void OnPointerMove(IDiagram diagram, PointerEventArgs args)
    {
        if (!_isDragActive || Model is null) return;

        _isDragging = true;
        Model.PositionX = (int)args.OffsetX;
        Model.PositionY = (int)args.OffsetY;
    }

    private void OnPointerUp(IDiagram diagram, PointerEventArgs args)
    {
        if (_isDragging && _isDragActive && Model is not null)
        {
            if (Model is INode node)
            {
                _diagram.AddNode(node);
            }
        }

        _isDragging = false;
        _isDragActive = false;
        Model = null;
    }
}