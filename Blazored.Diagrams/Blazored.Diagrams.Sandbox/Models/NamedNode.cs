using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Sandbox.Components;

namespace Blazored.Diagrams.Sandbox.Models;

public class NamedNode : Node<NamedNodeComponent>
{
    public string Name { get; set; } = string.Empty;
    public string Color { get; set; }
    public string Message { get; set; } = string.Empty;
}