using Blazored.Diagrams.Extensions;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

public class LinkCreationBehaviourTests
{
    [Fact]
    public void Test_PointerDown_StartsLinkCreation()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        node.Ports.Add(sourcePort);
        diagram.AddNode(node);

        // Act
        sourcePort.NotifyPointerDown(new PointerEventArgs());

        //Assert
        Assert.Same(sourcePort, diagram.AllLinks[0].SourcePort);
        Assert.Equal(1, diagram.AllLinks.Count);
    }

    [Fact]
    public void Test_PointerMove_ChangesTargetPosition()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        node.Ports.Add(sourcePort);
        diagram.AddNode(node);
        sourcePort.NotifyPointerDown(new PointerEventArgs());

        // Act
        diagram.NotifyPointerMove(new PointerEventArgs { ClientX = 300, ClientY = 500, Buttons = 1 });

        //Assert
        var expectedX = sourcePort.GetCenterCoordinates().CenterX;
        var expectedY = sourcePort.GetCenterCoordinates().CenterY;
        
        Assert.NotEqual(expectedX, diagram.AllLinks[0].TargetPositionX);
        Assert.NotEqual(expectedY, diagram.AllLinks[0].TargetPositionY);
    }

    [Fact]
    public void Test_PointerUp_On_Port_Creates_Link()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        var targetPort = ObjectFactory.Port;
        node.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        diagram.AddNode(node);
        diagram.AddNode(node2);
        targetPort.SetPosition(500, 500);
        sourcePort.NotifyPointerDown(new PointerEventArgs());
        var expectedX = targetPort.GetCenterCoordinates().CenterX;
        var expectedY = targetPort.GetCenterCoordinates().CenterY;

        diagram.NotifyPointerMove(new PointerEventArgs { ClientX = 300, ClientY = 500, Buttons = 1 });

        // Act
        targetPort.NotifyPointerUp(new PointerEventArgs());

        //Assert
        Assert.Same(targetPort, diagram.AllLinks[0].TargetPort);
        Assert.Equal(expectedX, diagram.AllLinks[0].TargetPositionX);
        Assert.Equal(expectedY, diagram.AllLinks[0].TargetPositionY);
    }

    [Fact]
    public void Test_PointerUp_On_Diagram_Removes_Link()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        node.Ports.Add(sourcePort);
        diagram.AddNode(node);
        sourcePort.NotifyPointerDown(new PointerEventArgs());
        diagram.NotifyPointerMove(new PointerEventArgs { ClientX = 300, ClientY = 500, Buttons = 1 });

        // Act
        diagram.NotifyPointerUp(new PointerEventArgs());

        //Assert
        Assert.Equal(0, diagram.AllLinks.Count);
    }
}