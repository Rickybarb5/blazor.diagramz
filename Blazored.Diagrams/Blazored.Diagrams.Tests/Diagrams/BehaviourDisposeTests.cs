using System.Collections;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Diagrams.Behaviours;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Groups.Behaviours;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Links.Behaviours;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Nodes.Behaviours;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Ports.Behaviours;

namespace Blazored.Diagrams.Test.Diagrams;

public class BehaviourDisposeTests
{
    [Theory]
    [ClassData(typeof(TestDataGenerator))]
    public void Test_Dispose(IBehaviour behaviour)
    {
        TestDispose(behaviour);
    }

    private void TestDispose(IBehaviour behaviour)
    {
        behaviour.Dispose();
    }
}

public class TestDataGenerator : IEnumerable<object[]>
{
    private readonly List<object[]> _data =
    [
        [new DeleteBehaviour(ObjectFactory.Diagram)],
        [new DeleteWithKeyBehaviour(ObjectFactory.Diagram)],
        [new ModelMoveBehaviour(ObjectFactory.Diagram)],
        [new LinkCreationBehavior(ObjectFactory.Diagram)],
        [new ModelSelectBehaviour(ObjectFactory.Diagram)],
        [new PanBehaviour(ObjectFactory.Diagram)],
        [new ZoomBehavior(ObjectFactory.Diagram)],
        [new GroupResizeBehaviour(ObjectFactory.Group)],
        [new GroupEventPipelineBehaviour(ObjectFactory.Group)],
        [new LinkContainerBehaviour(ObjectFactory.Port)],

        [new RedrawBehaviour<IGroup>(ObjectFactory.Group)],
        [new RedrawBehaviour<INode>(ObjectFactory.Node)],
        [new RedrawBehaviour<IPort>(ObjectFactory.Port)],
        [new RedrawBehaviour<ILink>(ObjectFactory.Link)],
        [new RedrawBehaviour<ILayer>(ObjectFactory.Layer)],
        [new RedrawBehaviour<IDiagram>(ObjectFactory.Diagram)],

        [new GroupMoveBehaviour(ObjectFactory.Group)],
        [new NodeMoveBehaviour(ObjectFactory.Node)],
        [new PortMoveBehaviour(ObjectFactory.Port)],

        [new DefaultGroupBehaviour(ObjectFactory.Group)],
        [new DefaultLinkBehaviour(ObjectFactory.Link)],

        [new EventPipelineBehaviour<IDiagram>(ObjectFactory.Diagram)],
        [new EventPipelineBehaviour<ILayer>(ObjectFactory.Layer)],
        [new EventPipelineBehaviour<INode>(ObjectFactory.Node)],
        [new EventPipelineBehaviour<IPort>(ObjectFactory.Port)],

        [new GroupContainerBehaviour<IGroup>(ObjectFactory.Group)],
        [new GroupContainerBehaviour<ILayer>(ObjectFactory.Layer)],

        [new NodeContainerBehaviour<ILayer>(ObjectFactory.Layer)],
        [new NodeContainerBehaviour<IGroup>(ObjectFactory.Group)],

        [new PortContainerBehaviour<IGroup>(ObjectFactory.Group)],
        [new PortContainerBehaviour<INode>(ObjectFactory.Node)],

        [new LayerContainerBehaviour<IDiagram>(ObjectFactory.Diagram)],
    ];

    public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
}