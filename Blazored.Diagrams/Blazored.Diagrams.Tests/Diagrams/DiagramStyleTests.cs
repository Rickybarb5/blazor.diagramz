using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Observers;
using Blazored.Diagrams.Services.Virtualization;
using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Diagrams;

public class DiagramStyleTests : TestContext
{
    [Theory]
    [InlineData("width", "50%")]
    [InlineData("height", "50%")]
    [InlineData("border", "2px solid white")]
    [InlineData("background-size", "50px 50px")]
    public void StyleChangeIsApplied(string property, string expectedValue)
    {
        var diagram = ObjectFactory.Diagram;
        diagram.Options.StyleOptions.GridStyle =
            $"background-image:linear-gradient(to right, rgba(0, 0, 0, 0.1) 1px, transparent 1px),linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 1px, transparent 1px); background-size:50px 50px;";
        diagram.Options.StyleOptions.ContainerStyle =
            "width:50%;height:50%;overflow:hidden;border:2px solid white";

        SetupJsInterop();
        SetupVirtualizationService();
        var mockResizeObserver = new Mock<IResizeObserver>();
        Services.AddSingleton(mockResizeObserver.Object);

        var cut = RenderComponent<DiagramContainer>(parameters => parameters
            .Add(p => p.Diagram, diagram));

        // Act
        var element = cut.Find($"#diagram-container-{diagram.Id}"); // Replace with appropriate selector
        var styleAttribute = element.GetAttribute("style");

        // Assert
        Assert.Contains($"{property}:{expectedValue}", styleAttribute);
    }

    [Fact]
    public void Grid_Size_Change_Affects_Css()
    {
        var diagram = ObjectFactory.Diagram;
        diagram.Options.StyleOptions.CellSize = 50;

        var mockResizeObserver = new Mock<IResizeObserver>();
        SetupJsInterop();
        SetupVirtualizationService();
        Services.AddSingleton(mockResizeObserver.Object);
        var cut = RenderComponent<DiagramContainer>(parameters => parameters
            .Add(p => p.Diagram, diagram));

        // Act
        var element = cut.Find($"#diagram-container-{diagram.Id}"); // Replace with appropriate selector
        var styleAttribute = element.GetAttribute("style");

        // Assert
        Assert.Equal(diagram.Options.StyleOptions.ContainerStyle + diagram.Options.StyleOptions.GridStyle,
            styleAttribute);
    }

    [Fact]
    public void Default_Grid_Style_Uses_Grid_Size()
    {
        var diagram = ObjectFactory.Diagram;
        diagram.Options.StyleOptions.CellSize = 50;

        var mockResizeObserver = new Mock<IResizeObserver>();
        SetupJsInterop();
        SetupVirtualizationService();
        Services.AddSingleton(mockResizeObserver.Object);
        var cut = RenderComponent<DiagramContainer>(parameters => parameters
            .Add(p => p.Diagram, diagram));

        // Act
        var element = cut.Find($"#diagram-container-{diagram.Id}"); // Replace with appropriate selector
        var styleAttribute = element.GetAttribute("style");

        // Assert
        Assert.Contains(
            $"background-size:{diagram.Options.StyleOptions.CellSize}px {diagram.Options.StyleOptions.CellSize}px",
            styleAttribute);
    }

    private void SetupJsInterop()
    {
        //Setup resize observer
        var expectedRect = new Rect { Left = 0, Top = 0, Width = 100, Height = 100, Bottom = 100, Right = 100 };

        //Add them to the services
        JSInterop.Setup<Rect>().SetResult(expectedRect);

        JSInterop.SetupVoid(JsFunctionConstants.HandleZoomFunctionName, _ => true);
    }

    private void SetupVirtualizationService()
    {
        var mock = new Mock<IVirtualizationService>();
        Services.AddSingleton(mock.Object);
    }
}