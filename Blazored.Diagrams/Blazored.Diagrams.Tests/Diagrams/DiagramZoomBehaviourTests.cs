using Blazored.Diagrams.Diagrams.Options;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

public class DiagramZoomBehaviourTests
{
    [Fact]
    public Task Wheel_Up_ChangesZoom()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var args = new WheelEventArgs { DeltaY = 1 };

        //Act
        diagram.NotifyWheel(args);

        //Assert
        Assert.Equal(1 - ZoomOptions.DefaultZoomStep, diagram.Zoom);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Wheel_Down_ChangesZoom()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var args = new WheelEventArgs { DeltaY = -1 };

        //Act
        diagram.NotifyWheel(args);

        //Assert
        Assert.Equal(1 + ZoomOptions.DefaultZoomStep, diagram.Zoom);
        return Task.CompletedTask;
    }
}