﻿using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams.Options;

namespace Blazored.Diagrams.Test.Diagrams;

public class DiagramTests
{
    [Fact]
    public void Test_AddLayer()
    {
        var diagram = ObjectFactory.Diagram;
        var layer = ObjectFactory.Layer;

        diagram.Layers.Add(layer);

        Assert.True(diagram.Layers.Count == 2);
        Assert.Equal(layer, diagram.Layers[diagram.Layers.Count - 1]);
    }

    [Fact]
    public void Test_RemoveLayer()
    {
        var diagram = ObjectFactory.Diagram;

        diagram.Layers.Remove(diagram.CurrentLayer);

        Assert.True(diagram.Layers.Count == 0);
    }

    [Fact]
    public void Diagram_Has_Default_Behaviours()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;

        //Assert
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(ZoomBehavior));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(PanBehaviour));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(ModelMoveBehaviour));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(ModelSelectBehaviour));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(LinkCreationBehavior));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(DeleteWithKeyBehaviour));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(DeleteBehaviour));
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(RedrawBehaviour<IDiagram>));
        Assert.Equal(10, diagram.Behaviours.All.Count);
    }

    [Fact]
    public void Only_Allows_One_Behaviour_Instance()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var behaviour = ObjectFactory.Behaviour;
        //Act
        diagram.Behaviours.Add(behaviour);
        diagram.Behaviours.Add(behaviour);

        //Assert
        Assert.Contains(diagram.Behaviours.All, x => x.GetType() == typeof(TestBehaviour));
        Assert.Equal(1, diagram.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void Removes_Behaviour()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var behaviour = ObjectFactory.Behaviour;
        diagram.Behaviours.Add(behaviour);
        //Act
        diagram.Behaviours.Remove<TestBehaviour>();

        //Assert
        Assert.Equal(0, diagram.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void Set_Zoom_StepUp_Returns_Correct_Value()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        //Act
        diagram.StepZoomUp();

        //Assert
        Assert.Equal(1 + diagram.Options.ZoomOptions.ZoomStep, diagram.Zoom);
    }

    [Fact]
    public void Set_Zoom_StepDown_Returns_Correct_Value()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        //Act
        diagram.StepZoomDown();

        //Assert
        Assert.Equal(1 - diagram.Options.ZoomOptions.ZoomStep, diagram.Zoom);
    }

    [Theory]
    [InlineData(ZoomOptions.DefaultMaximumZoom + 1, ZoomOptions.DefaultMaximumZoom)]
    [InlineData(ZoomOptions.DefaultMinimumZoom - 1, ZoomOptions.DefaultMinimumZoom)]
    public Task Zoom_Is_Clamped(double inputZoom, double expectedZoom)
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        //Act
        diagram.SetZoom(inputZoom);

        //Assert
        Assert.Equal(expectedZoom, diagram.Zoom);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Use_Layer_Changes_Current_Layer()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var newLayer = ObjectFactory.Layer;
        diagram.Layers.Add(newLayer);
        //Act
        diagram.UseLayer(newLayer);
        //Assert
        Assert.Same(newLayer, diagram.CurrentLayer);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Use_Layer_By_Id_Changes_Current_Layer()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var newLayer = ObjectFactory.Layer;
        diagram.Layers.Add(newLayer);
        //Act
        diagram.UseLayer(newLayer.Id);
        //Assert
        Assert.Same(newLayer, diagram.CurrentLayer);
        return Task.CompletedTask;
    }


    [Fact]
    public void Test_UnselectAll()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        var nestedGroup = ObjectFactory.Group;
        var nestedGroupNode = ObjectFactory.Node;
        diagram.AddNode(node);
        node.IsSelected = true;
        diagram.AddGroup(group);
        group.IsSelected = true;
        group.Groups.Add(nestedGroup);
        nestedGroup.IsSelected = true;
        group.Nodes.Add(nestedNode);
        nestedNode.IsSelected = true;
        nestedGroup.Nodes.Add(nestedGroupNode);
        nestedGroupNode.IsSelected = true;
        //Act
        diagram.UnselectAll();
        //Assert
        Assert.False(node.IsSelected);
        Assert.False(group.IsSelected);
        Assert.False(nestedGroup.IsSelected);
        Assert.False(nestedNode.IsSelected);
        Assert.False(nestedGroupNode.IsSelected);
    }

    [Fact]
    public void Diagram_Position_Changes()
    {
        var diagram = ObjectFactory.Diagram;

        diagram.PositionX = 10;
        diagram.PositionY = 20;

        Assert.Equal(20, diagram.PositionY);
        Assert.Equal(10, diagram.PositionX);
    }
}