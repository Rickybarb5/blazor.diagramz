using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

public class ModelSelectBehaviourTests
{
    [Fact]
    public void Node_Click_Triggers_Selection()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        diagram.AddNode(node);
        // Act
        node.NotifyPointerDown(new PointerEventArgs());
        // Assert
        Assert.True(node.IsSelected);
    }

    [Fact]
    public void Group_Click_Triggers_Selection()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        diagram.AddGroup(group);
        // Act
        group.NotifyPointerDown(new PointerEventArgs());
        // Assert
        Assert.True(group.IsSelected);
    }

    [Fact]
    public void Link_Click_Triggers_Selection()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        node.Ports.Add(port1);
        group.Ports.Add(port2);
        diagram.AddNode(node);
        diagram.AddGroup(group);
        var link = diagram.CreateLink<DummyComponent>(port1, port2);
        // Act
        link.NotifyPointerDown(new PointerEventArgs());
        // Assert
        Assert.True(link.IsSelected);
    }

    [Fact]
    public void Diagram_PointerDown_Unselects_All()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var node2 = ObjectFactory.Node;
        var group2 = ObjectFactory.Group;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        node2.Ports.Add(port1);
        group2.Ports.Add(port2);
        group.Nodes.Add(node);
        diagram.AddGroup(group);
        diagram.AddGroup(group2);
        diagram.AddNode(node2);
        var link = diagram.CreateLink<DummyComponent>(port1, port2);
        link.NotifyPointerDown(new PointerEventArgs { CtrlKey = true });
        group.NotifyPointerDown(new PointerEventArgs { CtrlKey = true });
        node.NotifyPointerDown(new PointerEventArgs { CtrlKey = true });
        // Act
        diagram.NotifyPointerDown(new PointerEventArgs());
        // Assert
        Assert.False(link.IsSelected);
        Assert.False(group.IsSelected);
        Assert.False(node.IsSelected);
    }
}