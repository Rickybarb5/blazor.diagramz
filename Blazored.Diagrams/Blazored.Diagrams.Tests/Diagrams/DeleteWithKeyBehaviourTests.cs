using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Observers;
using Blazored.Diagrams.Services.Virtualization;
using Bunit;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Diagrams;

public class DeleteWithKeyBehaviourTests : TestContext
{
    [Fact]
    public void Configured_Key_Deletes_Selected_Models()
    {
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        nestedGroup.Ports.Add(ObjectFactory.Port);
        node.Ports.Add(ObjectFactory.Port);
        node.IsSelected = true;
        group.IsSelected = true;
        diagram.AddGroup(group);
        group.Groups.Add(nestedGroup);
        group.Nodes.Add(nestedNode);
        diagram.AddNode(node);
        var link = diagram.CreateLink<LinkCurvedComponent>(nestedGroup.Ports.First(), node.Ports.First());
        link.IsSelected = true;

        SetupResizeObserver();
        SetupVirtualizationService();

        SetupJsInterop();
        var cut = RenderComponent<DiagramContainer>(parameters => parameters
            .Add(p => p.Diagram, diagram));
        var id = $"diagram-container-{diagram.Id}";
        var element = cut.Find($"#{id}");
        var args = new KeyboardEventArgs()
            { Code = "Delete" };

        //Act
        element.KeyDown(args);

        //Assert
        Assert.Equal(0, diagram.AllNodes.Count);
        Assert.Equal(0, diagram.AllGroups.Count);
        Assert.Equal(0, diagram.AllLinks.Count);
    }

    private void SetupResizeObserver()
    {
        var mockResizeObserver = new Mock<IResizeObserver>();
        Services.AddSingleton(mockResizeObserver.Object);
    }

    private void SetupVirtualizationService()
    {
        var mock = new Mock<IVirtualizationService>();
        Services.AddSingleton(mock.Object);
    }

    private void SetupJsInterop()
    {
        //Setupjsinterop
        var expectedRect = new Rect { Left = 0, Top = 0, Width = 100, Height = 100, Bottom = 100, Right = 100 };

        //Add them to the services
        JSInterop.Setup<Rect>().SetResult(expectedRect);
        JSInterop.SetupVoid(JsFunctionConstants.HandleZoomFunctionName, _ => true);
    }
}