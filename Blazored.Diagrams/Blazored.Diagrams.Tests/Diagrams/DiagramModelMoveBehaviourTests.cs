using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

public class ModelMoveBehaviourTests
{
    [Fact]
    public void Pointer_Events_Trigger_Node_Position_Update()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var startPositionX = node.PositionX;
        var startPositionY = node.PositionY;
        var nodeArgs = new PointerEventArgs();
        const int pixelsMoved = 3;
        var pointerMoveArgs = new PointerEventArgs { ClientX = pixelsMoved, ClientY = pixelsMoved };
        diagram.AddNode(node);

        // Act
        node.NotifyPointerDown(nodeArgs);
        diagram.NotifyPointerMove(pointerMoveArgs);

        // Assert
        Assert.Equal(startPositionX + pixelsMoved, node.PositionX);
        Assert.Equal(startPositionY + pixelsMoved, node.PositionY);
    }

    [Fact]
    public void Pointer_Events_Trigger_Group_Position_Update()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var startPositionX = group.PositionX;
        var startPositionY = group.PositionY;
        var nodeArgs = new PointerEventArgs();
        const int pixelsMoved = 3;
        var pointerMoveArgs = new PointerEventArgs { ClientX = pixelsMoved, ClientY = pixelsMoved };
        diagram.AddGroup(group);

        // Act
        group.NotifyPointerDown(nodeArgs);
        diagram.NotifyPointerMove(pointerMoveArgs);
        diagram.NotifyPointerUp(pointerMoveArgs);

        // Assert
        Assert.Equal(startPositionX + pixelsMoved, group.PositionX);
        Assert.Equal(startPositionY + pixelsMoved, group.PositionY);
    }
}