using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

/// <summary>
///     Tests events related to diagrams.
/// </summary>
public class DiagramEventTests
{
    [Fact]
    public Task Layer_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        ILayer? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var layer = ObjectFactory.Layer;
        diagram.OnLayerAdded += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        diagram.Layers.Add(layer);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(layer, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Layer_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        ILayer? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var layer = ObjectFactory.Layer;
        diagram.Layers.Add(layer);
        diagram.OnLayerRemoved += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        diagram.Layers.Remove(layer);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(layer, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Node_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        diagram.OnNodeAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        diagram.AddNode(node);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Node_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = new Node<DefaultNodeComponent>();
        diagram.AddNode(node);
        diagram.OnNodeRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        diagram.RemoveNode(node);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        diagram.OnGroupAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        diagram.AddGroup(group);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(group, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        diagram.AddGroup(group);
        diagram.OnGroupRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        diagram.RemoveGroup(group);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(group, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Nested_Group_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        var nestedGroup = new Group<DefaultGroupComponent>();
        diagram.AddGroup(group);
        diagram.OnGroupAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Groups.Add(nestedGroup);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task  Nested_Group_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        diagram.AddGroup(group);
        group.Groups.Add(nestedGroup);
        diagram.OnGroupRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Groups.Remove(nestedGroup);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Nested_Node_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        diagram.AddGroup(group);
        diagram.OnNodeAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Nodes.Add(nestedNode);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedNode, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Nested_Node_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        diagram.AddGroup(group);
        group.Nodes.Add(nestedNode);
        diagram.OnNodeRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Nodes.Remove(nestedNode);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedNode, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Zoom_Change_Triggers_Events()
    {
        // Arrange
        var beforeEventTriggered = false;
        var zoomEventTriggered = false;
        var diagram = ObjectFactory.Diagram;
        var newZoom = 0.7;

        diagram.OnBeforeZoomChanged += _ => { beforeEventTriggered = true; };

        diagram.OnZoomChanged += _ => { zoomEventTriggered = true; };
        // Act
        diagram.SetZoom(newZoom);
        // Assert
        Assert.True(beforeEventTriggered);
        Assert.True(zoomEventTriggered);
        Assert.Equal(newZoom, diagram.Zoom);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Pan_Change_Triggers_Events()
    {
        // Arrange
        var panEventTriggered = false;
        var diagram = ObjectFactory.Diagram;
        const int newPanX = 3;
        const int newPanY = 3;

        diagram.OnPanChanged += (_, _) =>
        {
            panEventTriggered = true;
            Assert.Equal(newPanX, diagram.PanX);
            Assert.Equal(newPanY, diagram.PanY);
        };
        // Act
        diagram.SetPan(newPanX, newPanY);
        // Assert
        Assert.True(panEventTriggered);
        return Task.CompletedTask;
    }

    [Fact]
    public void Same_Zoom_Does_Not_Trigger_Event()
    {
        //Arrange
        var zoomchangedTriggered = false;
        var beforeZoomchangedTriggered = false;
        var diagram = ObjectFactory.Diagram;
        diagram.OnZoomChanged += _ => zoomchangedTriggered = true;
        diagram.OnBeforeZoomChanged += _ => beforeZoomchangedTriggered = true;
        //Act
        diagram.SetZoom(diagram.Zoom);

        //Assert
        Assert.False(beforeZoomchangedTriggered);
        Assert.False(zoomchangedTriggered);
    }

    [Fact]
    public void Node_Port_Events_Propagate_To_Diagram()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();
        diagram.AddNode(node);
        node.Ports.Add(port);

        // Act
        // Assert
        Test_Port_Event_propagation(diagram, port);
    }

    [Fact]
    public void Group_Port_Events_Propagate_To_Diagram()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var port = new Port<DummyComponent>();
        diagram.AddGroup(group);
        group.Ports.Add(port);

        // Act
        // Assert
        Test_Port_Event_propagation(diagram, port);
    }

    private static void Test_Port_Event_propagation(IDiagram diagram, IPort port)
    {
        var args = new PointerEventArgs();
        var wheelArgs = new WheelEventArgs();
        var pointerDownEventTriggered = false;
        var pointerUpEventTriggered = false;
        var pointerEnterEventTriggered = false;
        var pointerLeaveEventTriggered = false;
        var pointerMoveEventTriggered = false;
        var pointerClickEventTriggered = false;
        var pointerDbClickEventTriggered = false;
        var wheelEventTriggered = false;

        diagram.OnPortPointerDown += (_, _) => pointerDownEventTriggered = true;
        diagram.OnPortPointerUp += (_, _) => pointerUpEventTriggered = true;
        diagram.OnPortPointerEnter += (_, _) => pointerEnterEventTriggered = true;
        diagram.OnPortPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
        diagram.OnPortPointerMove += (_, _) => pointerMoveEventTriggered = true;
        diagram.OnPortClicked += (_, _) => pointerClickEventTriggered = true;
        diagram.OnPortDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
        diagram.OnPortWheel += (_, _) => wheelEventTriggered = true;

        // Act
        port.NotifyPointerDown(args);
        port.NotifyPointerEnter(args);
        port.NotifyPointerLeave(args);
        port.NotifyPointerMove(args);
        port.NotifyPointerUp(args);
        port.NotifyClick(args);
        port.NotifyDoubleClick(args);
        port.NotifyWheel(wheelArgs);

        // Assert
        Assert.True(pointerDownEventTriggered);
        Assert.True(pointerUpEventTriggered);
        Assert.True(pointerEnterEventTriggered);
        Assert.True(pointerLeaveEventTriggered);
        Assert.True(pointerMoveEventTriggered);
        Assert.True(pointerClickEventTriggered);
        Assert.True(pointerDbClickEventTriggered);
        Assert.True(wheelEventTriggered);
    }

    [Fact]
    public Task Link_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        ILink? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        node.Ports.Add(port1);
        group.Ports.Add(port2);
        diagram.AddNode(node);
        diagram.AddGroup(group);
        diagram.OnLinkAdded += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        var link = diagram.CreateLink<DummyComponent>(port1, port2);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(link, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Link_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        ILink? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        node.Ports.Add(port1);
        group.Ports.Add(port2);
        diagram.AddNode(node);
        diagram.AddGroup(group);
        var link = diagram.CreateLink<DummyComponent>(port1, port2);
        diagram.OnLinkRemoved += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        diagram.RemoveLink(link);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(link, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Port_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IPort? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var port = ObjectFactory.Port;
        diagram.AddNode(node);
        node.Ports.Add(port);
        diagram.OnPortRemoved += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        node.Ports.Remove(port);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Port_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IPort? eventObject = null;
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var port = ObjectFactory.Port;
        diagram.AddNode(node);
        diagram.OnPortAdded += l =>
        {
            eventTriggered = true;
            eventObject = l;
        };

        // Act
        node.Ports.Add(port);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public void Link_Events_Propagate_To_Diagram()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        node.Ports.Add(port1);
        group.Ports.Add(port2);
        diagram.AddNode(node);
        diagram.AddGroup(group);
        var link = diagram.CreateLink<DummyComponent>(port1, port2);

        // Act
        // Assert
        Test_Link_Event_propagation(diagram, link);
    }

    private static void Test_Link_Event_propagation(IDiagram diagram, ILink link)
    {
        var args = new PointerEventArgs();
        var wheelArgs = new WheelEventArgs();
        var pointerDownEventTriggered = false;
        var pointerUpEventTriggered = false;
        var pointerEnterEventTriggered = false;
        var pointerLeaveEventTriggered = false;
        var pointerMoveEventTriggered = false;
        var pointerClickEventTriggered = false;
        var pointerDbClickEventTriggered = false;
        var wheelEventTriggered = false;

        diagram.OnLinkPointerDown += (_, _) => pointerDownEventTriggered = true;
        diagram.OnLinkPointerUp += (_, _) => pointerUpEventTriggered = true;
        diagram.OnLinkPointerEnter += (_, _) => pointerEnterEventTriggered = true;
        diagram.OnLinkPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
        diagram.OnLinkPointerMove += (_, _) => pointerMoveEventTriggered = true;
        diagram.OnLinkClicked += (_, _) => pointerClickEventTriggered = true;
        diagram.OnLinkDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
        diagram.OnLinkWheel += (_, _) => wheelEventTriggered = true;

        // Act
        link.NotifyPointerDown(args);
        link.NotifyPointerEnter(args);
        link.NotifyPointerLeave(args);
        link.NotifyPointerMove(args);
        link.NotifyPointerUp(args);
        link.NotifyClick(args);
        link.NotifyDoubleClick(args);
        link.NotifyWheel(wheelArgs);

        // Assert
        Assert.True(pointerDownEventTriggered);
        Assert.True(pointerUpEventTriggered);
        Assert.True(pointerEnterEventTriggered);
        Assert.True(pointerLeaveEventTriggered);
        Assert.True(pointerMoveEventTriggered);
        Assert.True(pointerClickEventTriggered);
        Assert.True(pointerDbClickEventTriggered);
        Assert.True(wheelEventTriggered);
    }


    [Fact]
    public void Diagram_Position_Triggers_Events()
    {
        var diagram = ObjectFactory.Diagram;
        var beforeEventTriggered = false;
        var posYTriggersEvent = false;
        var posXTriggersEvent = false;

        diagram.OnBeforePositionChanged += _ => beforeEventTriggered = true;
        diagram.OnPositionChanged += d =>
        {
            if (d.PositionX == 10)
            {
                posXTriggersEvent = true;
            }

            if (d.PositionY == 20)
            {
                posYTriggersEvent = true;
            }
        };

        diagram.PositionX = 10;
        diagram.PositionY = 20;

        Assert.True(beforeEventTriggered);
        Assert.True(posXTriggersEvent);
        Assert.True(posYTriggersEvent);
    }
}