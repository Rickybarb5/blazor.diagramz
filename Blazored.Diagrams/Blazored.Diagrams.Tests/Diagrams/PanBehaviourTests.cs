using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Diagrams;

public class PanBehaviourTests
{
    [Fact]
    public void Pointer_Events_Pan_Diagram()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;

        var startPanX = diagram.PanX;
        var startPanY = diagram.PanY;
        var pixelsMoved = 3;
        var pointerDownArgs = new PointerEventArgs { ClientX = 0, ClientY = 0 };
        var pointerMoveArgs = new PointerEventArgs { ClientX = 3, ClientY = 3 };
        var pointerUpArgs = new PointerEventArgs { ClientX = 3, ClientY = 3 };

        // Act
        diagram.NotifyPointerDown(pointerDownArgs);
        diagram.NotifyPointerMove(pointerMoveArgs);
        diagram.NotifyPointerUp(pointerUpArgs);

        // Assert
        Assert.Equal(startPanX + pixelsMoved, diagram.PanX);
        Assert.Equal(startPanY + pixelsMoved, diagram.PanY);
    }
}