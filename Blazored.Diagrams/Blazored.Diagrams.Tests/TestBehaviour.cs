using Blazored.Diagrams.Interfaces.Behaviours;

namespace Blazored.Diagrams.Test;

public class TestBehaviour : IBehaviour
{
    public void Dispose()
    {
    }

    public bool IsEnabled { get; set; } = true;
}