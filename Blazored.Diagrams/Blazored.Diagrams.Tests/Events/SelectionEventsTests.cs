using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Test.Events;

public class SelectionEventsTests
{
    [Fact]
    public void TestNodePositionEvents()
    {
        // Arrange
        var node = ObjectFactory.Node;

        //Act
        //Assert
        Test_Selection_Events(node, node);
    }

    [Fact]
    public void TestGroupPositionEvents()
    {
        // Arrange
        var group = ObjectFactory.Group;

        //Act
        //Assert
        Test_Selection_Events(group, group);
    }

    [Fact]
    public void TestLinkSelectionEvents()
    {
        // Arrange
        var link = ObjectFactory.Link;

        //Act
        //Assert
        Test_Selection_Events(link, link);
    }

    private void Test_Selection_Events<TModel>(ISelectionEvents<TModel> events, TModel model) where TModel : ISelectable
    {
        var eventTriggered = false;

        events.OnSelectionChanged += (_, _) => eventTriggered = true;

        // Act
        events.NotifySelectionChanged();

        // Assert
        Assert.True(eventTriggered);
    }
}