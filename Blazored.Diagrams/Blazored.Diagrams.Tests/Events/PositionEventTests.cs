using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Test.Events;

public class PositionEventTests
{
    [Fact]
    public void TestNodePositionEvents()
    {
        // Arrange
        var node = ObjectFactory.Node;

        //Act
        //Assert
        Test_Position_Events(node, node);
    }

    [Fact]
    public void TestGroupPositionEvents()
    {
        // Arrange
        var group = ObjectFactory.Group;

        //Act
        //Assert
        Test_Position_Events(group, group);
    }

    [Fact]
    public void TestPortPositionEvents()
    {
        // Arrange
        var port = ObjectFactory.Port;

        //Act
        //Assert
        Test_Position_Events(port, port);
    }

    private void Test_Position_Events<TModel>(IPositionEvents<TModel> events, TModel model) where TModel : IPosition
    {
        var positionChangedEventTriggered = false;
        var beforePositionChangedEventTriggered = false;

        events.OnPositionChanged += _ => positionChangedEventTriggered = true;

        // Act
        events.NotifyPositionChanged();

        // Assert
        Assert.True(positionChangedEventTriggered);
    }
}