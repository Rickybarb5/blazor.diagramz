using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Test.Events;

public class PortContainerEventsTestData : TheoryData<IPortContainer, IPortContainerEvents>
{
    private readonly IGroup _group = new Group<DummyComponent>();
    private readonly INode _node = new Node<DummyComponent>();

    public PortContainerEventsTestData()
    {
        Add(_node, _node);
        Add(_group, _group);
    }
}

public class NodeContainerEventsTestData : TheoryData<INodeContainer, INodeContainerEvents>
{
    private readonly IDiagram _diagram = new Diagram();
    private readonly IGroup _group = new Group<DummyComponent>();
    private readonly ILayer _layer = new Layer();

    public NodeContainerEventsTestData()
    {
        Add(_group, _group);
        Add(_layer, _layer);
    }
}

public class GroupContainerEventsTestData : TheoryData<IGroupContainer, IGroupContainerEvents>
{
    private readonly IGroup _group = new Group<DummyComponent>();
    private readonly ILayer _layer = new Layer();

    public GroupContainerEventsTestData()
    {
        Add(_group, _group);
        Add(_layer, _layer);
    }
}