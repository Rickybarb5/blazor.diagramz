using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Test.Events;

public class VisibilityEventTests
{
    [Fact]
    public void TestNodeVisibilityEvents()
    {
        // Arrange
        var node = ObjectFactory.Node;

        //Act
        //Assert
        Test_Visibility_Events(node, node);
    }

    [Fact]
    public void TestGroupVisibilityEvents()
    {
        // Arrange
        var group = ObjectFactory.Group;

        //Act
        //Assert
        Test_Visibility_Events(group, group);
    }

    [Fact]
    public void TestPortVisibilityEvents()
    {
        // Arrange
        var port = ObjectFactory.Port;

        //Act
        //Assert
        Test_Visibility_Events(port, port);
    }

    [Fact]
    public void TestLayerVisibilityEvents()
    {
        // Arrange
        var port = ObjectFactory.Port;

        //Act
        //Assert
        Test_Visibility_Events(port, port);
    }

    private void Test_Visibility_Events<TModel>(IVisibilityEvents<TModel> events, TModel model) where TModel : IVisible
    {
        var eventTriggered = false;

        events.OnVisibilityChanged += (_, _) => eventTriggered = true;

        // Act
        events.NotifyVisibilityChanged();

        // Assert
        Assert.True(eventTriggered);
    }
}