using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Events;

public class NodeContainerTests
{
    [Fact]
    public void Node_Container_Pointer_Events_Triggered()
    {
        // Arrange
        var testData = new NodeContainerEventsTestData();
        foreach (var data in testData)
        {
            var nodeContainer = data[0] as INodeContainer;
            var events = data[1] as INodeContainerEvents;
            var args = new PointerEventArgs();
            var wheelArgs = new WheelEventArgs();

            var node = ObjectFactory.Node;
            nodeContainer!.Nodes.Add(node);
            var pointerDownEventTriggered = false;
            var pointerUpEventTriggered = false;
            var pointerEnterEventTriggered = false;
            var pointerLeaveEventTriggered = false;
            var pointerMoveEventTriggered = false;
            var pointerClickEventTriggered = false;
            var pointerDbClickEventTriggered = false;
            var wheelEventTriggered = false;

            events!.OnNodePointerDown += (_, _) => pointerDownEventTriggered = true;
            events.OnNodePointerUp += (_, _) => pointerUpEventTriggered = true;
            events.OnNodePointerEnter += (_, _) => pointerEnterEventTriggered = true;
            events.OnNodePointerLeave += (_, _) => pointerLeaveEventTriggered = true;
            events.OnNodePointerMove += (_, _) => pointerMoveEventTriggered = true;
            events.OnNodeClicked += (_, _) => pointerClickEventTriggered = true;
            events.OnNodeDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
            events.OnNodeWheel += (_, _) => wheelEventTriggered = true;
            // Act
            node.NotifyPointerDown(args);
            node.NotifyPointerEnter(args);
            node.NotifyPointerLeave(args);
            node.NotifyPointerMove(args);
            node.NotifyPointerUp(args);
            node.NotifyClick(args);
            node.NotifyDoubleClick(args);
            node.NotifyWheel(wheelArgs);

            // Assert
            Assert.True(pointerDownEventTriggered);
            Assert.True(pointerUpEventTriggered);
            Assert.True(pointerEnterEventTriggered);
            Assert.True(pointerLeaveEventTriggered);
            Assert.True(pointerMoveEventTriggered);
            Assert.True(pointerClickEventTriggered);
            Assert.True(pointerDbClickEventTriggered);
            Assert.True(wheelEventTriggered);
        }
    }

    [Fact]
    public void Group_Container_Node_Added_Event_Triggered()
    {
        // Arrange
        var testData = new NodeContainerEventsTestData();
        foreach (var data in testData)
        {
            var nodeContainer = data[0] as INodeContainer;
            var events = data[1] as INodeContainerEvents;

            var node = ObjectFactory.Node;
            var eventTriggered = false;

            events!.OnNodeAdded += _ => eventTriggered = true;
            // Act
            nodeContainer!.Nodes.Add(node);

            // Assert
            Assert.True(eventTriggered);
        }
    }

    [Fact]
    public void Group_Container_Node_Removed_Event_Triggered()
    {
        // Arrange
        var testData = new NodeContainerEventsTestData();
        foreach (var data in testData)
        {
            var nodeContainer = data[0] as INodeContainer;
            var events = data[1] as INodeContainerEvents;

            var node = ObjectFactory.Node;
            var eventTriggered = false;
            nodeContainer!.Nodes.Add(node);

            events!.OnNodeRemoved += _ => eventTriggered = true;
            // Act
            nodeContainer!.Nodes.Remove(node);

            // Assert
            Assert.True(eventTriggered);
        }
    }
}