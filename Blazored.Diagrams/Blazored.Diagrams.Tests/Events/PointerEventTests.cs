using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Interfaces.Properties;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Events;

public class PointerEventTests
{
    [Fact]
    public void TestNodePointerEvents()
    {
        // Arrange
        var node = ObjectFactory.Node;

        //Act
        //Assert
        Test_Pointer_Events(node, node);
    }

    [Fact]
    public void TestGroupPointerEvents()
    {
        // Arrange
        var group = ObjectFactory.Group;

        //Act
        //Assert
        Test_Pointer_Events(group, group);
    }

    [Fact]
    public void TestPortPointerEvents()
    {
        // Arrange
        var port = ObjectFactory.Port;

        //Act
        //Assert
        Test_Pointer_Events(port, port);
    }

    [Fact]
    public void TestLinkPointerEvents()
    {
        // Arrange
        var port = ObjectFactory.Link;

        //Act
        //Assert
        Test_Pointer_Events(port, port);
    }

    [Fact]
    public void TestDiagramPointerEvents()
    {
        // Arrange
        var diagram = ObjectFactory.Diagram;

        //Act
        //Assert
        Test_Pointer_Events(diagram, diagram);
    }

    private void Test_Pointer_Events<Model>(IPointerEvents<Model> events, Model model) where Model : IModel
    {
        var args = new PointerEventArgs();
        var wheelArgs = new WheelEventArgs();
        var pointerUpEventTriggered = false;
        var pointerDownEventTriggered = false;
        var pointerEnterEventTriggered = false;
        var pointerLeaveEventTriggered = false;
        var pointerMoveEventTriggered = false;
        var pointerClickEventTriggered = false;
        var pointerDbClickEventTriggered = false;
        var wheelEventTriggered = false;

        events.OnPointerUp += (_, _) => pointerUpEventTriggered = true;
        events.OnPointerEnter += (_, _) => pointerEnterEventTriggered = true;
        events.OnPointerDown += (_, _) => pointerDownEventTriggered = true;
        events.OnPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
        events.OnPointerMove += (_, _) => pointerMoveEventTriggered = true;
        events.OnClick += (_, _) => pointerClickEventTriggered = true;
        events.OnDoubleClick += (_, _) => pointerDbClickEventTriggered = true;
        events.OnWheel += (_, _) => wheelEventTriggered = true;

        // Act
        events.NotifyPointerUp(args);
        events.NotifyPointerDown(args);
        events.NotifyPointerEnter(args);
        events.NotifyPointerLeave(args);
        events.NotifyPointerMove(args);
        events.NotifyClick(args);
        events.NotifyDoubleClick(args);
        events.NotifyWheel(wheelArgs);

        // Assert
        Assert.True(pointerUpEventTriggered);
        Assert.True(pointerDownEventTriggered);
        Assert.True(pointerEnterEventTriggered);
        Assert.True(pointerLeaveEventTriggered);
        Assert.True(pointerMoveEventTriggered);
        Assert.True(pointerClickEventTriggered);
        Assert.True(pointerDbClickEventTriggered);
        Assert.True(wheelEventTriggered);
    }
}