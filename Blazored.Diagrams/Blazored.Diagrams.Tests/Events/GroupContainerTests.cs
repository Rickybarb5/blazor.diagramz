using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Events;

public class GroupContainerTests
{
    [Fact]
    public void Group_Container_Pointer_Events_Are_Triggered()
    {
        // Arrange
        var testData = new GroupContainerEventsTestData();
        foreach (var data in testData)
        {
            var groupContainer = data[0] as IGroupContainer;
            var events = data[1] as IGroupContainerEvents;
            var args = new PointerEventArgs();
            var wheelArgs = new WheelEventArgs();

            var group = ObjectFactory.Group;
            groupContainer!.Groups.Add(group);
            var pointerDownEventTriggered = false;
            var pointerUpEventTriggered = false;
            var pointerEnterEventTriggered = false;
            var pointerLeaveEventTriggered = false;
            var pointerMoveEventTriggered = false;
            var pointerClickEventTriggered = false;
            var pointerDbClickEventTriggered = false;
            var wheelEventTriggered = false;

            events!.OnGroupPointerDown += (_, _) => pointerDownEventTriggered = true;
            events.OnGroupPointerUp += (_, _) => pointerUpEventTriggered = true;
            events.OnGroupPointerEnter += (_, _) => pointerEnterEventTriggered = true;
            events.OnGroupPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
            events.OnGroupPointerMove += (_, _) => pointerMoveEventTriggered = true;
            events.OnGroupClicked += (_, _) => pointerClickEventTriggered = true;
            events.OnGroupDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
            events.OnGroupWheel += (_, _) => wheelEventTriggered = true;
            // Act
            group.NotifyPointerDown(args);
            group.NotifyPointerEnter(args);
            group.NotifyPointerLeave(args);
            group.NotifyPointerMove(args);
            group.NotifyPointerUp(args);
            group.NotifyClick(args);
            group.NotifyDoubleClick(args);
            group.NotifyWheel(wheelArgs);

            // Assert
            Assert.True(pointerDownEventTriggered);
            Assert.True(pointerUpEventTriggered);
            Assert.True(pointerEnterEventTriggered);
            Assert.True(pointerLeaveEventTriggered);
            Assert.True(pointerMoveEventTriggered);
            Assert.True(pointerClickEventTriggered);
            Assert.True(pointerDbClickEventTriggered);
            Assert.True(wheelEventTriggered);
        }
    }

    [Fact]
    public void Group_Container_Group_Added_Event_Triggered()
    {
        // Arrange
        var testData = new GroupContainerEventsTestData();
        foreach (var data in testData)
        {
            var groupContainer = data[0] as IGroupContainer;
            var events = data[1] as IGroupContainerEvents;

            var group = ObjectFactory.Group;
            var eventTriggered = false;

            events!.OnGroupAdded += _ => eventTriggered = true;
            // Act
            groupContainer!.Groups.Add(group);

            // Assert
            Assert.True(eventTriggered);
        }
    }

    [Fact]
    public void Group_Container_Group_Removed_Event_Triggered()
    {
        // Arrange
        var testData = new GroupContainerEventsTestData();
        foreach (var data in testData)
        {
            var groupContainer = data[0] as IGroupContainer;
            var events = data[1] as IGroupContainerEvents;

            var group = ObjectFactory.Group;
            groupContainer!.Groups.Add(group);
            var eventTriggered = false;

            events!.OnGroupRemoved += _ => eventTriggered = true;
            // Act
            groupContainer!.Groups.Remove(group);

            // Assert
            Assert.True(eventTriggered);
        }
    }
}