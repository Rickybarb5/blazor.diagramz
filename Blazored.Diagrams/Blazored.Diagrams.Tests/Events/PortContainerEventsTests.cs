using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Events;

public class PortContainerEventsTests
{
    [Fact]
    public void Test_Port_Container_Pointer_Events_Triggered()
    {
        // Arrange
        var testData = new PortContainerEventsTestData();
        foreach (var data in testData)
        {
            var portContainer = data[0] as IPortContainer;
            var events = data[1] as IPortContainerEvents;
            var args = new PointerEventArgs();
            var wheelArgs = new WheelEventArgs();

            var port = ObjectFactory.Port;
            portContainer!.Ports.Add(port);
            var pointerDownEventTriggered = false;
            var pointerUpEventTriggered = false;
            var pointerEnterEventTriggered = false;
            var pointerLeaveEventTriggered = false;
            var pointerMoveEventTriggered = false;
            var pointerClickEventTriggered = false;
            var pointerDbClickEventTriggered = false;
            var wheelEventTriggered = false;

            events!.OnPortPointerDown += (_, _) => pointerDownEventTriggered = true;
            events.OnPortPointerUp += (_, _) => pointerUpEventTriggered = true;
            events.OnPortPointerEnter += (_, _) => pointerEnterEventTriggered = true;
            events.OnPortPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
            events.OnPortPointerMove += (_, _) => pointerMoveEventTriggered = true;
            events.OnPortClicked += (_, _) => pointerClickEventTriggered = true;
            events.OnPortDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
            events.OnPortWheel += (_, _) => wheelEventTriggered = true;
            // Act
            port.NotifyPointerDown(args);
            port.NotifyPointerEnter(args);
            port.NotifyPointerLeave(args);
            port.NotifyPointerMove(args);
            port.NotifyPointerUp(args);
            port.NotifyClick(args);
            port.NotifyDoubleClick(args);
            port.NotifyWheel(wheelArgs);

            // Assert
            Assert.True(pointerDownEventTriggered);
            Assert.True(pointerUpEventTriggered);
            Assert.True(pointerEnterEventTriggered);
            Assert.True(pointerLeaveEventTriggered);
            Assert.True(pointerMoveEventTriggered);
            Assert.True(pointerClickEventTriggered);
            Assert.True(pointerDbClickEventTriggered);
            Assert.True(wheelEventTriggered);
        }
    }

    [Fact]
    public void Port_Container_Port_Added_Event_Triggered()
    {
        // Arrange
        var testData = new PortContainerEventsTestData();
        foreach (var data in testData)
        {
            var nodeContainer = data[0] as IPortContainer;
            var events = data[1] as IPortContainerEvents;

            var port = ObjectFactory.Port;
            var eventTriggered = false;

            events!.OnPortAdded += _ => eventTriggered = true;
            // Act
            nodeContainer!.Ports.Add(port);

            // Assert
            Assert.True(eventTriggered);
        }
    }

    [Fact]
    public void Port_Container_Port_Removed_Event_Triggered()
    {
        // Arrange
        var testData = new PortContainerEventsTestData();
        foreach (var data in testData)
        {
            var nodeContainer = data[0] as IPortContainer;
            var events = data[1] as IPortContainerEvents;

            var port = ObjectFactory.Port;
            var eventTriggered = false;
            nodeContainer!.Ports.Add(port);

            events!.OnPortRemoved += _ => eventTriggered = true;
            // Act
            nodeContainer!.Ports.Remove(port);

            // Assert
            Assert.True(eventTriggered);
        }
    }
}