using AngleSharp.Dom;
using Blazored.Diagrams.Diagrams;
using Bunit;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Components;

public abstract class ComponentPointerTestBase<TComponent, Model> : BunitPointerTestBase where TComponent : IComponent
{
    public abstract (IRenderedComponent<TComponent> component, Model model, IDiagram diagram) ArrangeTest();
    
    public void TriggerPointerDown(IElement element)
    {
        var args = new PointerEventArgs();
        element.PointerDown(args);
    }

    public void TriggerPointerUp(IElement element)
    {
        var args = new PointerEventArgs();
        element.PointerUp(args);
    }

    public void TriggerPointerEnter(IElement element)
    {
        var args = new PointerEventArgs();
        element.PointerEnter(args);
    }

    public void TriggerPointerLeave(IElement element)
    {
        var args = new PointerEventArgs();
        element.PointerLeave(args);
    }

    public void TriggerClick(IElement element)
    {
        var args = new PointerEventArgs();
        element.Click(args);
    }

    public void TriggerDbClick(IElement element)
    {
        var args = new PointerEventArgs();
        element.DoubleClick(args);
    }

    public void TriggerPointerMove(IElement element)
    {
        var args = new PointerEventArgs();
        element.PointerMove(args);
    }

    public void TriggerWheelEvent(IElement element)
    {
        var args = new WheelEventArgs();
        element.Wheel(args);
    }
    
    public void TriggerKeyUpEvent(IElement element)
    {
        var args = new KeyboardEventArgs();
        element.KeyUp(args);
    }
    
    public void TriggerKeyDownEvent(IElement element)
    {
        var args = new KeyboardEventArgs();
        element.KeyDown(args);
    }
}