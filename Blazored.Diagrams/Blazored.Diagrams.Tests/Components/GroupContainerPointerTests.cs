using Bunit;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;

namespace Blazored.Diagrams.Test.Components;

public class GroupContainerPointerTests : ComponentPointerTestBase<GroupContainer, IGroup>
{
    [Fact]
    public void Test_Pointer_Events()
    {
        // Arrange
        var items = ArrangeTest();
        var pointerUpEventTriggeredTriggered = false;
        var pointerDownEventTriggeredTriggered = false;
        var pointerEnterEventTriggeredTriggered = false;
        var pointerLeaveEventTriggeredTriggered = false;
        var pointerMoveEventTriggeredTriggered = false;
        var clickEventTriggeredTriggered = false;
        var dbClickEventTriggeredTriggered = false;
        var wheelEventTriggered = false;
        var node = ObjectFactory.Node;
        node.Ports.Add(ObjectFactory.Port);
        items.model.Nodes.Add(node);
        items.model.Groups.Add(ObjectFactory.Group);
        var pointerElement = items.component.Find($"#{items.component.Instance.ContainerId}");
        items.model.OnPointerDown += (_, _) => pointerUpEventTriggeredTriggered = true;
        items.model.OnPointerUp += (_, _) => pointerDownEventTriggeredTriggered = true;
        items.model.OnPointerEnter += (_, _) => pointerEnterEventTriggeredTriggered = true;
        items.model.OnPointerLeave += (_, _) => pointerLeaveEventTriggeredTriggered = true;
        items.model.OnPointerMove += (_, _) => pointerMoveEventTriggeredTriggered = true;
        items.model.OnClick += (_, _) => clickEventTriggeredTriggered = true;
        items.model.OnDoubleClick += (_, _) => dbClickEventTriggeredTriggered = true;
        items.model.OnDoubleClick += (_, _) => dbClickEventTriggeredTriggered = true;
        items.model.OnWheel += (_, _) => wheelEventTriggered = true;

        // Act
        TriggerPointerDown(pointerElement);
        TriggerPointerUp(pointerElement);
        TriggerPointerMove(pointerElement);
        TriggerPointerEnter(pointerElement);
        TriggerPointerLeave(pointerElement);
        TriggerClick(pointerElement);
        TriggerDbClick(pointerElement);
        TriggerWheelEvent(pointerElement);

        // Assert
        Assert.True(pointerUpEventTriggeredTriggered);
        Assert.True(pointerDownEventTriggeredTriggered);
        Assert.True(pointerEnterEventTriggeredTriggered);
        Assert.True(pointerLeaveEventTriggeredTriggered);
        Assert.True(pointerMoveEventTriggeredTriggered);
        Assert.True(clickEventTriggeredTriggered);
        Assert.True(dbClickEventTriggeredTriggered);
        Assert.True(wheelEventTriggered);
    }

    public override (IRenderedComponent<GroupContainer> component, IGroup model, IDiagram diagram) ArrangeTest()
    {
        SetupResizeObserver();
        SetupJsInterop();
        var group = ObjectFactory.Group;
        var diagram = ObjectFactory.Diagram;
        var component = RenderComponent<GroupContainer>(parameters =>
            parameters
                .Add(p => p.Group, group));

        return (component, group, diagram);
    }
}