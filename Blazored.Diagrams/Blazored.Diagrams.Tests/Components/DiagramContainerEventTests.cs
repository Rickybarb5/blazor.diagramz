using Bunit;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Diagrams;

namespace Blazored.Diagrams.Test.Components;

public class DiagramContainerEventTests : ComponentPointerTestBase<DiagramContainer, IDiagram>
{
    [Fact]
    public void Test_Pointer_Events()
    {
        // Arrange
        var items = ArrangeTest();
        var pointerUpEventTriggeredTriggered = false;
        var pointerDownEventTriggeredTriggered = false;
        var pointerEnterEventTriggeredTriggered = false;
        var pointerLeaveEventTriggeredTriggered = false;
        var pointerMoveEventTriggeredTriggered = false;
        var clickEventTriggeredTriggered = false;
        var dbClickEventTriggeredTriggered = false;
        var keyUpEventTriggered = false;
        var keyDownEventTriggered = false;
        var id = $"diagram-container-{items.model.Id}";
        var pointerElement = items.component.Find($"#{id}");
        var node = ObjectFactory.Node;
        node.Ports.Add(ObjectFactory.Port);
        items.model.AddNode(node);
        items.model.AddGroup(ObjectFactory.Group);
        items.model.OnPointerDown += (_, _) => pointerUpEventTriggeredTriggered = true;
        items.model.OnPointerUp += (_, _) => pointerDownEventTriggeredTriggered = true;
        items.model.OnPointerEnter += (_, _) => pointerEnterEventTriggeredTriggered = true;
        items.model.OnPointerLeave += (_, _) => pointerLeaveEventTriggeredTriggered = true;
        items.model.OnPointerMove += (_, _) => pointerMoveEventTriggeredTriggered = true;
        items.model.OnClick += (_, _) => clickEventTriggeredTriggered = true;
        items.model.OnDoubleClick += (_, _) => dbClickEventTriggeredTriggered = true;
        items.model.OnKeyDown += _ => keyDownEventTriggered = true;
        items.model.OnKeyUp += _ => keyUpEventTriggered = true;

        // Act
        TriggerPointerDown(pointerElement);
        TriggerPointerUp(pointerElement);
        TriggerPointerMove(pointerElement);
        TriggerPointerEnter(pointerElement);
        TriggerPointerLeave(pointerElement);
        TriggerClick(pointerElement);
        TriggerDbClick(pointerElement);
        TriggerKeyDownEvent(pointerElement);
        TriggerKeyUpEvent(pointerElement);

        // Assert
        Assert.True(pointerUpEventTriggeredTriggered);
        Assert.True(pointerDownEventTriggeredTriggered);
        Assert.True(pointerEnterEventTriggeredTriggered);
        Assert.True(pointerLeaveEventTriggeredTriggered);
        Assert.True(pointerMoveEventTriggeredTriggered);
        Assert.True(clickEventTriggeredTriggered);
        Assert.True(dbClickEventTriggeredTriggered);
        Assert.True(keyUpEventTriggered);
        Assert.True(keyDownEventTriggered);
    }

    public override (IRenderedComponent<DiagramContainer> component, IDiagram model, IDiagram diagram) ArrangeTest()
    {
        SetupResizeObserver();
        SetupJsInterop();
        SetupVirtualizationService();
        var diagram = ObjectFactory.Diagram;
        var component = RenderComponent<DiagramContainer>(parameters =>
            parameters
                .Add(p => p.Diagram, diagram));

        return (component, diagram, diagram);
    }
}