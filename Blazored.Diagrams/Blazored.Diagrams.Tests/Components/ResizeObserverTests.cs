using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Observers;
using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Components;

public class ResizeObserverTests : BunitResizeObserverTestBase
{
    [Fact]
    public void ResizeObserver_ShouldUpdateNodeSize_WhenResizeOccurs()
    {
        // Arrange
        const int expectedWidth = 500;
        const int expectedHeight = 300;
        var eventTriggered = false;
        var mockResizeObserver = new Mock<IResizeObserver>();
        var node = ObjectFactory.Node;
        Action<ResizeObserverEntry> capturedCallback = null;

        mockResizeObserver
            .Setup(ro => ro.ObserveAsync(It.IsAny<string>(), It.IsAny<Action<ResizeObserverEntry>>()))
            .Callback<string, Action<ResizeObserverEntry>>((_, callback) => capturedCallback = callback)
            .Returns(Task.CompletedTask);

        var expectedRect = new Rect { Left = 0, Top = 0, Width = 100, Height = 100, Bottom = 100, Right = 100 };

        //Add them to the services
        JSInterop.Setup<Rect>().SetResult(expectedRect);
        Services.AddSingleton(mockResizeObserver.Object);

        var cut = RenderComponent<NodeContainer>(parameters => parameters
            .Add(p => p.Node, node));

        mockResizeObserver.Verify(
            ro => ro.ObserveAsync(cut.Instance.ContainerId, It.IsAny<Action<ResizeObserverEntry>>()), Times.Once);
        node.OnSizeChanged += _ => eventTriggered = true;
        Assert.NotNull(capturedCallback);

        // Act
        var newSize = new ResizeObserverEntry { Height = expectedHeight, Width = expectedWidth };
        capturedCallback.Invoke(newSize);

        // Assert
        Assert.True(eventTriggered);
        Assert.Equal(expectedWidth, node.Width);
        Assert.Equal(expectedHeight, node.Height);
    }
}