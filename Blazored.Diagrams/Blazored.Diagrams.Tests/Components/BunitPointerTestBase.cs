using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Observers;
using Blazored.Diagrams.Services.Virtualization;
using Bunit;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Components;

public class BunitPointerTestBase : TestContext
{
    public void SetupResizeObserver()
    {
        var mockResizeObserver = new Mock<IResizeObserver>();
        mockResizeObserver
            .Setup(ro => ro.ObserveAsync(It.IsAny<ElementReference>(), It.IsAny<Action<ResizeObserverEntry>>()))
            .Returns(Task.CompletedTask);
        Services.AddSingleton(mockResizeObserver.Object);
    }

    public void SetupJsInterop()
    {
        //Setup resize observer
        var expectedRect = new Rect { Left = 0, Top = 0, Width = 100, Height = 100, Bottom = 100, Right = 100 };

        //Add them to the services
        JSInterop.Setup<Rect>().SetResult(expectedRect);

        JSInterop.SetupVoid(JsFunctionConstants.HandleZoomFunctionName, _ => true);
    }

    public void SetupVirtualizationService()
    {
        var mock = new Mock<IVirtualizationService>();
        Services.AddSingleton(mock.Object);
    }
}