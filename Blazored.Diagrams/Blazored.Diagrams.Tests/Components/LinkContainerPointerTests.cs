using Bunit;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Observers;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Components;

public class LinkContainerPointerTests : TestContext
{
    [Fact]
    public void Test_Redraw()
    {
        // Arrange
        var link = ObjectFactory.Link;
        var diagram = ObjectFactory.Diagram;
        SetupResizeObserver();
        SetupJsInterop();
        var component = RenderComponent<LinkContainer>(parameters =>
            parameters
                .Add(p => p.Link, link)
                .Add(p => p.Diagram, diagram));
        var eventTriggered = false;
        link.OnRedraw += () => eventTriggered = true;
        // Act
        link.NotifyRedraw();

        // Assert
        Assert.True(eventTriggered);
    }


    public void SetupResizeObserver()
    {
        var mockResizeObserver = new Mock<IResizeObserver>();
        mockResizeObserver
            .Setup(ro => ro.ObserveAsync(It.IsAny<ElementReference>(), It.IsAny<Action<ResizeObserverEntry>>()))
            .Returns(Task.CompletedTask);
        Services.AddSingleton(mockResizeObserver.Object);
    }

    public void SetupJsInterop()
    {
        //Setup resize observer
        var expectedRect = new Rect { Left = 0, Top = 0, Width = 100, Height = 100, Bottom = 100, Right = 100 };

        //Add them to the services
        JSInterop.Setup<Rect>().SetResult(expectedRect);
    }
}