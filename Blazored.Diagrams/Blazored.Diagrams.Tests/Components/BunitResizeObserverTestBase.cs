using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Services.Observers;
using Bunit;
using Microsoft.Extensions.DependencyInjection;
using Moq;

namespace Blazored.Diagrams.Test.Components;

public class BunitResizeObserverTestBase: TestContext
{
    public (Action<ResizeObserverEntry> callback, Mock<IResizeObserver>)SetupObserverService<T>(T model) where T: ISize,IId
    {
        Action<ResizeObserverEntry> capturedCallback = null;
        var mockResizeObserver = new Mock<IResizeObserver>();
        mockResizeObserver
            .Setup(ro => ro.ObserveAsync(It.IsAny<string>(), It.IsAny<Action<ResizeObserverEntry>>()))
            .Callback<string, Action<ResizeObserverEntry>>((_, callback) => capturedCallback = callback)
            .Returns(Task.CompletedTask);

        Services.AddSingleton(mockResizeObserver.Object);
        return (capturedCallback, mockResizeObserver);
    }
}