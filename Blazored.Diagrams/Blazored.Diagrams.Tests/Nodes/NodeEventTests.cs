using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Nodes;

public class NodeEventTests
{
    [Fact]
    public Task Port_Added_Triggers_Event()
    {
        //Arrange
        var node = ObjectFactory.Node;
        IPort? eventObject = null;
        var eventTriggered = false;
        var port = new Port<DefaultPortComponent>();
        node.OnPortAdded += p =>
        {
            eventTriggered = true;
            eventObject = p;
        };
        //Act
        node.Ports.Add(port);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Port_Removed_Triggers_Event()
    {
        //Arrange
        var node = ObjectFactory.Node;
        IPort? eventObject = null;
        var eventTriggered = false;
        var port = new Port<DefaultPortComponent>();
        node.Ports.Add(port);
        node.OnPortRemoved += p =>
        {
            eventTriggered = true;
            eventObject = p;
        };
        //Act
        node.Ports.Remove(port);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }
}