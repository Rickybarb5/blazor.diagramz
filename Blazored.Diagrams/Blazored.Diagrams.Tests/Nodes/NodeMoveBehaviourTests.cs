using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Nodes;

public class NodeMoveBehaviourTests
{
    [Fact]
    public Task Test_Position_Change_Triggers_Behaviour()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();
        node.Ports.Add(port);
        var oldPositionX = port.PositionX;
        var oldPositionY = port.PositionY;

        //Act
        node.SetPosition(500, 500);

        //Assert
        Assert.NotEqual(oldPositionX, port.PositionX);
        Assert.NotEqual(oldPositionY, port.PositionY);

        return Task.CompletedTask;
    }

    [Fact]
    public Task Test_Size_Change_Triggers_Behaviour()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();
        port.Position = PortPosition.Right;
        node.Ports.Add(port);
        var oldPositionX = port.PositionX;
        var oldPositionY = port.PositionY;

        //Act
        node.SetSize(500, 500);

        //Assert
        Assert.NotEqual(oldPositionX, port.PositionX);
        Assert.NotEqual(oldPositionY, port.PositionY);

        return Task.CompletedTask;
    }
}