using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Nodes;

public class NodeTests
{
    [Fact]
    public void Test_AddPort()
    {
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();

        node.Ports.Add(port);

        Assert.True(node.Ports.Count == 1);
        Assert.Equal(port, node.Ports[0]);
    }

    [Fact]
    public void Test_RemovePort()
    {
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();
        node.Ports.Add(port);

        //Act
        node.Ports.Remove(port);

        //Assert
        Assert.True(node.Ports.Count == 0);
    }

    [Fact]
    public void Moving_Node_Also_Moves_Ports()
    {
        const int offsetX = 100;
        const int offsetY = 250;
        var node = ObjectFactory.Node;
        var port1 = new Port<DummyComponent>();
        node.Ports.Add(port1);

        //Act
        node.SetPosition(node.PositionX + offsetX, node.PositionY + offsetY);
        //Assert
        Assert.Equal(100, port1.PositionX);
        Assert.Equal(250, port1.PositionY);
    }
    [Fact]
    public void Only_Allows_One_Behaviour_Instance()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var behaviour = ObjectFactory.Behaviour;
        //Act
        node.Behaviours.Add(behaviour);
        node.Behaviours.Add(behaviour);

        //Assert
        Assert.Contains(node.Behaviours.All, x => x.GetType() == typeof(TestBehaviour));
        Assert.Equal(1, node.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void Removes_Behaviour()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var behaviour = ObjectFactory.Behaviour;
        node.Behaviours.Add(behaviour);
        //Act
        node.Behaviours.Remove<TestBehaviour>();

        //Assert
        Assert.Equal(0, node.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }
}