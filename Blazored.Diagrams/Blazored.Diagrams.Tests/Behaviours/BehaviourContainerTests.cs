using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams.Behaviours;
using Blazored.Diagrams.Links.Behaviours;

namespace Blazored.Diagrams.Test.Behaviours;

public class BehaviourContainerTests
{
    private BehaviourContainer Instance => new();

    [Fact]
    public void Exists_Returns_Correct_Value()
    {
        var container = Instance;
        container.Add(new PanBehaviour(ObjectFactory.Diagram));
        var expectedFalse = container.Exists<DefaultLinkBehaviour>();
        var expectedTrue = container.Exists<PanBehaviour>();

        Assert.False(expectedFalse);
        Assert.True(expectedTrue);
    }

    [Fact]
    public void Get_Returns_Correct_Value()
    {
        var container = Instance;
        var expected = new PanBehaviour(ObjectFactory.Diagram);
        container.Add(expected);

        var actual = container.Get<PanBehaviour>();

        Assert.Same(expected, actual);
    }

    [Fact]
    public void TryGet_Returns_Correct_Value()
    {
        var container = Instance;
        var expected = new PanBehaviour(ObjectFactory.Diagram);
        container.Add(expected);

        var exists = container.TryGet<PanBehaviour>(out var actual);

        Assert.Same(expected, actual);
        Assert.True(exists);
    }
}