﻿using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Test;

public static class ObjectFactory
{
    public static Node<DummyComponent> Node => new();
    public static TestNode TestNode => new();
    public static Group<DummyComponent> Group => new();
    public static Diagram Diagram => new();
    public static Layer Layer => new();
    public static Port<DummyComponent> Port => new();

    public static Link<DummyComponent> Link
    {
        get
        {
            var link = new Link<DummyComponent>();
            link.SourcePort = Port;
            link.TargetPort = Port;
            return link;
        }
    }

    public static TestBehaviour Behaviour => new();

    public static Link<TComponent> LinkWithPorts<TComponent>(IPort source, IPort target) where TComponent : IComponent
    {
        var link = new Link<TComponent>();
        link.SourcePort = source;
        link.TargetPort = target;
        return link;
    }
}