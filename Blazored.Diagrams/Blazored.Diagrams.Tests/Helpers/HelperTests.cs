using Blazored.Diagrams.Extensions;

namespace Blazored.Diagrams.Test.Helpers;

public class HelperTests
{
    [Fact]
    public void Test_Center_In()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;

        group.SetSize(100, 100);
        node.SetSize(50, 50);
        //Act
        node.CenterIn(group);
        //Assert
        Assert.NotEqual(0, node.PositionX);
        Assert.NotEqual(0, node.PositionY);
    }

    [Fact]
    public void Test_Center_In_Diagram()
    {
        //Arrange
        var diagram = ObjectFactory.Diagram;
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;

        group.SetSize(100, 100);
        node.SetSize(50, 50);
        //Act
        node.CenterIn(diagram);
        //Assert
        Assert.NotEqual(0, node.PositionX);
        Assert.NotEqual(0, node.PositionY);
    }
}