using Blazored.Diagrams.Interfaces.Containers;

namespace Blazored.Diagrams.Test.Common;

public class GroupContainerTests
{
    private static List<IGroupContainer> GetModels()
    {
        return
        [
            ObjectFactory.Group,
            ObjectFactory.Layer
        ];
    }

    [Fact]
    public void Test_AddGroup()
    {
        // Arrange
        var containers = GetModels();
        var group = ObjectFactory.Group;
        foreach (var container in containers)
        {
            // Act
            container.Groups.Add(group);

            //Assert
            Assert.Same(group, container.Groups[0]);
            Assert.True(container.Groups.Count == 1);
        }
    }

    [Fact]
    public void Test_RemoveGroup()
    {
        // Arrange
        var containers = GetModels();
        var group = ObjectFactory.Group;
        foreach (var container in containers)
        {
            container.Groups.Add(group);
            // Act
            container.Groups.Remove(group);

            //Assert
            Assert.Empty(container.Groups);
        }
    }
}