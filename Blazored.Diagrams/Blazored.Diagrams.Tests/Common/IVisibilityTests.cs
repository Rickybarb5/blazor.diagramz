using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Test.Common;

public class VisibilityTests
{
    private static List<IVisible> GetModels()
    {
        return
        [
            ObjectFactory.Node,
            ObjectFactory.Group,
            ObjectFactory.Link,
            ObjectFactory.Port,
            ObjectFactory.Layer
        ];
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void Test_Visibility(bool expected)
    {
        // Arrange
        var models = GetModels();
        foreach (var visible in models)
        {
            // Act
            visible.IsVisible = expected;

            //Assert
            Assert.Equal(expected, visible.IsVisible);
        }
    }
}