using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Test.Common;

public class SelectionTests
{
    private static List<ISelectable> GetModels()
    {
        return
        [
            ObjectFactory.Node,
            ObjectFactory.Group,
            ObjectFactory.Link
        ];
    }

    [Theory]
    [InlineData(true)]
    [InlineData(false)]
    public void Test_Selection(bool expected)
    {
        // Arrange
        var models = GetModels();
        foreach (var selectable in models)
        {
            // Act
            selectable.IsSelected = expected;

            //Assert
            Assert.Equal(expected, selectable.IsSelected);
        }
    }
}