using Blazored.Diagrams.Components.Models;

namespace Blazored.Diagrams.Test.Layers;

public class LayerTests
{
    [Fact]
    public void Test_UnselectAll()
    {
        //Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        var nestedGroup = ObjectFactory.Group;
        var nestedGroupNode = ObjectFactory.Node;
        layer.Nodes.Add(node);
        node.IsSelected = true;
        layer.Groups.Add(group);
        group.IsSelected = true;
        group.Groups.Add(nestedGroup);
        nestedGroup.IsSelected = true;
        group.Nodes.Add(nestedNode);
        nestedNode.IsSelected = true;
        nestedGroup.Nodes.Add(nestedGroupNode);
        nestedGroupNode.IsSelected = true;
        //Act
        layer.UnselectAll();
        //Assert
        Assert.False(node.IsSelected);
        Assert.False(group.IsSelected);
        Assert.False(nestedGroup.IsSelected);
        Assert.False(nestedNode.IsSelected);
        Assert.False(nestedGroupNode.IsSelected);
    }

    [Fact]
    public void Test_SelectAll()
    {
        //Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        var nestedGroup = ObjectFactory.Group;
        var nestedGroupNode = ObjectFactory.Node;
        layer.Nodes.Add(node);
        layer.Groups.Add(group);
        group.Groups.Add(nestedGroup);
        group.Nodes.Add(nestedNode);
        nestedGroup.Nodes.Add(nestedGroupNode);
        //Act
        layer.SelectAll();
        //Assert
        Assert.True(node.IsSelected);
        Assert.True(group.IsSelected);
        Assert.True(nestedGroup.IsSelected);
        Assert.True(nestedNode.IsSelected);
        Assert.True(nestedGroupNode.IsSelected);
    }


    [Fact]
    public void Test_RemoveLink()
    {
        //Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var port1 = ObjectFactory.Port;
        var port2 = ObjectFactory.Port;
        layer.Nodes.Add(node);
        layer.Nodes.Add(node2);
        node.Ports.Add(port1);
        node2.Ports.Add(port2);
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(port1, port2);

        //Act
        layer.RemoveLink(link);

        //Assert
        Assert.Empty(layer.AllLinks);
    }

    [Fact]
    public void Test_CreateLink_Throws_Exception_When_Source_Port_Is_Not_Part_Of_Layer()
    {
        //Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var port = ObjectFactory.Port;
        node.Ports.Add(port);
        layer.Nodes.Add(node);

        //Act
        //Assert
        Assert.Throws<InvalidOperationException>(() =>
            layer.CreateLink<DummyComponent>(node.Ports.First(), ObjectFactory.Port));
    }

    [Fact]
    public void Test_CreateLink_Throws_Exception_When_Target_Port_Is_Not_Part_Of_Layer()
    {
        //Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var port = ObjectFactory.Port;
        node.Ports.Add(port);
        layer.Nodes.Add(node);

        //Act
        //Assert
        Assert.Throws<InvalidOperationException>(() =>
            layer.CreateLink<DummyComponent>(ObjectFactory.Port, node.Ports.First()));
    }
}