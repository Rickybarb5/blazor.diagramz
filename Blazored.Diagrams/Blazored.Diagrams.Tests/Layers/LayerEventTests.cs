using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Test.Layers;

public class LayerEventTests
{
    [Fact]
    public Task Node_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        layer.OnNodeAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        layer.Nodes.Add(node);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Node_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var layer = ObjectFactory.Layer;
        var node = new Node<DefaultNodeComponent>();
        layer.Nodes.Add(node);
        layer.OnNodeRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        layer.Nodes.Remove(node);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Added_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = new Group<DefaultGroupComponent>();
        layer.OnGroupAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        layer.Groups.Add(group);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(group, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Removed_Triggers_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = new Group<DefaultGroupComponent>();
        layer.Groups.Add(group);
        layer.OnGroupRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        layer.Groups.Remove(group);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(group, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Add_Group_From_Group_Triggers_Layer_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = new Group<DefaultGroupComponent>();
        var nestedGroup = new Group<DefaultGroupComponent>();
        layer.Groups.Add(group);
        layer.OnGroupAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Groups.Add(nestedGroup);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact(Skip = "Skipped for now because we dont go deeper into the group to get the nested ones.")]
    public Task Removed_Nested_Group_From_Layer_Triggers_Layer_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        group.Groups.Add(nestedGroup);
        layer.Groups.Add(group);
        layer.OnGroupRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        layer.Groups.Remove(nestedGroup);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Removed_Nested_Group_From_Group_Triggers_Layer_Event()
    {
        // Arrange
        var eventTriggered = false;
        IGroup? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        group.Groups.Add(nestedGroup);
        layer.Groups.Add(group);
        layer.OnGroupRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Groups.Remove(nestedGroup);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Add_Node_From_Group_Triggers_Layer_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        layer.Groups.Add(group);
        layer.OnNodeAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Nodes.Add(nestedNode);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedNode, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Remove_Node_From_Group_Triggers_Layer_Event()
    {
        // Arrange
        var eventTriggered = false;
        INode? eventObject = null;
        var layer = ObjectFactory.Layer;
        var group = ObjectFactory.Group;
        var nestedNode = ObjectFactory.Node;
        layer.Groups.Add(group);
        group.Nodes.Add(nestedNode);
        layer.OnNodeRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };

        // Act
        group.Nodes.Remove(nestedNode);

        // Assert
        Assert.True(eventTriggered);
        Assert.Same(nestedNode, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public void Node_Port_Events_Propagate_To_Layer()
    {
        // Arrange
        var layer = ObjectFactory.Layer;
        var node = ObjectFactory.Node;
        var port = new Port<DummyComponent>();
        node.Ports.Add(port);
        layer.Nodes.Add(node);

        // Act
        // Assert
        Test_Port_Event_propagation(layer, port);
    }

    [Fact]
    public void Group_Port_Events_Propagate_To_Layer()
    {
        // Arrange
        var layer = ObjectFactory.Layer;
        var group = ObjectFactory.Group;
        var port = new Port<DummyComponent>();
        group.Ports.Add(port);
        layer.Groups.Add(group);

        // Act
        // Assert
        Test_Port_Event_propagation(layer, port);
    }

    private static void Test_Port_Event_propagation(ILayer layer, IPort port)
    {
        var args = new PointerEventArgs();
        var wheelArgs = new WheelEventArgs();
        var pointerDownEventTriggered = false;
        var pointerUpEventTriggered = false;
        var pointerEnterEventTriggered = false;
        var pointerLeaveEventTriggered = false;
        var pointerMoveEventTriggered = false;
        var pointerClickEventTriggered = false;
        var pointerDbClickEventTriggered = false;
        var wheelEventTriggered = false;

        layer.OnPortPointerDown += (_, _) => pointerDownEventTriggered = true;
        layer.OnPortPointerUp += (_, _) => pointerUpEventTriggered = true;
        layer.OnPortPointerEnter += (_, _) => pointerEnterEventTriggered = true;
        layer.OnPortPointerLeave += (_, _) => pointerLeaveEventTriggered = true;
        layer.OnPortPointerMove += (_, _) => pointerMoveEventTriggered = true;
        layer.OnPortClicked += (_, _) => pointerClickEventTriggered = true;
        layer.OnPortDoubleClicked += (_, _) => pointerDbClickEventTriggered = true;
        layer.OnPortWheel += (_, _) => wheelEventTriggered = true;

        // Act
        port.NotifyPointerDown(args);
        port.NotifyPointerEnter(args);
        port.NotifyPointerLeave(args);
        port.NotifyPointerMove(args);
        port.NotifyPointerUp(args);
        port.NotifyClick(args);
        port.NotifyDoubleClick(args);
        port.NotifyWheel(wheelArgs);

        // Assert
        Assert.True(pointerDownEventTriggered);
        Assert.True(pointerUpEventTriggered);
        Assert.True(pointerEnterEventTriggered);
        Assert.True(pointerLeaveEventTriggered);
        Assert.True(pointerMoveEventTriggered);
        Assert.True(pointerClickEventTriggered);
        Assert.True(pointerDbClickEventTriggered);
        Assert.True(wheelEventTriggered);
    }
}