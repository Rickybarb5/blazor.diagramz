using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Services.Serialization;

namespace Blazored.Diagrams.Test.Services;

public class DeserializationTests
{
    private const string Json =
        """{ "$id": "5b904a20-7c0d-418a-bd81-f1a3c7bc07e1", "object-type": "Diagram", "Layers": [ { "$id": "b9d8e20c-b53c-4606-b401-7086a6577363", "object-type": "Layer", "Id": "b9d8e20c-b53c-4606-b401-7086a6577363", "Nodes": [ { "$id": "e92566d7-c871-4ce7-9b76-42d305f0ab04", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "e92566d7-c871-4ce7-9b76-42d305f0ab04", "Width": 100, "Height": 100, "PositionX": 772, "PositionY": 64, "IsSelected": false, "IsVisible": true, "Ports": [ { "$id": "921079f1-b8ea-4db6-bbae-b6467c966420", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "921079f1-b8ea-4db6-bbae-b6467c966420", "Width": 30, "Height": 30, "PositionX": 757, "PositionY": 99, "IsVisible": true, "Alignment": 0, "Position": 0, "OutgoingLinks": [ { "$id": "cd06cb11-66a1-422e-8997-1b68e1f6c5fe", "object-type": "Link\u00601", "component-type": "LinkCurvedComponent", "Id": "cd06cb11-66a1-422e-8997-1b68e1f6c5fe", "TargetPositionX": 633, "TargetPositionY": 445, "IsSelected": false, "IsVisible": true }, { "$id": "10b30df9-5d30-4688-8995-424e8477b790", "object-type": "Link\u00601", "component-type": "LinkCurvedComponent", "Id": "10b30df9-5d30-4688-8995-424e8477b790", "TargetPositionX": 1040, "TargetPositionY": 484, "IsSelected": false, "IsVisible": true } ], "IncomingLinks": [ { "$id": "9b0f7ed0-6f4d-4c69-98f9-5c490266a270", "object-type": "Link\u00601", "component-type": "LinkCurvedComponent", "Id": "9b0f7ed0-6f4d-4c69-98f9-5c490266a270", "TargetPositionX": 772, "TargetPositionY": 114, "IsSelected": false, "IsVisible": true }, { "$id": "2118b225-8dcd-48ad-a3b3-5a47c58c7cc5", "object-type": "Link\u00601", "component-type": "LinkCurvedComponent", "Id": "2118b225-8dcd-48ad-a3b3-5a47c58c7cc5", "TargetPositionX": 772, "TargetPositionY": 114, "IsSelected": false, "IsVisible": true } ] } ] }, { "$id": "09cd3c3f-4648-4acd-b16e-39a48fa5dcbd", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "09cd3c3f-4648-4acd-b16e-39a48fa5dcbd", "Width": 100, "Height": 100, "PositionX": 346, "PositionY": 43, "IsSelected": false, "IsVisible": true, "Ports": [] } ], "Groups": [ { "$id": "bc59ff2f-501e-4554-81ce-6b0da346ccd5", "object-type": "Group\u00601", "component-type": "DefaultGroupComponent", "Id": "bc59ff2f-501e-4554-81ce-6b0da346ccd5", "Width": 383, "Height": 244, "PositionX": 250, "PositionY": 323, "IsSelected": false, "IsVisible": true, "Padding": 20, "Ports": [ { "$id": "59a9648e-2c73-4713-aa67-aafcbe63a0ac", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "59a9648e-2c73-4713-aa67-aafcbe63a0ac", "Width": 30, "Height": 30, "PositionX": 618, "PositionY": 430, "IsVisible": true, "Alignment": 0, "Position": 1, "OutgoingLinks": [], "IncomingLinks": [ { "$ref": "cd06cb11-66a1-422e-8997-1b68e1f6c5fe" } ] } ], "Nodes": [ { "$id": "801d92a0-6e65-4138-9e53-e124de514aa1", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "801d92a0-6e65-4138-9e53-e124de514aa1", "Width": 100, "Height": 100, "PositionX": 270, "PositionY": 343, "IsSelected": false, "IsVisible": true, "Ports": [ { "$id": "21fe2736-cee4-40cb-b9eb-a76cb65febfb", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "21fe2736-cee4-40cb-b9eb-a76cb65febfb", "Width": 30, "Height": 30, "PositionX": 355, "PositionY": 378, "IsVisible": true, "Alignment": 0, "Position": 1, "OutgoingLinks": [ { "$id": "766a4031-c60f-4177-b124-00d269ef7ada", "object-type": "Link\u00601", "component-type": "LinkCurvedComponent", "Id": "766a4031-c60f-4177-b124-00d269ef7ada", "TargetPositionX": 1560, "TargetPositionY": 734, "IsSelected": false, "IsVisible": true } ], "IncomingLinks": [] } ] }, { "$id": "bb9293ea-143d-43a1-987e-dcc3434bf345", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "bb9293ea-143d-43a1-987e-dcc3434bf345", "Width": 100, "Height": 100, "PositionX": 513, "PositionY": 447, "IsSelected": false, "IsVisible": true, "Ports": [ { "$id": "f7c6dc6c-f744-4f43-8ebf-185d94b80ab4", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "f7c6dc6c-f744-4f43-8ebf-185d94b80ab4", "Width": 30, "Height": 30, "PositionX": 498, "PositionY": 482, "IsVisible": true, "Alignment": 0, "Position": 0, "OutgoingLinks": [ { "$ref": "9b0f7ed0-6f4d-4c69-98f9-5c490266a270" } ], "IncomingLinks": [] } ] } ], "Groups": [] }, { "$id": "ac6fa1f1-dc85-48d1-94f4-a8e53ba8e01b", "object-type": "Group\u00601", "component-type": "DefaultGroupComponent", "Id": "ac6fa1f1-dc85-48d1-94f4-a8e53ba8e01b", "Width": 640, "Height": 640, "PositionX": 1040, "PositionY": 164, "IsSelected": false, "IsVisible": true, "Padding": 20, "Ports": [ { "$id": "7005739e-8f20-4b2d-bff9-6cbbecab7e1b", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "7005739e-8f20-4b2d-bff9-6cbbecab7e1b", "Width": 30, "Height": 30, "PositionX": 1025, "PositionY": 469, "IsVisible": true, "Alignment": 0, "Position": 0, "OutgoingLinks": [], "IncomingLinks": [ { "$ref": "10b30df9-5d30-4688-8995-424e8477b790" } ] } ], "Nodes": [ { "$id": "568ee874-b34b-4520-b4ad-f0d271fec0d5", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "568ee874-b34b-4520-b4ad-f0d271fec0d5", "Width": 100, "Height": 100, "PositionX": 1060, "PositionY": 184, "IsSelected": false, "IsVisible": true, "Ports": [] }, { "$id": "c01aa178-9139-497f-bc26-962c870b9af9", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "c01aa178-9139-497f-bc26-962c870b9af9", "Width": 100, "Height": 100, "PositionX": 1160, "PositionY": 284, "IsSelected": false, "IsVisible": true, "Ports": [] } ], "Groups": [ { "$id": "4bc1f8b1-37ea-40ba-b790-5b813296c5b8", "object-type": "Group\u00601", "component-type": "DefaultGroupComponent", "Id": "4bc1f8b1-37ea-40ba-b790-5b813296c5b8", "Width": 440, "Height": 340, "PositionX": 1240, "PositionY": 464, "IsSelected": true, "IsVisible": true, "Padding": 20, "Ports": [], "Nodes": [ { "$id": "4e63ef4b-45e2-424f-b02c-4a08ab4b4236", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "4e63ef4b-45e2-424f-b02c-4a08ab4b4236", "Width": 100, "Height": 100, "PositionX": 1260, "PositionY": 484, "IsSelected": false, "IsVisible": true, "Ports": [] }, { "$id": "f412d1dd-cd0a-4747-9603-6cbdc94cda6b", "object-type": "Node\u00601", "component-type": "DefaultNodeComponent", "Id": "f412d1dd-cd0a-4747-9603-6cbdc94cda6b", "Width": 100, "Height": 100, "PositionX": 1560, "PositionY": 684, "IsSelected": false, "IsVisible": true, "Ports": [ { "$id": "f7bee9af-79b9-40fd-8390-ad4cd10791d1", "object-type": "Port\u00601", "component-type": "DefaultPortComponent", "Id": "f7bee9af-79b9-40fd-8390-ad4cd10791d1", "Width": 30, "Height": 30, "PositionX": 1545, "PositionY": 719, "IsVisible": true, "Alignment": 0, "Position": 0, "OutgoingLinks": [ { "$ref": "2118b225-8dcd-48ad-a3b3-5a47c58c7cc5" } ], "IncomingLinks": [ { "$ref": "766a4031-c60f-4177-b124-00d269ef7ada" } ] } ] } ], "Groups": [] } ] } ], "IsVisible": true } ], "CurrentLayer": { "$ref": "b9d8e20c-b53c-4606-b401-7086a6577363" }, "Id": "5b904a20-7c0d-418a-bd81-f1a3c7bc07e1", "DiagramOptions": { "$id": "db471cf0-f076-4b89-85ee-45f3c8a0ad5e", "StyleOptions": { "$id": "04011eb3-687a-486e-81fe-86abf0ae78f9", "ContainerStyle": "width:100%;height:100%;overflow:hidden;", "GridEnabled": true, "CellSize": 20, "GridStyle": "background-image:linear-gradient(to right, rgba(0, 0, 0, 0.1) 1px, transparent 1px),linear-gradient(to bottom, rgba(0, 0, 0, 0.1) 1px, transparent 1px); background-size:20px 20px;" }, "BehaviourOptions": { "$id": "957045e8-9dad-4ed9-b074-ca7d39f491a7", "ZoomOptions": { "$id": "cd94e9c4-7719-4622-99a1-dc0ec4904c8a", "Enabled": true, "MinZoom": 0.05, "MaxZoom": 3, "ZoomStep": 0.05 }, "LinkCreationOptions": { "$id": "f16323e1-2e8a-4823-a3cb-92b176561812", "Enabled": true, "DefaultLinkComponentType": "LinkCurvedComponent" }, "ModelMoveOptions": { "$id": "b453e0fe-3e58-42e0-80c1-c087c373e40e", "Enabled": true }, "PanningOptions": { "$id": "331f8111-66d5-4e5e-8c6d-dfdadda0f729", "Enabled": true }, "SelectionOptions": { "$id": "2c5491fb-878e-4f56-b238-aaa0ce6ba239", "MultiSelectEnabled": false, "SelectionEnabled": true }, "DeleteOptions": { "$id": "d4f16b73-9344-44ee-8b82-844b9789249d", "Enabled": true, "DeleteKeyCode": "Delete" }, "VirtualizationOptions": { "$id": "2623a64d-d93c-4bdf-9aec-cf437ec5534e", "Enabled": true } } }, "Zoom": 1, "PanX": 0, "PanY": 0 }""";

    private DiagramSerializer Instance => new(null);

    [Fact]
    public void Deserialize_Layer()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        diagram.Layers.Add(ObjectFactory.Layer);
        var json = Instance.SaveAsJson(diagram);

        //Act
        var result = service.LoadFromJson<Diagram>(json);

        //Assert
        Assert.NotNull(result);
        Assert.Equal(diagram.Layers.Count, result.Layers.Count);
        AssertDiagram(diagram, result);
    }

    [Fact]
    public void Deserialize_Current_Layer()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        diagram.Layers.Add(ObjectFactory.Layer);
        var json = Instance.SaveAsJson(diagram);

        //Act
        var result = service.LoadFromJson<Diagram>(json);

        //Assert
        Assert.NotNull(result);
        Assert.Equal(diagram.Layers.Count, result.Layers.Count);
        var currentLayer = result.Layers.FirstOrDefault(x => x.Id == result.CurrentLayer.Id);
        Assert.True(ReferenceEquals(currentLayer, result.CurrentLayer));
    }

    [Fact]
    public void Deserialize_Node()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        var node = new Node<DefaultNodeComponent>();
        diagram.AddNode(node);
        var json = Instance.SaveAsJson(diagram);

        //Act
        var result = service.LoadFromJson<Diagram>(json);

        //Assert
        Assert.NotNull(result);
        AssertDiagram(diagram, result);
    }

    [Fact]
    public void Deserialize_Group()
    {
        // Arrange
        var service = Instance;

        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        diagram.AddGroup(group);
        var json = Instance.SaveAsJson(diagram);

        // Act
        var result = service.LoadFromJson<Diagram>(json);

        // Assert
        Assert.NotNull(result);
        AssertDiagram(diagram, result);
    }

    [Fact]
    public void Deserialize_Group_And_Nested_Node()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        var nestedNode = new Node<DefaultNodeComponent>();
        diagram.AddGroup(group);
        group.Nodes.Add(nestedNode);
        var json = Instance.SaveAsJson(diagram);

        // Act
        var result = service.LoadFromJson<Diagram>(json);

        // Assert
        Assert.NotNull(result);
        Assert.Equivalent(group.Nodes.Count(), result.AllGroups.First().Nodes.Count);
        Assert.Equivalent(group.AllNodes.Count, result.AllGroups.First().AllNodes.Count);
        AssertDiagram(diagram, result);
    }

    [Fact]
    public void Deserialize_Nodes_And_Ports()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        var nestedNode = new Node<DefaultNodeComponent>();
        var node = new Node<DefaultNodeComponent>();
        diagram.AddGroup(group);
        diagram.AddNode(node);
        group.Nodes.Add(nestedNode);
        group.Ports.Add(new Port<DefaultPortComponent>());
        nestedNode.Ports.Add(new Port<DefaultPortComponent>());
        node.Ports.Add(new Port<DefaultPortComponent>());
        var json = Instance.SaveAsJson(diagram);

        // Act
        var result = service.LoadFromJson<Diagram>(json);

        // Assert
        Assert.NotNull(result);
        Assert.Equivalent(group.Nodes.Count(), result.AllGroups.First().Nodes.Count);
        Assert.Equivalent(group.AllNodes.Count, result.AllGroups.First().AllNodes.Count);
        AssertDiagram(diagram, result);
    }

    [Fact]
    public void Deserialize_Complex_Json()
    {
        //Arrange
        //Act
        var result = Instance.LoadFromJson<Diagram>(Json);
        //Assert
        Assert.NotNull(result);
    }

    [Fact]
    public void Deserialize_FullDiagram()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        var nestedNode = new Node<DefaultNodeComponent>();
        var node1 = new Node<DefaultNodeComponent>();
        var node2 = new Node<DefaultNodeComponent>();
        var port1 = new Port<DefaultPortComponent>();
        var port2 = new Port<DefaultPortComponent>();
        node2.Ports.Add(port1);
        nestedNode.Ports.Add(port2);
        diagram.AddGroup(group);
        diagram.AddNode(node1);
        diagram.AddNode(node2);
        group.Nodes.Add(nestedNode);
        diagram.CreateLink<LinkCurvedComponent>(port1, port2);
        var json = Instance.SaveAsJson(diagram);

        // Act
        var result = service.LoadFromJson<Diagram>(json);

        // Assert
        Assert.NotNull(result);
        Assert.Equivalent(group.Nodes.Count(), result.AllGroups.First().Nodes.Count);
        Assert.Equivalent(group.AllNodes.Count, result.AllGroups.First().AllNodes.Count);
        AssertDiagram(diagram, result);
    }

    /// <summary>
    ///     Asserts that the ids were deserialized correctly
    /// </summary>
    /// <param name="expected"></param>
    /// <param name="actual"></param>
    private void AssertDiagram(IDiagram expected, IDiagram actual)
    {
        Assert.NotNull(actual);
        AssertId(expected, actual);
        AssertZoom(expected, actual);
        AssertPan(expected, actual);

        foreach (var actualLayer in actual.Layers)
        {
            var expectedLayer = expected.Layers.FirstOrDefault(x => x.Id == actualLayer.Id);
            Assert.NotNull(expectedLayer);
            AssertId(expectedLayer, actualLayer);
            AssertVisibility(expectedLayer, actualLayer);
        }

        foreach (var actualGroup in actual.AllGroups)
        {
            var expectedGroup = expected.AllGroups.FirstOrDefault(x => x.Id == actualGroup.Id);
            AssertGroup(expectedGroup, actualGroup);
        }

        foreach (var actualNode in actual.AllNodes)
        {
            var expectedNode = expected.AllNodes.FirstOrDefault(x => x.Id == actualNode.Id);
            AssertNode(expectedNode, actualNode);
        }

        foreach (var actualPort in actual.AllPorts)
        {
            var expectedPort = expected.AllPorts.FirstOrDefault(x => x.Id == actualPort.Id);
            AssertPort(expectedPort, actualPort);
        }

        foreach (var actualLink in actual.AllLinks)
        {
            var expectedLink = expected.AllLinks.FirstOrDefault(x => x.Id == actualLink.Id);
            AssertLink(expectedLink, actualLink);

            //Check link port dependencies.
            Assert.Equal(expectedLink!.SourcePort.Id, actualLink.SourcePort.Id);
            Assert.Equal(expectedLink.TargetPort?.Id, actualLink.TargetPort?.Id);
            var sourcePort = actual.AllPorts.FirstOrDefault(x => x.Id == actualLink.SourcePort.Id);
            var targetPort = actual.AllPorts.FirstOrDefault(x => x.Id == actualLink.TargetPort?.Id);
            Assert.True(ReferenceEquals(actualLink.SourcePort, sourcePort));
            Assert.True(ReferenceEquals(actualLink.TargetPort, targetPort));
        }

        foreach (var actualLayer in actual.Layers)
        {
            var expectedLayer = expected.Layers.FirstOrDefault(x => x.Id == actualLayer.Id);
            AssertLayer(expectedLayer, actualLayer);
        }
    }

    private void AssertNode(INode? expectedNode, INode actualNode)
    {
        Assert.NotNull(expectedNode);
        AssertId(expectedNode, actualNode);
        AssertVisibility(expectedNode, actualNode);
        AssertPosition(expectedNode, actualNode);
        AssertSelectable(expectedNode, actualNode);
        AssertSize(expectedNode, actualNode);
    }

    private void AssertGroup(IGroup? expectedGroup, IGroup actualGroup)
    {
        Assert.NotNull(expectedGroup);
        AssertId(expectedGroup, actualGroup);
        AssertVisibility(expectedGroup, actualGroup);
        AssertPosition(expectedGroup, actualGroup);
        AssertPadding(expectedGroup, actualGroup);
        AssertSelectable(expectedGroup, actualGroup);
        AssertSize(expectedGroup, actualGroup);
    }

    private void AssertLayer(ILayer? expectedLink, ILayer actualLayer)
    {
        Assert.NotNull(expectedLink);
        AssertId(expectedLink, actualLayer);
        AssertVisibility(expectedLink, actualLayer);
    }

    private void AssertLink(ILink? expectedLink, ILink actualLink)
    {
        Assert.NotNull(expectedLink);
        AssertId(expectedLink, actualLink);
        AssertVisibility(expectedLink, actualLink);
        AssertSelectable(expectedLink, actualLink);
    }


    private void AssertPort(IPort? expectedPort, IPort actualPort)
    {
        Assert.NotNull(expectedPort);
        Assert.Equal(expectedPort.Position, actualPort.Position);
        Assert.Equal(expectedPort.Alignment, actualPort.Alignment);
        AssertId(expectedPort, actualPort);
        AssertVisibility(expectedPort, actualPort);
        AssertPosition(expectedPort, actualPort);
        AssertSize(expectedPort, actualPort);
    }

    private void AssertVisibility(IVisible expected, IVisible actual)
    {
        Assert.Equal(expected.IsVisible, actual.IsVisible);
    }

    private void AssertPosition(IPosition expected, IPosition actual)
    {
        Assert.Equal(expected.PositionX, actual.PositionX);
        Assert.Equal(expected.PositionY, actual.PositionY);
    }

    private void AssertPadding(IPadding expected, IPadding actual)
    {
        Assert.Equal(expected.Padding, actual.Padding);
    }

    private void AssertSelectable(ISelectable expected, ISelectable actual)
    {
        Assert.Equal(expected.IsSelected, actual.IsSelected);
    }

    private void AssertZoom(IZoomable expected, IZoomable actual)
    {
        Assert.Equal(expected.Zoom, actual.Zoom);
    }

    private void AssertSize(ISize expected, ISize actual)
    {
        Assert.Equal(expected.Width, actual.Width);
        Assert.Equal(expected.Height, actual.Height);
    }

    private void AssertPan(IPan expected, IPan actual)
    {
        Assert.Equal(expected.PanX, actual.PanX);
        Assert.Equal(expected.PanY, actual.PanY);
    }

    private void AssertId(IId expected, IId actual)
    {
        Assert.Equal(expected.Id, actual.Id);
    }
}