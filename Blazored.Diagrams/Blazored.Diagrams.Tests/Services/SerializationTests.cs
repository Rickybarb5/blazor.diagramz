using System.Text.Json;
using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Services.Serialization;

namespace Blazored.Diagrams.Test.Services;

public class SerializationTests
{
    private DiagramSerializer Instance => new(null);

    [Fact]
    public void Deserialize_FullDiagram()
    {
        // Arrange
        var service = Instance;
        var diagram = ObjectFactory.Diagram;
        var group = new Group<DefaultGroupComponent>();
        var nestedNode = new Node<DefaultNodeComponent>();
        var node1 = new Node<DefaultNodeComponent>();
        var node2 = new Node<DefaultNodeComponent>();
        var port1 = new Port<DefaultPortComponent>();
        var port2 = new Port<DefaultPortComponent>();
        node2.Ports.Add(port1);
        nestedNode.Ports.Add(port2);
        diagram.AddGroup(group);
        diagram.AddNode(node1);
        diagram.AddNode(node2);
        group.Nodes.Add(nestedNode);
        diagram.CreateLink<LinkCurvedComponent>(port1, port2);
        //Act
        var json = Instance.SaveAsJson(diagram);

        // Assert
        // Deserialize to dictionary
        var jsonDocument = JsonDocument.Parse(json);
        var rootElement = jsonDocument.RootElement;
        AssertDiagram(diagram, rootElement);
    }

    private void AssertDiagram(Diagram diagram, JsonElement rootElement)
    {
        AssertId(rootElement, diagram);
        AssertPan(rootElement, diagram);
        AssertZoom(rootElement, diagram);
        AssertId(rootElement, diagram);
        AssertId(rootElement, diagram);
        rootElement.TryGetProperty(nameof(diagram.Layers), out var layerProperty);
        AssertLayerArray(layerProperty, diagram.Layers);
    }

    private bool IsReferenceObject(JsonElement element)
    {
        return element.TryGetProperty("$ref", out var item);
    }

    private void AssertLayerArray(JsonElement layerProperty, ObservableList<ILayer> diagramLayers)
    {
        var enumerator = layerProperty.EnumerateArray();
        Assert.Equal(diagramLayers.Count, enumerator.Count());
        for (var i = 0; i < diagramLayers.Count; i++)
        {
            AssertLayer(diagramLayers.ElementAt(i), enumerator.ElementAt(i));
        }
    }

    private void AssertLayer(ILayer layer, JsonElement jsonElement)
    {
        if (IsReferenceObject(jsonElement))
        {
            return;
        }

        AssertId(jsonElement, layer);
        AssertVisible(jsonElement, layer);
        jsonElement.TryGetProperty(nameof(ILayer.Nodes), out var nodeArrayProperty);
        jsonElement.TryGetProperty(nameof(ILayer.Groups), out var groupArrayProperty);
        AssertNodeArray(nodeArrayProperty, layer.Nodes);
        AssertGroupArray(groupArrayProperty, layer.Groups);
    }

    private void AssertGroupArray(JsonElement groupArrayProperty, ObservableList<IGroup> groups)
    {
        var enumerator = groupArrayProperty.EnumerateArray();
        Assert.Equal(groups.Count, enumerator.Count());
        for (var i = 0; i < groups.Count; i++) AssertGroup(groups.ElementAt(i), enumerator.ElementAt(i));
    }

    private void AssertGroup(IGroup group, JsonElement element)
    {
        if (IsReferenceObject(element))
        {
            return;
        }

        AssertId(element, group);
        AssertVisible(element, group);
        AssertSelected(element, group);
        AssertPosition(element, group);
        AssertSize(element, group);
        element.TryGetProperty(nameof(IGroup.Ports), out var portArray);
        element.TryGetProperty(nameof(IGroup.Groups), out var groupArray);
        element.TryGetProperty(nameof(IGroup.Nodes), out var nodeArray);
        AssertPortsArray(portArray, group.Ports);
        AssertNodeArray(nodeArray, group.Nodes);
        AssertGroupArray(groupArray, group.Groups);
    }

    private void AssertNodeArray(JsonElement nodeArrayProperty, ObservableList<INode> nodes)
    {
        var enumerator = nodeArrayProperty.EnumerateArray();
        Assert.Equal(nodes.Count, enumerator.Count());
        for (var i = 0; i < nodes.Count; i++) AssertNode(nodes.ElementAt(i), enumerator.ElementAt(i));
    }

    private void AssertNode(INode node, JsonElement element)
    {
        if (IsReferenceObject(element))
        {
            return;
        }

        AssertId(element, node);
        AssertVisible(element, node);
        AssertSelected(element, node);
        AssertPosition(element, node);
        AssertSize(element, node);
        element.TryGetProperty(nameof(INode.Ports), out var portArray);
        AssertPortsArray(portArray, node.Ports);
    }

    private void AssertPortsArray(JsonElement portArray, ObservableList<IPort> ports)
    {
        var enumerator = portArray.EnumerateArray();
        Assert.Equal(ports.Count, enumerator.Count());
        for (var i = 0; i < ports.Count; i++) AssertPort(ports.ElementAt(i), enumerator.ElementAt(i));
    }

    private void AssertPort(IPort port, JsonElement jsonElement)
    {
        if (IsReferenceObject(jsonElement))
        {
            return;
        }

        AssertId(jsonElement, port);
        AssertVisible(jsonElement, port);
        AssertPosition(jsonElement, port);

        jsonElement.TryGetProperty(nameof(IPort.IncomingLinks), out var incomingLinks);
        jsonElement.TryGetProperty(nameof(IPort.OutgoingLinks), out var outgoingLinks);
        AssertLinkArray(incomingLinks, port.IncomingLinks);
        AssertLinkArray(outgoingLinks, port.OutgoingLinks);
    }

    private void AssertLinkArray(JsonElement incomingLinks, ObservableList<ILink> portIncomingLinks)
    {
        var enumerator = incomingLinks.EnumerateArray();
        Assert.Equal(portIncomingLinks.Count, enumerator.Count());
        for (var i = 0; i < portIncomingLinks.Count; i++)
            AssertLink(portIncomingLinks.ElementAt(i), enumerator.ElementAt(i));
    }

    private void AssertLink(ILink link, JsonElement jsonElement)
    {
        if (IsReferenceObject(jsonElement))
        {
            return;
        }

        AssertId(jsonElement, link);
        AssertVisible(jsonElement, link);
        AssertSelected(jsonElement, link);
    }


    private void AssertId(JsonElement element, IId obj)
    {
        Assert.True(element.TryGetProperty(nameof(IId.Id), out var property));
        Assert.Equal(obj.Id, property.GetGuid());
    }

    private void AssertVisible(JsonElement element, IVisible obj)
    {
        Assert.True(element.TryGetProperty(nameof(IVisible.IsVisible), out var property));
        Assert.Equal(obj.IsVisible, property.GetBoolean());
    }

    private void AssertSelected(JsonElement element, ISelectable objSelectable)
    {
        Assert.True(element.TryGetProperty(nameof(ISelectable.IsSelected), out var property));
        Assert.Equal(objSelectable.IsSelected, property.GetBoolean());
    }

    private void AssertPosition(JsonElement element, IPosition obj)
    {
        Assert.True(element.TryGetProperty(nameof(IPosition.PositionX), out var propertyX));
        Assert.True(element.TryGetProperty(nameof(IPosition.PositionY), out var propertyY));
        Assert.Equal(obj.PositionX, propertyX.GetInt32());
        Assert.Equal(obj.PositionY, propertyY.GetInt32());
    }

    private void AssertSize(JsonElement element, ISize obj)
    {
        Assert.True(element.TryGetProperty(nameof(ISize.Width), out var propertyWidth));
        Assert.True(element.TryGetProperty(nameof(ISize.Height), out var propertyHeight));
        Assert.Equal(obj.Width, propertyWidth.GetInt32());
        Assert.Equal(obj.Height, propertyHeight.GetInt32());
    }

    private void AssertPan(JsonElement element, IPan obj)
    {
        Assert.True(element.TryGetProperty(nameof(IPan.PanX), out var panxProperty));
        Assert.True(element.TryGetProperty(nameof(IPan.PanY), out var panYProperty));
        Assert.Equal(obj.PanX, panxProperty.GetInt32());
        Assert.Equal(obj.PanY, panYProperty.GetInt32());
    }

    private void AssertZoom(JsonElement element, IZoomable obj)
    {
        Assert.True(element.TryGetProperty(nameof(IZoomable.Zoom), out var property));
        Assert.Equal(obj.Zoom, property.GetDouble());
    }
}