namespace Blazored.Diagrams.Test.Groups;

public class GroupMoveBehaviourTests
{
    [Fact]
    public Task Test_Position_Change_Triggers_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var port = ObjectFactory.Port;
        var node = ObjectFactory.Node;
        group.Ports.Add(port);
        group.Nodes.Add(node);
        var oldPositionX = port.PositionX;
        var oldPositionY = port.PositionY;

        //Act
        group.SetPosition(500, 500);

        //Assert
        Assert.NotEqual(oldPositionX, port.PositionX);
        Assert.NotEqual(oldPositionY, port.PositionY);

        return Task.CompletedTask;
    }
}