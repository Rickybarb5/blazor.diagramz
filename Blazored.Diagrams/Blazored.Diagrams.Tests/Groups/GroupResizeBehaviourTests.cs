namespace Blazored.Diagrams.Test.Groups;

public class ResizeBehaviourTests
{
    [Fact]
    public Task Node_Add_Triggers_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;
        var eventTriggered = false;
        var width = 100;
        var height = 500;
        node.SetSize(width, height);
        group.OnRedraw += () => eventTriggered = true;

        //Act
        group.Nodes.Add(node);
        //Assert
        Assert.Equal(140, group.Width);
        Assert.Equal(540, group.Height);
        Assert.True(eventTriggered);

        return Task.CompletedTask;
    }

    [Fact]
    public Task Node_Resize_Triggers_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;
        var eventTriggered = false;
        var width = 100;
        var height = 500;
        group.Nodes.Add(node);
        group.OnRedraw += () => eventTriggered = true;

        //Act
        node.SetSize(width, height);
        //Assert
        Assert.Equal(140, group.Width);
        Assert.Equal(540, group.Height);
        Assert.True(eventTriggered);

        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Add_Triggers_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        var node1 = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var eventTriggered = false;
        nestedGroup.Nodes.Add(node1);
        nestedGroup.Nodes.Add(node2);
        var width = 100;
        var height = 500;
        node1.SetSize(width, height);
        node2.SetSize(width, height);

        group.OnRedraw += () => eventTriggered = true;

        //Act
        group.Groups.Add(nestedGroup);

        //Assert
        Assert.True(eventTriggered);

        return Task.CompletedTask;
    }

    [Fact]
    public Task Nested_Group_Add_Triggers_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var nestedGroup = ObjectFactory.Group;
        var node1 = ObjectFactory.Node;
        var eventTriggered = false;
        nestedGroup.Nodes.Add(node1);
        var width = 100;
        var height = 500;
        node1.SetSize(width, height);

        group.OnRedraw += () => eventTriggered = true;

        //Act
        group.Groups.Add(nestedGroup);

        //Assert
        Assert.True(eventTriggered);

        return Task.CompletedTask;
    }
}