using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Groups.Behaviours;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Groups;

public class GroupTests
{
    [Fact]
    public void Test_AddNode()
    {
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;

        group.Nodes.Add(node);

        Assert.Contains(node, group.Nodes);
        Assert.Equal(1, group.Nodes.Count);
    }

    [Fact]
    public void Test_AddPort()
    {
        var group = ObjectFactory.Group;
        var port = new Port<DummyComponent>();

        group.Ports.Add(port);

        Assert.True(group.Ports.Count == 1);
        Assert.Equal(port, group.Ports[0]);
    }

    [Fact]
    public void Test_RemovePort()
    {
        var group = ObjectFactory.Group;
        var port = new Port<DummyComponent>();
        group.Ports.Add(port);

        //Act
        group.Ports.Remove(port);

        //Assert
        Assert.True(group.Ports.Count == 0);
    }

    [Fact]
    public void Group_Has_Default_Behaviours()
    {
        //Arrange
        var group = ObjectFactory.Group;
        //Assert
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(DefaultGroupBehaviour));
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(GroupEventPipelineBehaviour));
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(GroupResizeBehaviour));
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(GroupMoveBehaviour));
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(RedrawBehaviour<IGroup>));
        Assert.Equal(8, group.Behaviours.All.Count);
    }

    [Fact]
    public void Only_Allows_One_Behaviour_Instance()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var behaviour = ObjectFactory.Behaviour;
        //Act
        group.Behaviours.Add(behaviour);
        group.Behaviours.Add(behaviour);

        //Assert
        Assert.Contains(group.Behaviours.All, x => x.GetType() == typeof(TestBehaviour));
        Assert.Equal(1, group.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void Removes_Behaviour()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var behaviour = ObjectFactory.Behaviour;
        group.Behaviours.Add(behaviour);
        //Act
        group.Behaviours.Remove<TestBehaviour>();

        //Assert
        Assert.Equal(0, group.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public Task Cannot_Add_Group_To_Itself()
    {
        //Arrange
        var group = ObjectFactory.Group;
        //Act
        var action = () => group.Groups.Add(group);

        //Assert
        Assert.Throws<InvalidOperationException>(action);
        return Task.CompletedTask;
    }

    [Fact]
    public void Padding_Is_Changed()
    {
        //Arrange
        const int expected = 50;
        var group = ObjectFactory.Group;
        //Act
        group.Padding = expected;

        //Assert
        Assert.Equal(expected, group.Padding);
    }
}