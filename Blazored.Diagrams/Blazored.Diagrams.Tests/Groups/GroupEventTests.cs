using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Groups;

public class GroupEventTests
{
    [Fact]
    public Task Node_Added_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;
        INode? eventObject = null;
        var eventTriggered = false;
        group.OnNodeAdded += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };
        //Act
        group.Nodes.Add(node);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Node_Removed_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var node = ObjectFactory.Node;
        INode? eventObject = null;
        var eventTriggered = false;
        group.OnNodeRemoved += n =>
        {
            eventTriggered = true;
            eventObject = n;
        };
        group.Nodes.Add(node);
        //Act
        group.Nodes.Remove(node);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(node, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Added_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var innerGroup = ObjectFactory.Group;
        IGroup? eventObject = null;
        var eventTriggered = false;
        group.OnGroupAdded += g =>
        {
            eventTriggered = true;
            eventObject = g;
        };
        //Act
        group.Groups.Add(innerGroup);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(innerGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Removed_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var innerGroup = ObjectFactory.Group;
        IGroup? eventObject = null;
        var eventTriggered = false;
        group.OnGroupRemoved += g =>
        {
            eventTriggered = true;
            eventObject = g;
        };
        group.Groups.Add(innerGroup);
        //Act
        group.Groups.Remove(innerGroup);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(innerGroup, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Port_Added_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        IPort? eventObject = null;
        var eventTriggered = false;
        var port = new Port<DefaultPortComponent>();
        group.OnPortAdded += p =>
        {
            eventTriggered = true;
            eventObject = p;
        };
        //Act
        group.Ports.Add(port);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Port_Removed_Triggers_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        IPort? eventObject = null;
        var eventTriggered = false;
        var port = new Port<DefaultPortComponent>();
        group.Ports.Add(port);
        group.OnPortRemoved += p =>
        {
            eventTriggered = true;
            eventObject = p;
        };
        //Act
        group.Ports.Remove(port);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(port, eventObject);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Position_Change_Triggers_Events()
    {
        //Arrange
        var beforePositionChangedEventTriggered = false;
        var positionChangedEventTriggered = false;
        var node = ObjectFactory.Group;
        node.OnBeforePositionChanged += _ => { beforePositionChangedEventTriggered = true; };
        node.OnPositionChanged += _ => { positionChangedEventTriggered = true; };
        //Act
        node.SetPosition(123, 456);
        //Assert
        Assert.True(beforePositionChangedEventTriggered);
        Assert.True(positionChangedEventTriggered);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Size_Change_Triggers_Events()
    {
        //Arrange
        var eventTriggered = false;
        var node = ObjectFactory.Group;
        node.OnSizeChanged += _ => { eventTriggered = true; };
        //Act
        node.SetSize(123, 456);
        //Assert
        Assert.True(eventTriggered);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Added_Triggers_Nested_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var innerGroup = ObjectFactory.Group;
        group.Groups.Add(innerGroup);
        var nestedGroup = ObjectFactory.Group;
        IGroup? parentGroup = null;
        IGroup? addedGroup = null;
        var eventTriggered = false;
        group.OnNestedGroupAdded += (p, g) =>
        {
            eventTriggered = true;
            parentGroup = p;
            addedGroup = g;
        };
        //Act
        innerGroup.Groups.Add(nestedGroup);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(innerGroup, parentGroup);
        Assert.Same(nestedGroup, addedGroup);
        return Task.CompletedTask;
    }

    [Fact]
    public Task Group_Removed_Triggers_Nested_Event()
    {
        //Arrange
        var group = ObjectFactory.Group;
        var innerGroup = ObjectFactory.Group;
        group.Groups.Add(innerGroup);
        var nestedGroup = ObjectFactory.Group;
        innerGroup.Groups.Add(nestedGroup);
        IGroup? parentGroup = null;
        IGroup? addedGroup = null;
        var eventTriggered = false;
        group.OnNestedGroupRemoved += (p, g) =>
        {
            eventTriggered = true;
            parentGroup = p;
            addedGroup = g;
        };
        //Act
        innerGroup.Groups.Remove(nestedGroup);
        //Assert
        Assert.True(eventTriggered);
        Assert.Same(innerGroup, parentGroup);
        Assert.Same(nestedGroup, addedGroup);
        return Task.CompletedTask;
    }
}