using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Ports;

public class PortMoveBehaviourTests
{
    [Fact]
    public void Test_Behaviour_When_Parent_Position_Changes()
    {
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var list = new List<IPortContainer>() { node, group };
        foreach (var container in list)
        {
            var port = ObjectFactory.Port;
            container.Ports.Add(port);
            container.SetPosition(100, 100);
            Assert.NotEqual(0, port.PositionX);
            Assert.NotEqual(0, port.PositionY);
        }
    }

    [Fact]
    public void Test_Behaviour_When_Parent_Size_Changes()
    {
        var node = ObjectFactory.Node;
        var group = ObjectFactory.Group;
        var list = new List<IPortContainer>() { node, group };
        foreach (var container in list)
        {
            var port = ObjectFactory.Port;
            port.Position = PortPosition.CenterParent;
            container.Ports.Add(port);
            container.SetSize(300, 300);
            Assert.NotEqual(0, port.PositionX);
            Assert.NotEqual(0, port.PositionY);
        }
    }
}