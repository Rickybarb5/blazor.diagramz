﻿using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Ports;

public class PortTests
{
    [Theory]
    [InlineData(PortPosition.Top, PortAlignment.Start, -50, -50)]
    [InlineData(PortPosition.Top, PortAlignment.Center, 0, -50)]
    [InlineData(PortPosition.Top, PortAlignment.End, 50, -50)]
    [InlineData(PortPosition.Left, PortAlignment.Start, -50, -50)]
    [InlineData(PortPosition.Left, PortAlignment.Center, -50, 0)]
    [InlineData(PortPosition.Left, PortAlignment.End, -50, 50)]
    [InlineData(PortPosition.Bottom, PortAlignment.Start, -50, 50)]
    [InlineData(PortPosition.Bottom, PortAlignment.Center, 0, 50)]
    [InlineData(PortPosition.Bottom, PortAlignment.End, 50, 50)]
    [InlineData(PortPosition.Right, PortAlignment.Start, 50, -50)]
    [InlineData(PortPosition.Right, PortAlignment.Center, 50, 0)]
    [InlineData(PortPosition.Right, PortAlignment.End, 50, 50)]
    public void Test_Position_Calculation(PortPosition position, PortAlignment alignment, int expectedX,
        int expectedY)
    {
        var node = ObjectFactory.Node;
        node.SetSize(100, 100);
        var port = new Port<DummyComponent>();
        port.SetSize(100, 100);
        port.Alignment = alignment;
        port.Position = position;
        node.Ports.Add(port);

        var (positionX, positionY) = port.CalculatePosition();

        Assert.Equal(expectedX, positionX);
        Assert.Equal(expectedY, positionY);
    }

    [Fact]
    public void Only_Allows_One_Behaviour_Instance()
    {
        //Arrange
        var port = new Port<DummyComponent>();
        var behaviour = ObjectFactory.Behaviour;
        //Act
        port.Behaviours.Add(behaviour);
        port.Behaviours.Add(behaviour);

        //Assert
        Assert.Contains(port.Behaviours.All, x => x.GetType() == typeof(TestBehaviour));
        Assert.Equal(1, port.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void Removes_Behaviour()
    {
        //Arrange
        var port = new Port<DummyComponent>();
        var behaviour = ObjectFactory.Behaviour;
        port.Behaviours.Add(behaviour);
        //Act
        port.Behaviours.Remove<TestBehaviour>();

        //Assert
        Assert.Equal(0, port.Behaviours.All.Count(b => b.GetType() == typeof(TestBehaviour)));
    }

    [Fact]
    public void CanConnectTo_Default_Behaviour_Does_Not_Connect_To_Target_Port()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var sourcePort = new Port<DefaultPortComponent>();
        var targetPort = new Port<DefaultPortComponent>();
        node.Ports.Add(sourcePort);
        node.Ports.Add(targetPort);

        // Act
        var result = sourcePort.CanConnectTo(targetPort);
        // Assert
        Assert.False(result);
    }

    [Fact]
    public void CanConnectTo_Default_Behaviour_Connects_To_Target_Port()
    {
        //Arrange
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var sourcePort = new Port<DefaultPortComponent>();
        var targetPort = new Port<DefaultPortComponent>();
        node.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);

        // Act
        var result = sourcePort.CanConnectTo(targetPort);
        // Assert
        Assert.True(result);
    }
}