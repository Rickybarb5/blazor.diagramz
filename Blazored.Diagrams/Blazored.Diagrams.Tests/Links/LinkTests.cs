using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Extensions;

namespace Blazored.Diagrams.Test.Links;

public class LinkTests
{
    [Fact]
    public void Test_Constructor()
    {
        var node1 = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        var targetPort = ObjectFactory.Port;
        node1.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);

        Assert.Same(link.SourcePort, sourcePort);
        Assert.Same(link.TargetPort, targetPort);
        Assert.Equal(link.TargetPositionX, targetPort.GetCenterCoordinates().CenterX);
        Assert.Equal(link.TargetPositionY, targetPort.GetCenterCoordinates().CenterY);
    }

    [Fact]
    public void Test_Set_Target_Port()
    {
        var node1 = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var node3 = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        var targetPort = ObjectFactory.Port;
        var targetPort2 = ObjectFactory.Port;
        node1.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        node3.Ports.Add(targetPort2);
        targetPort2.SetPosition(100, 100);
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);
        link.TargetPort = targetPort2;

        Assert.Equal(link.TargetPositionX, targetPort2.GetCenterCoordinates().CenterX);
        Assert.Equal(link.TargetPositionY, targetPort2.GetCenterCoordinates().CenterY);
    }

    [Fact]
    public void Test_Set_Target_Position()
    {
        var sourcePort = ObjectFactory.Port;
        var targetPort = ObjectFactory.Port;
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);
        link.SetTargetPosition(300, 300);

        Assert.Equal(300, link.TargetPositionX);
        Assert.Equal(300, link.TargetPositionY);
    }

    [Fact]
    public void Test_SetTargetPositionToPortCenterPosition()
    {
        var sourcePort = ObjectFactory.Port;

        var targetPort = ObjectFactory.Port;
        targetPort.SetPosition(100, 100);
        targetPort.SetSize(100, 100);
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);

        Assert.Equal(targetPort.GetCenterCoordinates().CenterX, link.TargetPositionX);
        Assert.Equal(targetPort.GetCenterCoordinates().CenterY, link.TargetPositionY);
    }

    [Fact]
    public void Has_IsConnected_Is_Set()
    {
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var sourcePort = ObjectFactory.Port;
        var targetPort = ObjectFactory.Port;
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);
        node.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        link.TargetPort = targetPort;

        Assert.True(link.IsConnected);
    }
}