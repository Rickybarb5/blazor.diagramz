using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Test.Links;

public class LinkEventTests
{
    [Fact]
    public void OnBeforeTargetPortConnected_Event_Triggered()
    {
        // Arrange
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var node3 = ObjectFactory.Node;
        var sourcePort = new Port<DummyComponent>();
        var targetPort = new Port<DummyComponent>();
        var targetPort2 = new Port<DummyComponent>();
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);
        node.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        node3.Ports.Add(targetPort2);
        var eventTriggered = false;
        link.OnBeforeTargetPortConnected += (_, _, _) => eventTriggered = true;
        // Act
        link.TargetPort = targetPort2;

        //Assert
        Assert.True(eventTriggered);
    }

    [Fact]
    public void OnTargetPositionChanged_Event_Triggered()
    {
        // Arrange
        var link = ObjectFactory.Link;
        var eventTriggered = false;
        link.OnTargetPositionChanged += (_, _) => eventTriggered = true;
        // Act
        link.SetTargetPosition(100, 200);

        //Assert
        Assert.True(eventTriggered);
        Assert.Equal(100, link.TargetPositionX);
        Assert.Equal(200, link.TargetPositionY);
    }

    [Fact]
    public void OnTargetPortChanged_Event_Triggered()
    {
        // Arrange
        var node = ObjectFactory.Node;
        var node2 = ObjectFactory.Node;
        var node3 = ObjectFactory.Node;
        var sourcePort = new Port<DummyComponent>();
        var targetPort = new Port<DummyComponent>();
        var targetPort2 = new Port<DummyComponent>();
        node.Ports.Add(sourcePort);
        node2.Ports.Add(targetPort);
        node3.Ports.Add(targetPort2);
        var eventTriggered = false;
        var link = ObjectFactory.LinkWithPorts<LinkCurvedComponent>(sourcePort, targetPort);
        link.OnTargetPortChanged += _ => eventTriggered = true;
        // Act
        link.TargetPort = targetPort2;

        //Assert
        Assert.True(eventTriggered);
        Assert.Same(targetPort2, link.TargetPort);
    }
}