﻿using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Test;

public class DummyComponent : IComponent
{
    public void Attach(RenderHandle renderHandle)
    {
        }

    public Task SetParametersAsync(ParameterView parameters)
    {
            return Task.CompletedTask;
        }
}