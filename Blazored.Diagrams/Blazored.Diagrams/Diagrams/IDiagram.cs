﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Diagrams.Options;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Diagrams;

/// <summary>
///     Represents a diagram.
/// </summary>
public interface IDiagram :
    IId,
    IPan,
    IZoomable,
    IModel,
    ILayerContainer,
    IDiagramEvents
{
    /// <summary>
    ///     Gets the position on the x-axis.
    /// </summary>
    [JsonIgnore]
    public int PositionX { get; set; }

    /// <summary>
    ///     Gets the position on the y-axis.
    /// </summary>
    [JsonIgnore]
    public int PositionY { get; set; }

    /// <summary>
    ///     Current layer in use.
    /// </summary>
    ILayer CurrentLayer { get; set; }

    /// <summary>
    ///     Configurable options for the diagram.
    /// </summary>
    DiagramOptions Options { get; set; }

    /// <summary>
    ///     Diagram events.
    /// </summary>
    IBehaviourContainer Behaviours { get; }

    /// <summary>
    ///     Current width of the diagram.
    /// </summary>
    public int Width { get; }

    /// <summary>
    ///     Current height of the diagram.
    /// </summary>
    public int Height { get; }

    /// <summary>
    ///     Gets all links from all layers.
    /// </summary>
    [JsonIgnore]
    IReadOnlyList<ILink> AllLinks { get; }

    /// <summary>
    ///     Gets all groups from all layers.
    /// </summary>
    [JsonIgnore]
    IReadOnlyList<IGroup> AllGroups { get; }

    /// <summary>
    ///     Gets all nodes from all layers
    /// </summary>
    [JsonIgnore]
    IReadOnlyList<INode> AllNodes { get; }

    /// <summary>
    ///     Gets all ports from all layers.
    /// </summary>
    [JsonIgnore]
    public IReadOnlyList<IPort> AllPorts { get; }

    /// <summary>
    ///     Unselects all models in the diagram.
    /// </summary>
    void UnselectAll();

    /// <summary>
    ///     Sets the value of <see cref="CurrentLayer" /> to another layer.
    /// </summary>
    /// <param name="layerId"></param>
    void UseLayer(Guid layerId);

    /// <summary>
    ///     Sets the value of <see cref="CurrentLayer" /> to another layer.
    /// </summary>
    /// <param name="layer"></param>
    void UseLayer(ILayer layer);

    internal void SetSize(int objWidth, int objHeight);

    /// <summary>
    ///     Adds a node to the current layer.
    /// </summary>
    /// <param name="node"></param>
    void AddNode(INode node);

    /// <summary>
    ///     Removes a node from the diagram.
    /// </summary>
    /// <param name="nodeToRemove"></param>
    bool RemoveNode(INode nodeToRemove);

    /// <summary>
    ///     Adds a group to the current layer.
    /// </summary>
    /// <param name="group"></param>
    void AddGroup(IGroup group);

    /// <summary>
    ///     Removes a group from the diagram.
    /// </summary>
    /// <param name="groupToRemove"></param>
    bool RemoveGroup(IGroup groupToRemove);

    /// <summary>
    ///     Creates a link between two ports.
    /// </summary>
    /// <param name="sourcePort">Source port of the link</param>
    /// <param name="targetPort">Target port of the link.</param>
    /// <typeparam name="TLinkComponent"></typeparam>
    ILink CreateLink<TLinkComponent>(IPort sourcePort, IPort? targetPort) where TLinkComponent : IComponent;

    /// <summary>
    ///     Creates a link between two ports.
    ///     The two ports must be part of the layer.
    /// </summary>
    /// <param name="sourcePort">Source port of the link</param>
    /// <param name="targetPort">Target port of the link.</param>
    /// <param name="linkComponentType">Link component Type</param>
    public ILink CreateLink(IPort sourcePort, IPort? targetPort, Type linkComponentType);

    /// <summary>
    ///     Removes a link from the diagram.
    /// </summary>
    void RemoveLink(ILink linkToRemove);


    /// <summary>
    ///     Sets the X and Y coordinates on the screen.
    ///     Triggers the onPosition events.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    void SetPosition(int x, int y);
}