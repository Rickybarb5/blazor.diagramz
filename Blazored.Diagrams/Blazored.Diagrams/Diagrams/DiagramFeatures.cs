using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Diagrams;

public partial class Diagram
{
    /// <inheritdoc />
    public virtual void AddNode(INode node)
    {
        _currentLayer.Nodes.Add(node);
    }

    /// <inheritdoc />
    public virtual bool RemoveNode(INode nodeToRemove)
    {
        foreach (var layer in Layers)
        {
            var removed = layer.RemoveNode(nodeToRemove);
            if (removed) return true;
        }

        return false;
    }

    /// <inheritdoc />
    public virtual void AddGroup(IGroup group)
    {
        _currentLayer.Groups.Add(group);
    }

    /// <inheritdoc />
    public virtual bool RemoveGroup(IGroup groupToRemove)
    {
        foreach (var layer in Layers)
        {
            var removed = layer.RemoveGroup(groupToRemove);
            if (removed) return true;
        }

        return false;
    }

    /// <inheritdoc />
    public virtual ILink CreateLink<TLinkComponent>(IPort sourcePort, IPort? targetPort)
        where TLinkComponent : IComponent
    {
        return _currentLayer.CreateLink<TLinkComponent>(sourcePort, targetPort);
    }

    /// <inheritdoc />
    public virtual ILink CreateLink(IPort sourcePort, IPort? targetPort, Type linkComponentType)
    {
        return _currentLayer.CreateLink(sourcePort, targetPort, linkComponentType);
    }

    /// <inheritdoc />
    public virtual void RemoveLink(ILink linkToRemove)
    {
        linkToRemove.Dispose();
    }

    /// <inheritdoc />
    public virtual void SetPan(int panX, int panY)
    {
        if (_panX != panX || _panY != panY)
        {
            _panX = panX;
            _panY = panY;
            NotifyPanChanged(_panX, _panY);
        }
    }

    /// <inheritdoc />
    public virtual void UnselectAll()
    {
        foreach (var layer in _layers) layer.UnselectAll();
    }

    /// <inheritdoc />
    public virtual void SetZoom(double zoom)
    {
        if (zoom > Options.ZoomOptions.MaxZoom)
        {
            zoom = Options.ZoomOptions.MaxZoom;
        }
        else if (zoom < Options.ZoomOptions.MinZoom)
        {
            zoom = Options.ZoomOptions.MinZoom;
        }

        if (zoom != _zoom)
        {
            NotifyBeforeZoomChanged(Zoom);
            Zoom = zoom;
        }
    }

    /// <inheritdoc />
    public virtual void StepZoomUp()
    {
        SetZoom(Zoom + Options.ZoomOptions.ZoomStep);
    }

    /// <inheritdoc />
    public virtual void StepZoomDown()
    {
        SetZoom(Zoom - Options.ZoomOptions.ZoomStep);
    }

    /// <inheritdoc />
    public virtual void UseLayer(ILayer layer)
    {
        var currentLayer = _layers.FirstOrDefault(x => x.Id == layer.Id);
        _currentLayer = currentLayer ??
                        throw new InvalidOperationException($"Layer {layer.Id} is not a part of the diagram");
    }

    /// <inheritdoc />
    public virtual void UseLayer(Guid layerId)
    {
        var layer = _layers.FirstOrDefault(x => x.Id == layerId);
        _currentLayer = layer ?? throw new InvalidOperationException($"Layer {layerId} is not a part of the diagram");
    }

    /// <inheritdoc />
    void IDiagram.SetSize(int width, int height)
    {
        var stateChanged = width != Width || Height != height;

        if (stateChanged)
        {
            Width = width;
            Height = height;
            NotifySizeChanged();
        }
    }

    /// <inheritdoc />
    void IDiagram.SetPosition(int x, int y)
    {
        var stateChanged = x != _positionX || y != _positionY;

        if (stateChanged)
        {
            _positionX = x;
            _positionY = y;
            NotifyPositionChanged();
        }
    }
}