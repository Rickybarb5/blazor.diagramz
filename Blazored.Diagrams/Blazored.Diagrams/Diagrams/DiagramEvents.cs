﻿using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Helpers;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;
using Blazored.Diagrams.Events;

namespace Blazored.Diagrams.Diagrams;

/// <inheritdoc />
public partial class Diagram
{
    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnRedraw triggered for Diagram", callerMemberName);
        OnRedraw?.Invoke();
    }

    /// <inheritdoc />
    public event Action<KeyboardEventArgs>? OnKeyDown;

    /// <inheritdoc />
    public event Action<KeyboardEventArgs>? OnKeyUp;

    /// <inheritdoc />
    public void NotifyKeyDown(KeyboardEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnKeyDown triggered - Key: {args.Key}", callerMemberName);
        OnKeyDown?.Invoke(args);
    }

    /// <inheritdoc />
    public void NotifyKeyUp(KeyboardEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnKeyUp triggered - Key: {args.Key}", callerMemberName);
        OnKeyUp?.Invoke(args);
    }

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkRemoved;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkRemoved;

    /// <inheritdoc />
    public void NotifyIncomingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnIncomingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyIncomingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnIncomingLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnOutgoingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnOutgoingLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public event Action<IDiagram>? OnSizeChanged;

    /// <inheritdoc />
    public void NotifySizeChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSizeChanged triggered for Diagram", callerMemberName);
        OnSizeChanged?.Invoke(this);
    }

    #region Node events

    /// <inheritdoc />
    public event Action<INode>? OnNodeAdded;

    /// <inheritdoc />
    public event Action<INode>? OnNodeRemoved;

    /// <inheritdoc />
    public void NotifyNodeAdded(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeAdded triggered - Node: {node.Id}", callerMemberName);
        OnNodeAdded?.Invoke(node);
    }

    /// <inheritdoc />
    public void NotifyNodeRemoved(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeRemoved triggered - Node: {node.Id}", callerMemberName);
        OnNodeRemoved?.Invoke(node);
    }

    #endregion

    #region Node pointer events

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerDown;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerUp;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerMove;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerEnter;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerLeave;

    /// <inheritdoc />
    public event Action<INode, WheelEventArgs>? OnNodeWheel;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeClicked;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeDoubleClicked;

    /// <inheritdoc />
    public void NotifyNodePointerDown(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerDown triggered - Node: {node.Id}", callerMemberName);
        OnNodePointerDown?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerUp(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerUp triggered - Node: {node.Id}", callerMemberName);
        OnNodePointerUp?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerMove(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnNodePointerMove triggered - Node: {node.Id}", callerMemberName);
        OnNodePointerMove?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerEnter(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerEnter triggered - Node: {node.Id}", callerMemberName);
        OnNodePointerEnter?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerLeave(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerLeave triggered - Node: {node.Id}", callerMemberName);
        OnNodePointerLeave?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeWheel(INode node, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeWheel triggered - Node: {node.Id}", callerMemberName);
        OnNodeWheel?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeClicked(INode node, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeClicked triggered - Node: {node.Id}", callerMemberName);
        OnNodeClicked?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeDoubleClicked(INode node, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeDoubleClicked triggered - Node: {node.Id}", callerMemberName);
        OnNodeDoubleClicked?.Invoke(node, args);
    }

    #endregion

    #region Link Events

    /// <inheritdoc />
    public event Action<ILink>? OnLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnLinkRemoved;

    /// <inheritdoc />
    public void NotifyLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkAdded triggered - Link: {link.Id}", callerMemberName);
        OnLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkRemoved triggered - Link: {link.Id}", callerMemberName);
        OnLinkRemoved?.Invoke(link);
    }

    #endregion

    #region Layer Events

    /// <inheritdoc />
    public event Action<ILayer>? OnLayerAdded;

    /// <inheritdoc />
    public event Action<ILayer>? OnLayerRemoved;

    /// <inheritdoc />
    public event Action<double, double>? OnPanEnded;

    /// <inheritdoc />
    public void NotifyLayerAdded(ILayer layer, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLayerAdded triggered - Layer: {layer.Id}", callerMemberName);
        OnLayerAdded?.Invoke(layer);
    }

    /// <inheritdoc />
    public void NotifyLayerRemoved(ILayer layer, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLayerRemoved triggered - Layer: {layer.Id}", callerMemberName);
        OnLayerRemoved?.Invoke(layer);
    }

    #endregion

    #region Port events

    /// <inheritdoc />
    public event Action<IPort>? OnPortAdded;

    /// <inheritdoc />
    public event Action<IPort>? OnPortRemoved;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortClicked;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortDoubleClicked;

    /// <inheritdoc />
    public void NotifyPortAdded(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortAdded triggered - Port: {port.Id}", callerMemberName);
        OnPortAdded?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortRemoved(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortRemoved triggered - Port: {port.Id}", callerMemberName);
        OnPortRemoved?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortClicked triggered - Port: {port.Id}", callerMemberName);
        OnPortClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortDoubleClicked(IPort port, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortDoubleClicked triggered - Port: {port.Id}", callerMemberName);
        OnPortDoubleClicked?.Invoke(port, args);
    }

    #endregion

    #region Diagram events

    /// <inheritdoc />
    public event Action<double>? OnBeforeZoomChanged;

    /// <inheritdoc />
    public event Action<double>? OnZoomChanged;

    /// <inheritdoc />
    public void NotifyBeforeZoomChanged(double zoom, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnBeforeZoomChanged triggered - Zoom: {zoom}", callerMemberName);
        OnBeforeZoomChanged?.Invoke(zoom);
    }

    /// <inheritdoc />
    public void NotifyZoomChanged(double zoom, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnZoomChanged triggered - Zoom: {zoom}", callerMemberName);
        OnZoomChanged?.Invoke(zoom);
    }

    /// <inheritdoc />
    public event Action<double, double>? OnPanStarted;

    /// <inheritdoc />
    public event Action<double, double>? OnPanChanged;

    /// <inheritdoc />
    public void NotifyPanStart(double panX, double panY, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPanStarted triggered - X: {panX}, Y: {panY}", callerMemberName);
        OnPanStarted?.Invoke(panX, panY);
    }

    /// <inheritdoc />
    public void NotifyPanChanged(double panX, double panY, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPanChanged triggered - X: {panX}, Y: {panY}", callerMemberName);
        OnPanChanged?.Invoke(panX, panY);
    }

    /// <inheritdoc />
    public void NotifyPanEnd(double panX, double panY, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPanEnded triggered - X: {panX}, Y: {panY}", callerMemberName);
        OnPanEnded?.Invoke(panX, panY);
    }

    #endregion

    #region Pointer events

    /// <inheritdoc />
    public event Action<IDiagram, PointerEventArgs>? OnPointerDown;

    /// <inheritdoc />
    public event Action<IDiagram, PointerEventArgs>? OnPointerUp;

    /// <inheritdoc />
    public event Action<IDiagram, PointerEventArgs>? OnPointerMove;

    /// <inheritdoc />
    public event Action<IDiagram, PointerEventArgs>? OnPointerEnter;

    /// <inheritdoc />
    public event Action<IDiagram, PointerEventArgs>? OnPointerLeave;

    /// <inheritdoc />
    public event Action<IDiagram, WheelEventArgs>? OnWheel;

    /// <inheritdoc />
    public event Action<IDiagram, MouseEventArgs>? OnClick;

    /// <inheritdoc />
    public event Action<IDiagram, MouseEventArgs>? OnDoubleClick;

    /// <inheritdoc />
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerDown triggered - PointerId: {args.PointerId}", callerMemberName);
        OnPointerDown?.Invoke(this, args);
        EventInvoker.InvokeEvent(OnPointerDown, this, args);
    }


    /// <inheritdoc />
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerUp triggered - PointerId: {args.PointerId}", callerMemberName);
        OnPointerUp?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerMove triggered - PointerId: {args.PointerId}", callerMemberName);
        OnPointerMove?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerEnter triggered - PointerId: {args.PointerId}", callerMemberName);
        OnPointerEnter?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerLeave triggered - PointerId: {args.PointerId}", callerMemberName);
        OnPointerLeave?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnWheel triggered - DeltaX: {args.DeltaX}, DeltaY: {args.DeltaY}", callerMemberName);
        OnWheel?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnClick triggered - Button: {args.Button}", callerMemberName);
        OnClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnDoubleClick triggered - Button: {args.Button}", callerMemberName);
        OnDoubleClick?.Invoke(this, args);
    }

    #endregion

    #region Group events

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupAdded;

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupRemoved;

    /// <inheritdoc />
    public void NotifyGroupAdded(IGroup addedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupAdded triggered - Group: {addedGroup.Id}", callerMemberName);
        OnGroupAdded?.Invoke(addedGroup);
    }

    /// <inheritdoc />
    public void NotifyGroupRemoved(IGroup removedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupRemoved triggered - Group: {removedGroup.Id}", callerMemberName);
        OnGroupRemoved?.Invoke(removedGroup);
    }

    #endregion

    #region Group pointer events

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerDown;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerUp;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerMove;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerEnter;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerLeave;

    /// <inheritdoc />
    public event Action<IGroup, WheelEventArgs>? OnGroupWheel;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupClicked;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupDoubleClicked;

    /// <inheritdoc />
    public void NotifyGroupPointerDown(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerDown triggered - Group: {group.Id}", callerMemberName);
        OnGroupPointerDown?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerUp(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerUp triggered - Group: {group.Id}", callerMemberName);
        OnGroupPointerUp?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerMove(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnGroupPointerMove triggered - Group: {group.Id}", callerMemberName);
        OnGroupPointerMove?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerEnter(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerEnter triggered - Group: {group.Id}", callerMemberName);
        OnGroupPointerEnter?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerLeave(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerLeave triggered - Group: {group.Id}", callerMemberName);
        OnGroupPointerLeave?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupWheel(IGroup group, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupWheel triggered - Group: {group.Id}", callerMemberName);
        OnGroupWheel?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupClicked triggered - Group: {group.Id}", callerMemberName);
        OnGroupClicked?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupDoubleClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupDoubleClicked triggered - Group: {group.Id}", callerMemberName);
        OnGroupDoubleClicked?.Invoke(group, args);
    }

    #endregion

    #region Port pointer events

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerDown;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerUp;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerMove;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerEnter;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerLeave;

    /// <inheritdoc />
    public event Action<IPort, WheelEventArgs>? OnPortWheel;

    /// <inheritdoc />
    public void NotifyPortPointerDown(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerDown triggered - Port: {port.Id}", callerMemberName);
        OnPortPointerDown?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerUp(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerUp triggered - Port: {port.Id}", callerMemberName);
        OnPortPointerUp?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerMove(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPortPointerMove triggered - Port: {port.Id}", callerMemberName);
        OnPortPointerMove?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerEnter(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerEnter triggered - Port: {port.Id}", callerMemberName);
        OnPortPointerEnter?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerLeave(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerLeave triggered - Port: {port.Id}", callerMemberName);
        OnPortPointerLeave?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortWheel(IPort port, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortWheel triggered - Port: {port.Id}", callerMemberName);
        OnPortWheel?.Invoke(port, args);
    }

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerDown;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerUp;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerMove;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerEnter;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerLeave;

    /// <inheritdoc />
    public event Action<ILink, WheelEventArgs>? OnLinkWheel;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkClicked;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkDoubleClicked;

    /// <inheritdoc />
    public void NotifyLinkPointerDown(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerDown triggered - Link: {link.Id}", callerMemberName);
        OnLinkPointerDown?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerUp(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerUp triggered - Link: {link.Id}", callerMemberName);
        OnLinkPointerUp?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerMove(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnLinkPointerMove triggered - Link: {link.Id}", callerMemberName);
        OnLinkPointerMove?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerEnter(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerEnter triggered - Link: {link.Id}", callerMemberName);
        OnLinkPointerEnter?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerLeave(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerLeave triggered - Link: {link.Id}", callerMemberName);
        OnLinkPointerLeave?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkWheel(ILink link, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkWheel triggered - Link: {link.Id}", callerMemberName);
        OnLinkWheel?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkClicked(ILink link, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkClicked triggered - Link: {link.Id}", callerMemberName);
        OnLinkClicked?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkDoubleClicked(ILink link, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkDoubleClicked triggered - Link: {link.Id}", callerMemberName);
        OnLinkDoubleClicked?.Invoke(link, args);
    }

    #endregion

    /// <summary>
    /// Event triggered when the diagram position changes.
    /// </summary>
    public event Action<IDiagram>? OnPositionChanged;

    /// <summary>
    /// Event triggered before the diagram position changes.
    /// </summary>
    public event Action<IDiagram>? OnBeforePositionChanged;

    /// <summary>
    /// Triggers the <see cref="OnPositionChanged"/> event.
    /// </summary>
    /// <param name="callerMemberName"></param>
    public void NotifyPositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPositionChanged triggered", callerMemberName);
        OnPositionChanged?.Invoke(this);
    }


    /// <summary>
    /// Triggers the <see cref="OnBeforePositionChanged"/> event.
    /// </summary>
    /// <param name="callerMemberName"></param>
    public void NotifyBeforePositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnBeforePositionChanged triggered", callerMemberName);
        OnBeforePositionChanged?.Invoke(this);
    }
}