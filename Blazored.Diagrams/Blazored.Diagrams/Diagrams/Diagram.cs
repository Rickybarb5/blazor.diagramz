﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams.Behaviours;
using Blazored.Diagrams.Diagrams.Options;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Diagrams;

/// <summary>
///     Top level class for a diagram and all of its components/children.
/// </summary>
public partial class Diagram : IDiagram
{
    private readonly ObservableList<ILayer> _layers = [];
    private ILayer _currentLayer = new Layer();
    private int _panX;
    private int _panY;
    private double _zoom = 1;
    private int _positionX = 0;
    private int _positionY = 0;


    /// <summary>
    /// Instantiates a new <see cref="Diagram"/>
    /// </summary>
    public Diagram()
    {
        Layers.Add(_currentLayer);
        Behaviours = new DiagramBehaviourContainer(this);
    }


    /// <inheritdoc />
    public virtual void Dispose()
    {
        _layers.ForEach(x => x.Dispose());
        _layers.Clear();
        Behaviours.Dispose();
    }


    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    [JsonPropertyOrder(2)]
    public virtual ILayer CurrentLayer
    {
        get => _currentLayer;
        set
        {
            _currentLayer = _layers.First(x => x.Id == value.Id) ??
                            throw new InvalidOperationException("Current layer does not exist in diagram.");
        }
    }

    /// <inheritdoc />
    public virtual DiagramOptions Options { get; set; } = new();

    /// <inheritdoc />
    public virtual double Zoom
    {
        get => _zoom;
        set
        {
            if (Math.Abs(_zoom - value) > 0.0001)
            {
                _zoom = Math.Round(value, 3);
                NotifyZoomChanged(_zoom);
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }

    /// <inheritdoc />
    public virtual int PanX
    {
        get => _panX;
        set
        {
            if (_panX != value)
            {
                _panX = value;
                NotifyPanChanged(_panX, _panY);
            }
        }
    }

    /// <inheritdoc />
    public virtual int PanY
    {
        get => _panY;
        set
        {
            if (_panY != value)
            {
                _panY = value;
                NotifyPanChanged(_panX, _panY);
            }
        }
    }


    /// <inheritdoc />
    [JsonIgnore]
    public virtual int Width { get; internal set; }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual int Height { get; internal set; }

    /// <summary>
    ///     Gets all the layers in this diagram.
    /// </summary>
    [JsonPropertyOrder(1)]
    public virtual ObservableList<ILayer> Layers
    {
        get => _layers;
        init
        {
            _layers.Clear();
            foreach (var val in value) _layers.Add(val);
        }
    }

    /// <summary>
    ///     Gets all the nodes from all layers.
    /// </summary>
    [JsonIgnore]
    public virtual IReadOnlyList<INode> AllNodes => Layers.SelectMany(layer => layer.AllNodes).ToList().AsReadOnly();

    /// <summary>
    ///     Gets the links from all layers.
    /// </summary>
    [JsonIgnore]
    public virtual IReadOnlyList<ILink> AllLinks => Layers.SelectMany(layer => layer.AllLinks).ToList().AsReadOnly();

    /// <summary>
    ///     Gets the groups from all layers.
    /// </summary>
    [JsonIgnore]
    public virtual IReadOnlyList<IGroup> AllGroups => Layers.SelectMany(layer => layer.AllGroups).ToList().AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<IPort> AllPorts => Layers.SelectMany(layer => layer.AllPorts).ToList().AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual int PositionX
    {
        get => _positionX;
        set
        {
            if (value != _positionX)
            {
                NotifyBeforePositionChanged();
                _positionX = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual int PositionY
    {
        get => _positionY;
        set
        {
            if (value != _positionY)
            {
                NotifyBeforePositionChanged();
                _positionY = value;
                NotifyPositionChanged();
            }
        }
    }
}