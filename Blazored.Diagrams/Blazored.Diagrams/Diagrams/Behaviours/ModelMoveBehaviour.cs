﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Allows the user to move selected nodes and groups, taking zoom into account.
/// </summary>
public class ModelMoveBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;
    private bool _isDragging;
    private double _lastPointerX;
    private double _lastPointerY;

    /// <summary>
    /// Instantiates a new <see cref="ModelMoveBehaviour"/>
    /// </summary>
    /// <param name="diagram"></param>
    public ModelMoveBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    private IEnumerable<IPosition> SelectedModels =>
        _diagram
            .Layers
            .SelectMany(l => l.AllNodes.Where(x => x.IsSelected)
                .Cast<IPosition>()
                .Concat(l.AllGroups.Where(g => g.IsSelected)));

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnNodePointerDown -= OnPointerDown;
        _diagram.OnGroupPointerDown -= OnPointerDown;
        _diagram.OnPointerMove -= OnPointerMove;
        _diagram.OnPointerUp -= OnPointerUp;
    }

    private void EnableBehaviour()
    {
        _diagram.OnNodePointerDown += OnPointerDown;
        _diagram.OnGroupPointerDown += OnPointerDown;
        _diagram.OnPointerMove += OnPointerMove;
        _diagram.OnPointerUp += OnPointerUp;
    }

    private void OnPointerDown(IPosition model, PointerEventArgs e)
    {
        _isDragging = true;
        _lastPointerX = e.ClientX;
        _lastPointerY = e.ClientY;
    }

    private void OnPointerMove(IDiagram diagram, PointerEventArgs e)
    {
        if (!IsEnabled || !_isDragging) return;

        var xDiff = e.ClientX - _lastPointerX;
        var yDiff = e.ClientY - _lastPointerY;

        // Apply inverse zoom to the movement
        var inverseZoom = 1 / diagram.Zoom;
        var xDiffAdjusted = xDiff * inverseZoom;
        var yDiffAdjusted = yDiff * inverseZoom;

        foreach (var model in SelectedModels)
        {
            var x = (int)(model.PositionX + xDiffAdjusted);
            var y = (int)(model.PositionY + yDiffAdjusted);
            model.SetPosition(x, y);
        }

        _lastPointerX = e.ClientX;
        _lastPointerY = e.ClientY;
    }

    private void OnPointerUp(IDiagram diagram, PointerEventArgs e)
    {
        _isDragging = false;
        _lastPointerX = 0;
        _lastPointerY = 0;
    }
}