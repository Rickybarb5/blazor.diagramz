﻿using Blazored.Diagrams.Components.Models;
using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Behaviour for creating a link when a port is clicked.
/// </summary>
public class LinkCreationBehavior : IBehaviour
{
    /// <summary>
    ///     Type of link that the behaviour will create.
    /// Default is <see cref="LinkCurvedComponent"/>
    /// </summary>
    public Type LinkComponentType = typeof(LinkCurvedComponent);

    private readonly IDiagram _diagram;
    private int _initialClickX;
    private int _initialClickY;
    private bool _isCreatingLink;

    private IPort? _sourcePort;
    private IPort? _targetPort;

    /// <summary>
    ///     Initializes a new instance of <see cref="LinkCreationBehavior"/>
    /// </summary>
    /// <param name="diagram"></param>
    public LinkCreationBehavior(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    private ILink? Link { get; set; }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnPortPointerDown -= StartLinkCreation;
        _diagram.OnPortPointerUp -= CreateLink;
        _diagram.OnPointerMove -= UpdateTargetPosition;
        _diagram.OnPointerUp -= OnDiagramPointerUp;
    }

    private void EnableBehaviour()
    {
        _diagram.OnPortPointerDown += StartLinkCreation;
        _diagram.OnPortPointerUp += CreateLink;
        _diagram.OnPointerMove += UpdateTargetPosition;
        _diagram.OnPointerUp += OnDiagramPointerUp;
    }

    /// <summary>
    ///     1-When pointer is down on a port, start link creation.
    /// </summary>
    /// <param name="port"></param>
    /// <param name="e"></param>
    private void StartLinkCreation(IPort port, PointerEventArgs e)
    {
        if (!IsEnabled || !port.CanCreateLink()) return;
        Link = null;
        _sourcePort = null;
        _targetPort = null;
        _isCreatingLink = false;
        _initialClickX = 0;
        _initialClickY = 0;
        ClearUnboundedLinks();
        _isCreatingLink = true;
        _sourcePort = port;
        _initialClickX = (int)e.ClientX;
        _initialClickY = (int)e.ClientY;

        Link = _diagram.CreateLink(_sourcePort, null, LinkComponentType);
        var startCoordinates = _sourcePort.GetCenterCoordinates();
        Link.SetTargetPosition(startCoordinates.CenterX, startCoordinates.CenterY);
    }

    /// <summary>
    ///     2-Pointer is moving.
    /// </summary>
    /// <param name="diagram"></param>
    /// <param name="e"></param>
    private void UpdateTargetPosition(IDiagram diagram, PointerEventArgs e)
    {
        _isCreatingLink = e.Buttons == 1 && _sourcePort is not null;

        if (_isCreatingLink && Link is not null)
        {
            // Calculate new target position based on the movement from the initial click
            var newX = (int)(_sourcePort!.PositionX + (e.ClientX - _initialClickX) / _diagram.Zoom);
            var newY = (int)(_sourcePort.PositionY + (e.ClientY - _initialClickY) / _diagram.Zoom);

            Link.SetTargetPosition(newX, newY);
        }
    }

    /// <summary>
    ///     3-When the pointer is released on top of a port.
    /// </summary>
    /// <param name="port"></param>
    /// <param name="e"></param>
    private void CreateLink(IPort port, PointerEventArgs e)
    {
        if (_isCreatingLink && _sourcePort is not null && Link is not null)
        {
            _targetPort = port;
            OnDiagramPointerUp(_diagram, e);
        }

        ClearUnboundedLinks();
    }

    /// <summary>
    ///     4-When the mouse is released.
    /// </summary>
    /// <param name="e"></param>
    /// <param name="diagram"></param>
    private void OnDiagramPointerUp(IDiagram diagram, PointerEventArgs e)
    {
        if (CanLinkToTarget())
        {
            _targetPort!.IncomingLinks.Add(Link!);
        }

        ClearUnboundedLinks();

        Link = null;
        _sourcePort = null;
        _targetPort = null;
        _isCreatingLink = false;
        _initialClickX = 0;
        _initialClickY = 0;
    }

    private bool CanLinkToTarget()
    {
        return _targetPort is not null && _sourcePort is not null && Link is not null &&
               _sourcePort.CanConnectTo(_targetPort) && _targetPort.CanConnectTo(_sourcePort);
    }

    private void ClearUnboundedLinks()
    {
        var unboundedLinks = _diagram.AllLinks.Where(x => x.TargetPort is null);
        foreach (var unboundedLink in unboundedLinks) unboundedLink.Dispose();
    }
}