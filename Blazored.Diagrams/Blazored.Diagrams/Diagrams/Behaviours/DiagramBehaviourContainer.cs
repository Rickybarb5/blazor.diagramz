using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
/// Diagram behaviour container with default events.
/// </summary>
public class DiagramBehaviourContainer : BehaviourContainer
{
    /// <inheritdoc />
    public DiagramBehaviourContainer(IDiagram diagram)
    {
        Add(new EventPipelineBehaviour<IDiagram>(diagram));
        Add(new LayerContainerBehaviour<IDiagram>(diagram));
        Add(new ZoomBehavior(diagram));
        Add(new PanBehaviour(diagram));
        Add(new ModelMoveBehaviour(diagram));
        Add(new ModelSelectBehaviour(diagram));
        Add(new LinkCreationBehavior(diagram));
        Add(new DeleteWithKeyBehaviour(diagram));
        Add(new DeleteBehaviour(diagram));
        Add(new RedrawBehaviour<IDiagram>(diagram));
    }
}