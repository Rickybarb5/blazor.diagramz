using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Layers;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Handles events for a layer container.
/// </summary>
/// <typeparam name="Container"></typeparam>
public class LayerContainerBehaviour<Container> : IBehaviour
    where Container : ILayerContainer, ILayerContainerEvents
{
    private Container _layerContainer;

    /// <summary>
    ///     Instantiates a new <see cref="LayerContainerBehaviour{LayerContainer}" />
    /// </summary>
    /// <param name="layerContainer"></param>
    public LayerContainerBehaviour(Container layerContainer)
    {
        _layerContainer = layerContainer;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _layerContainer.Layers.OnItemAdded -= HandleLayerAdded;
        _layerContainer.Layers.OnItemRemoved -= HandleLayerRemoved;
    }

    private void EnableBehaviour()
    {
        _layerContainer.Layers.OnItemAdded += HandleLayerAdded;
        _layerContainer.Layers.OnItemRemoved += HandleLayerRemoved;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void HandleLayerRemoved(ILayer obj)
    {
        _layerContainer.NotifyLayerRemoved(obj);
    }

    private void HandleLayerAdded(ILayer obj)
    {
        _layerContainer.NotifyLayerAdded(obj);
    }
}