using Blazored.Diagrams.Interfaces.Behaviours;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
/// Behaviour that removes objects from the diagram if they are selected and the delete key is pressed.
/// </summary>
public class DeleteWithKeyBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;


    /// <summary>
    /// Key code that deletes diagram children.
    /// </summary>
    public string DeleteKeyCode { get; set; } = "Delete";

    /// <summary>
    /// Instantiates a new <see cref="DeleteWithKeyBehaviour"/>.
    /// </summary>
    /// <param name="diagram"></param>
    public DeleteWithKeyBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnKeyDown -= OnKeyDown;
    }

    private void EnableBehaviour()
    {
        _diagram.OnKeyDown += OnKeyDown;
    }

    private void OnKeyDown(KeyboardEventArgs obj)
    {
        if (obj.Code != DeleteKeyCode ||
            !IsEnabled) return;

        var selectedGroups = _diagram.AllGroups.Where(x => x.IsSelected);
        var selectedLinks = _diagram.AllLinks.Where(x => x.IsSelected);
        var selectedNodes = _diagram.AllNodes.Where(x => x.IsSelected);


        foreach (var link in selectedLinks)
        {
            _diagram.Layers.ForEach(l => l.RemoveLink(link));
        }

        foreach (var group in selectedGroups)
        {
            _diagram.RemoveGroup(group);
        }

        foreach (var node in selectedNodes)
        {
            _diagram.RemoveNode(node);
        }
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}