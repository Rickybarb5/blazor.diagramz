using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Performs cleanup actions when something is deleted from the diagram.
/// </summary>
public class DeleteBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;
    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnLayerRemoved -= CleanupLayerDependencies;
        _diagram.OnNodeRemoved -= CleanupNodeDependencies;
        _diagram.OnGroupRemoved -= CleanupGroupDependencies;
        _diagram.OnPortRemoved -= CleanupPortDependencies;
        _diagram.OnLinkRemoved -= CleanupLinkDependencies;
    }

    private void EnableBehaviour()
    {
        _diagram.OnLayerRemoved += CleanupLayerDependencies;
        _diagram.OnNodeRemoved += CleanupNodeDependencies;
        _diagram.OnGroupRemoved += CleanupGroupDependencies;
        _diagram.OnPortRemoved += CleanupPortDependencies;
        _diagram.OnLinkRemoved += CleanupLinkDependencies;
    }

    /// <summary>
    /// Instantiates a new <see cref="DeleteBehaviour"/>
    /// </summary>
    /// <param name="diagram"></param>
    public DeleteBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    private void CleanupLayerDependencies(ILayer obj)
    {
        obj.Dispose();
    }

    private void CleanupLinkDependencies(ILink obj)
    {
        obj.Dispose();
    }

    private void CleanupGroupDependencies(IGroup obj)
    {
        obj.Dispose();
    }

    /// <summary>
    ///     Removes any link dependencies associated with the node.
    /// </summary>
    /// <param name="obj"></param>
    private void CleanupNodeDependencies(INode obj)
    {
        obj.Dispose();
    }

    private void CleanupPortDependencies(IPort port)
    {
        port.Dispose();
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}