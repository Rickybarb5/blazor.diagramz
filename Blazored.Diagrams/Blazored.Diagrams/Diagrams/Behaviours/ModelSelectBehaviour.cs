﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Selection behaviour for the UI.
///     Supports multi selection using ctrl key.
/// </summary>
public class ModelSelectBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;
    private bool _isEnabled = true;

    /// <summary>
    ///     Enables or disables multi selection
    /// </summary>
    public bool MultiSelectEnabled { get; set; } = true;

    /// <summary>
    ///     Enables or disables model selection.
    /// </summary>
    public bool SelectionEnabled { get; set; } = true;

    /// <summary>
    /// Instantiates a new <see cref="ModelSelectBehaviour"/>
    /// </summary>
    /// <param name="diagram"></param>
    public ModelSelectBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void SelectModel(ISelectable selectableModel, PointerEventArgs args)
    {
        if (args.Button != 0 || !SelectionEnabled) return;

        if (MultiSelectEnabled && args.CtrlKey)
        {
            // Toggle selection when Ctrl is pressed
            selectableModel.IsSelected = !selectableModel.IsSelected;
        }
        else
        {
            // If not multiselect or Ctrl not pressed, unselect all and select the new one
            var wasSelected = selectableModel.IsSelected;
            _diagram.UnselectAll();
            selectableModel.IsSelected = true;
        }
    }

    private void HandleBackgroundClick(IDiagram diagram, PointerEventArgs args)
    {
        if (args.Button == 0) // Only handle left clicks
        {
            // Only clear selection if Ctrl is not pressed or multiselect is disabled
            if (!MultiSelectEnabled || !args.CtrlKey)
            {
                _diagram.UnselectAll();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnNodePointerDown -= SelectModel;
        _diagram.OnGroupPointerDown -= SelectModel;
        _diagram.OnLinkPointerDown -= SelectModel;
        _diagram.OnPointerDown -= HandleBackgroundClick;
    }

    private void EnableBehaviour()
    {
        _diagram.OnNodePointerDown += SelectModel;
        _diagram.OnGroupPointerDown += SelectModel;
        _diagram.OnLinkPointerDown += SelectModel;
        _diagram.OnPointerDown += HandleBackgroundClick;
    }
}