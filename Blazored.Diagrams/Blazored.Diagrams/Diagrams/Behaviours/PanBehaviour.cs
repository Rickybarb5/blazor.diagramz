﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Behaviour for panning the diagram using pointer click and drag.
/// </summary>
public class PanBehaviour : IBehaviour
{
    private readonly IDiagram _diagram;
    private bool _isPanning;
    private double _lastPointerX;
    private double _lastPointerY;

    /// <summary>
    /// Instantiates a new <see cref="PanBehaviour"/>
    /// </summary>
    /// <param name="diagram"></param>
    public PanBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnPointerDown -= OnPanStart;
        _diagram.OnPointerMove -= OnPan;
        _diagram.OnPointerUp -= OnPanEnd;
    }

    private void EnableBehaviour()
    {
        _diagram.OnPointerDown += OnPanStart;
        _diagram.OnPointerMove += OnPan;
        _diagram.OnPointerUp += OnPanEnd;
    }

    private void OnPanStart(IDiagram diagram, PointerEventArgs args)
    {
        // We only pan if the ctrl key is not being pressed.
        // This is because of the multi select behaviour.
        if (!args.CtrlKey)
        {
            _isPanning = true;
            _lastPointerX = args.ClientX;
            _lastPointerY = args.ClientY;
            _diagram.NotifyPanStart(_diagram.PanX, _diagram.PanY);
        }
    }

    private void OnPan(IDiagram diagram, PointerEventArgs args)
    {
        if (_isPanning && IsEnabled)
        {
            // Calculate the pointer movement delta
            var deltaX = args.ClientX - _lastPointerX;
            var deltaY = args.ClientY - _lastPointerY;

            var panX = (int)(_diagram.PanX + deltaX);
            var panY = (int)(_diagram.PanY + deltaY);

            _diagram.SetPan(panX, panY);

            // Store the current pointer coordinates for the next pan event
            _lastPointerX = args.ClientX;
            _lastPointerY = args.ClientY;
        }
    }

    private void OnPanEnd(IDiagram diagram, PointerEventArgs args)
    {
        if (_isPanning)
        {
            _isPanning = false;
            _lastPointerX = 0;
            _lastPointerY = 0;
            _diagram.NotifyPanEnd(_diagram.PanX, _diagram.PanY);
        }
    }
}