﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Diagrams.Behaviours;

/// <summary>
///     Behaviour for zooming the diagram using the pointer wheel.
/// </summary>
public class ZoomBehavior : IBehaviour
{
    private readonly IDiagram _diagram;

    /// <summary>
    /// Instantiates a new <see cref="ZoomBehavior"/>
    /// </summary>
    /// <param name="diagram"></param>
    public ZoomBehavior(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;

    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnWheel -= Events_OnWheel;
    }

    private void EnableBehaviour()
    {
        _diagram.OnWheel += Events_OnWheel;
    }

    private void Events_OnWheel(IDiagram diagram, WheelEventArgs args)
    {
        if (!_diagram.Options.ZoomOptions.Enabled) return;
        var wheelDeltaY = args.DeltaY;
        switch (wheelDeltaY)
        {
            case > 0:
                _diagram.StepZoomDown();
                break;
            case < 0:
                _diagram.StepZoomUp();
                break;
        }
    }
}