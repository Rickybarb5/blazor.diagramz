﻿using Blazored.Diagrams.Interfaces.Events;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Diagrams;

/// <summary>
///     Event management for a diagram.
/// </summary>
public interface IDiagramEvents :
    ILinkContainerEvents,
    IPortContainerEvents,
    INodeContainerEvents,
    IGroupContainerEvents,
    ILayerContainerEvents,
    IPointerEvents<IDiagram>,
    IKeyboardEvents,
    IUiEvents,
    IDisposable
{
    /// <summary>
    ///     Event triggered before the zoom changes.
    /// </summary>
    event Action<double> OnBeforeZoomChanged;

    /// <summary>
    ///     Event triggered when the zoom value changes.
    /// </summary>
    event Action<double> OnZoomChanged;

    /// <summary>
    ///     Event triggered when a pan action starts.
    /// </summary>
    event Action<double, double> OnPanStarted;

    /// <summary>
    ///     Event triggered when a pan value changes.
    /// </summary>
    event Action<double, double> OnPanChanged;

    /// <summary>
    ///     Event triggered when the panning behaviour ends.
    /// </summary>
    event Action<double, double> OnPanEnded;

    /// <summary>
    ///     Triggers the <see cref="OnBeforeZoomChanged" /> event.
    /// </summary>
    /// <param name="zoom">The zoom value.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyBeforeZoomChanged(double zoom, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnZoomChanged" /> event.
    /// </summary>
    /// <param name="zoom">The zoom value.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyZoomChanged(double zoom, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPanStarted" /> event.
    /// </summary>
    /// <param name="panX">The X coordinate of the pan.</param>
    /// <param name="panY">The Y coordinate of the pan.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPanStart(double panX, double panY, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPanChanged" /> event.
    /// </summary>
    /// <param name="panX">The X coordinate of the pan.</param>
    /// <param name="panY">The Y coordinate of the pan.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPanChanged(double panX, double panY, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPanEnded" /> event.
    /// </summary>
    /// <param name="panX">The final X coordinate of the pan.</param>
    /// <param name="panY">The final Y coordinate of the pan.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPanEnd(double panX, double panY, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    /// Event triggered when the diagram's size changes.
    /// </summary>
    event Action<IDiagram> OnSizeChanged;

    /// <summary>
    /// Triggers the <see cref="OnSizeChanged"/> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifySizeChanged([CallerFilePath] string? callerMemberName = null);
}