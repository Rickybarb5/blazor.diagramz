﻿namespace Blazored.Diagrams.Diagrams.Options;

/// <summary>
///    Configurable diagram options
/// </summary>
public class DiagramOptions
{
    /// <summary>
    ///     Style options.
    /// </summary>
    public DiagramStyleOptions StyleOptions { get; set; } = new();

    /// <summary>
    /// General zoom options.
    /// </summary>
    public ZoomOptions ZoomOptions { get; set; } = new();

    /// <summary>
    /// Virtualization options.
    /// </summary>
    public VirtualizationOptions VirtualizationOptions { get; set; } = new();
}