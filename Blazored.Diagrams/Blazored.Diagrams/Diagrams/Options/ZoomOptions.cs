﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Diagrams.Behaviours;

namespace Blazored.Diagrams.Diagrams.Options;

/// <summary>
/// Options for <see cref="ZoomBehavior"/>
/// </summary>
public class ZoomOptions
{
    /// <summary>
    ///     Default value for  the zoom step.
    /// </summary>
    [JsonIgnore]
    public const double DefaultZoomStep = 0.05;

    /// <summary>
    ///     Default value for the minimum  zoom value.
    ///     Must be positive.
    /// </summary>
    [JsonIgnore]
    public const double DefaultMinimumZoom = 0.05;

    /// <summary>
    ///     Default value for the maximum zoom value.
    /// </summary>
    [JsonIgnore]
    public const double DefaultMaximumZoom = 3;


    /// <summary>
    ///     Enables or disables Zooming.
    /// </summary>
    
    public bool Enabled { get; set; } = true;

    /// <summary>
    ///     Minimum allowed zoom value of the diagram.
    /// </summary>
    public double MinZoom { get; set; } = DefaultMinimumZoom;

    /// <summary>
    ///     Maximum allowed zoom value of the diagram.
    /// </summary>
    public double MaxZoom { get; set; } = DefaultMaximumZoom;

    /// <summary>
    ///     Step value from zooming with the pointer wheel.
    /// </summary>
    public double ZoomStep { get; set; } = DefaultZoomStep;
}