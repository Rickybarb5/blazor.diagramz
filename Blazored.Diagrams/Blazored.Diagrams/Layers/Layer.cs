﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Layers.Behaviours;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Layers;

/// <summary>
///     Base implementation of a layer.
/// </summary>
public partial class Layer : ILayer
{
    private readonly ObservableList<IGroup> _groups = [];
    private readonly ObservableList<INode> _nodes = [];
    private bool _isVisible = true;

    /// <summary>
    ///     Initializes an <see cref="Layer" />
    /// </summary>
    public Layer()
    {
        Behaviours = new LayerBehaviourContainer(this);
    }

    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    public virtual ObservableList<INode> Nodes
    {
        get => _nodes;
        init
        {
            _nodes.Clear();
            foreach (var val in value) _nodes.Add(val);
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<IGroup> Groups
    {
        get => _groups;
        init
        {
            _groups.Clear();
            foreach (var val in value) _groups.Add(val);
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }

    /// <inheritdoc />
    public virtual bool IsVisible
    {
        get => _isVisible;
        set
        {
            if (_isVisible != value)
            {
                _isVisible = value;
                NotifyVisibilityChanged();
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<IGroup> AllGroups =>
        Groups
            .Concat(Groups.SelectMany(g => g.AllGroups))
            .DistinctBy(x => x.Id)
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<INode> AllNodes =>
        Nodes
            .Concat(Groups.SelectMany(g => g.AllNodes))
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<ILink> AllLinks => AllPorts
        .SelectMany(x => x.OutgoingLinks)
        .Concat(AllPorts.SelectMany(x => x.IncomingLinks))
        .DistinctBy(x => x.Id)
        .ToList()
        .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<IPort> AllPorts =>
        Nodes.SelectMany(x => x.Ports)
            .Concat(Groups.SelectMany(x => x.AllPorts))
            .DistinctBy(x => x.Id)
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    public virtual void Dispose()
    {
        _nodes.ForEach(x => x.Dispose());
        _nodes.Clear();
        _groups.ForEach(x => x.Dispose());
        _groups.Clear();
        Behaviours.Dispose();
    }
}