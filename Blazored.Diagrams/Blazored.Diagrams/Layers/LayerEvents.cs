﻿using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Helpers;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Layers;

/// <summary>
///     Base implementation of the layer events.
/// </summary>
public partial class Layer
{
    /// <inheritdoc />
    public event Action<ILayer, bool>? OnVisibilityChanged;

    /// <inheritdoc />
    public void NotifyVisibilityChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnVisibilityChanged triggered for Layer {Id} - Visibility: {IsVisible}",
            callerMemberName);
        OnVisibilityChanged?.Invoke(this, IsVisible);
    }

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupAdded;

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupRemoved;

    /// <inheritdoc />
    public void NotifyGroupAdded(IGroup addedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupAdded triggered - Group {addedGroup.Id} added to Layer {Id}",
            callerMemberName);
        OnGroupAdded?.Invoke(addedGroup);
    }

    /// <inheritdoc />
    public void NotifyGroupRemoved(IGroup removedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupRemoved triggered - Group {removedGroup.Id} removed from Layer {Id}",
            callerMemberName);
        OnGroupRemoved?.Invoke(removedGroup);
    }

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerDown;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerUp;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerMove;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerEnter;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerLeave;

    /// <inheritdoc />
    public event Action<IGroup, WheelEventArgs>? OnGroupWheel;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupClicked;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupDoubleClicked;

    /// <inheritdoc />
    public void NotifyGroupPointerDown(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerDown triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupPointerDown?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerUp(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerUp triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupPointerUp?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerMove(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnGroupPointerMove triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupPointerMove?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerEnter(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerEnter triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupPointerEnter?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerLeave(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerLeave triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupPointerLeave?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupWheel(IGroup group, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupWheel triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupWheel?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupClicked triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupClicked?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupDoubleClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupDoubleClicked triggered - Group {group.Id} in Layer {Id}", callerMemberName);
        OnGroupDoubleClicked?.Invoke(group, args);
    }

    /// <inheritdoc />
    public event Action<INode>? OnNodeAdded;

    /// <inheritdoc />
    public event Action<INode>? OnNodeRemoved;

    /// <inheritdoc />
    public void NotifyNodeAdded(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeAdded triggered - Node {node.Id} added to Layer {Id}", callerMemberName);
        OnNodeAdded?.Invoke(node);
    }

    /// <inheritdoc />
    public void NotifyNodeRemoved(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeRemoved triggered - Node {node.Id} removed from Layer {Id}", callerMemberName);
        OnNodeRemoved?.Invoke(node);
    }

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerDown;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerUp;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerMove;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerEnter;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerLeave;

    /// <inheritdoc />
    public event Action<INode, WheelEventArgs>? OnNodeWheel;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeClicked;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeDoubleClicked;

    /// <inheritdoc />
    public void NotifyNodePointerDown(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerDown triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodePointerDown?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerUp(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerUp triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodePointerUp?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerMove(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnNodePointerMove triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodePointerMove?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerEnter(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerEnter triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodePointerEnter?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerLeave(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerLeave triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodePointerLeave?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeWheel(INode node, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeWheel triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodeWheel?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeClicked(INode node, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeClicked triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodeClicked?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeDoubleClicked(INode node, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeDoubleClicked triggered - Node {node.Id} in Layer {Id}", callerMemberName);
        OnNodeDoubleClicked?.Invoke(node, args);
    }

    /// <inheritdoc />
    public event Action<ILink>? OnLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnLinkRemoved;

    /// <inheritdoc />
    public void NotifyLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkAdded triggered - Link {link.Id} added to Layer {Id}", callerMemberName);
        OnLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkRemoved triggered - Link {link.Id} removed from Layer {Id}", callerMemberName);
        OnLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerDown;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerUp;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerMove;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerEnter;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerLeave;

    /// <inheritdoc />
    public event Action<ILink, WheelEventArgs>? OnLinkWheel;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkClicked;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkDoubleClicked;

    /// <inheritdoc />
    public void NotifyLinkPointerDown(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerDown triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkPointerDown?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerUp(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerUp triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkPointerUp?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerMove(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnLinkPointerMove triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkPointerMove?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerEnter(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerEnter triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkPointerEnter?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerLeave(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerLeave triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkPointerLeave?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkWheel(ILink link, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkWheel triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkWheel?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkClicked(ILink link, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkClicked triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkClicked?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkDoubleClicked(ILink link, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkDoubleClicked triggered - Link {link.Id} in Layer {Id}", callerMemberName);
        OnLinkDoubleClicked?.Invoke(link, args);
    }

    /// <inheritdoc />
    public event Action<IPort>? OnPortAdded;

    /// <inheritdoc />
    public event Action<IPort>? OnPortRemoved;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortClicked;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortDoubleClicked;

    /// <inheritdoc />
    public void NotifyPortAdded(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortAdded triggered - Port {port.Id} added to Layer {Id}", callerMemberName);
        OnPortAdded?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortRemoved(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortRemoved triggered - Port {port.Id} removed from Layer {Id}", callerMemberName);
        OnPortRemoved?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortClicked triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortDoubleClicked(IPort port, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortDoubleClicked triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortDoubleClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnRedraw triggered for Layer {Id}", callerMemberName);
        OnRedraw?.Invoke();
    }

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkRemoved;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkRemoved;

    /// <inheritdoc />
    public void NotifyIncomingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnIncomingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyIncomingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnIncomingLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnOutgoingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnOutgoingLinkRemoved?.Invoke(link);
    }

    #region Port pointer events

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerDown;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerUp;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerMove;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerEnter;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerLeave;

    /// <inheritdoc />
    public event Action<IPort, WheelEventArgs>? OnPortWheel;

    /// <inheritdoc />
    public void NotifyPortPointerDown(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerDown triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortPointerDown?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerUp(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerUp triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortPointerUp?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerMove(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPortPointerMove triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortPointerMove?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerEnter(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerEnter triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortPointerEnter?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerLeave(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerLeave triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortPointerLeave?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortWheel(IPort port, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortWheel triggered - Port {port.Id} in Layer {Id}", callerMemberName);
        OnPortWheel?.Invoke(port, args);
    }

    #endregion
}