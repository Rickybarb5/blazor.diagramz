﻿using Blazored.Diagrams.Interfaces.Events;

namespace Blazored.Diagrams.Layers;

/// <summary>
///     Events that can be triggered by a layer.
/// </summary>
public interface ILayerEvents :
    IVisibilityEvents<ILayer>,
    INodeContainerEvents,
    IGroupContainerEvents,
    ILinkContainerEvents,
    IPortContainerEvents,
    IUiEvents;