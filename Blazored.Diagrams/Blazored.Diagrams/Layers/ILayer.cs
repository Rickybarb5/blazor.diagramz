﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Layers;

/// <summary>
///     Contains all methods relevant for layers.
/// </summary>
public interface ILayer : IId,
    IVisible,
    IModel,
    INodeContainer,
    IGroupContainer,
    ILayerEvents,
    IDisposable
{
    /// <summary>
    ///     Ports that are container within the layer.
    /// </summary>
    [JsonIgnore]
    IReadOnlyList<IPort> AllPorts { get; }

    /// <summary>
    ///     All top-level groups and nested groups.
    /// </summary>
    [JsonIgnore]
    public IReadOnlyList<IGroup> AllGroups { get; }

    /// <summary>
    ///     All top-level nodes and nested nodes.
    /// </summary>
    [JsonIgnore]
    public IReadOnlyList<INode> AllNodes { get; }

    /// <summary>
    ///     All link contained within this layer.
    /// </summary>
    [JsonIgnore]
    public IReadOnlyList<ILink> AllLinks { get; }

    /// <summary>
    ///     Behaviour management.
    /// </summary>
    public IBehaviourContainer Behaviours { get; }

    /// <summary>
    ///     Unselects all models within a layer.
    /// </summary>
    void UnselectAll();

    /// <summary>
    ///     Selects all models within a layer.
    /// </summary>
    void SelectAll();

    /// <summary>
    ///     Creates a link between two ports.
    ///     The two ports must be part of the layer.
    /// </summary>
    /// <param name="sourcePort">Source port of the link</param>
    /// <param name="targetPort">Target port of the link.</param>
    /// <typeparam name="TLinkComponent"></typeparam>
    ILink CreateLink<TLinkComponent>(IPort sourcePort, IPort? targetPort) where TLinkComponent : IComponent;

    /// <summary>
    ///     Creates a link between two ports.
    ///     The two ports must be part of the layer.
    /// </summary>
    /// <param name="sourcePort">Source port of the link</param>
    /// <param name="targetPort">Target port of the link.</param>
    /// <param name="linkComponentType">Link component Type</param>
    public ILink CreateLink(IPort sourcePort, IPort? targetPort, Type linkComponentType);

    /// <summary>
    ///     Removes a link from the layer.
    /// </summary>
    void RemoveLink(ILink linkToRemove);

    /// <summary>
    /// Removes a group from a layer, no matter how nested it is.
    /// </summary>
    /// <param name="groupToRemove"></param>
    /// <returns></returns>
    bool RemoveGroup(IGroup groupToRemove);

    /// <summary>
    /// Removes a node from a layer, no matter how nested it is.
    /// </summary>
    /// <param name="nodeToRemove"></param>
    /// <returns></returns>
    bool RemoveNode(INode nodeToRemove);
}