using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Layers;

public partial class Layer
{
    /// <inheritdoc />
    public virtual void UnselectAll()
    {
        Nodes.ForEach(x => x.IsSelected = false);
        Groups.ForEach(x =>
        {
            x.IsSelected = false;
            x.UnselectAll();
        });
        foreach (var link in AllLinks) link.IsSelected = false;
    }

    /// <inheritdoc />
    public virtual void SelectAll()
    {
        Nodes.ForEach(x => x.IsSelected = true);
        Groups.ForEach(x =>
        {
            x.IsSelected = true;
            x.SelectAll();
        });
        foreach (var link in AllLinks) link.IsSelected = true;
    }

    /// <inheritdoc />
    public virtual ILink CreateLink<TLinkComponent>(IPort sourcePort, IPort? targetPort)
        where TLinkComponent : IComponent
    {
        return CreateLink(sourcePort, targetPort, typeof(TLinkComponent));
    }

    /// <inheritdoc />
    public virtual ILink CreateLink(IPort sourcePort, IPort? targetPort, Type linkComponentType)
    {
        // Ensure the provided type implements IComponent
        if (!typeof(IComponent).IsAssignableFrom(linkComponentType))
        {
            throw new ArgumentException($"The type {linkComponentType.Name} must implement IComponent.",
                nameof(linkComponentType));
        }

        // Create an instance of the Link<TLinkComponent> using reflection
        var linkType = typeof(Link<>).MakeGenericType(linkComponentType);
        var link = (ILink?)Activator.CreateInstance(linkType);
        if (link is null)
        {
            throw new InvalidOperationException(
                $"Link couldn't be created with component type: {linkComponentType.Name}");
        }

        if (targetPort is not null)
        {
            CanLinkBeCreated(sourcePort, targetPort);
        }

        sourcePort.OutgoingLinks.Add(link);
        targetPort?.IncomingLinks.Add(link);
        return link;
    }

    /// <inheritdoc />
    public virtual void RemoveLink(ILink linkToRemove)
    {
        linkToRemove.Dispose();
    }

    /// <inheritdoc />
    public virtual bool RemoveGroup(IGroup groupToRemove)
    {
        var removed = Groups.Remove(groupToRemove);
        if (removed) return true;
        return Groups.Select(group => group.RemoveGroup(groupToRemove)).Any(r => removed);
    }

    /// <inheritdoc />
    public virtual bool RemoveNode(INode nodeToRemove)
    {
        var removed = Nodes.Remove(nodeToRemove);
        if (removed) return true;
        return Groups.Select(group => group.RemoveNode(nodeToRemove)).Any(groupNodeRemoved => groupNodeRemoved);
    }

    /// <summary>
    ///  Internal function that defines if a link can be created.
    /// </summary>
    /// <param name="sourcePort"></param>
    /// <param name="targetPort"></param>
    /// <exception cref="InvalidOperationException"></exception>
    protected virtual void CanLinkBeCreated(IPort sourcePort, IPort targetPort)
    {
        if (AllPorts.FirstOrDefault(x => x.Id == sourcePort.Id) is null)
        {
            throw new InvalidOperationException(
                "Source port does not belong to the current layer. Check if port and parent have been added to the diagram.");
        }

        if (AllPorts.FirstOrDefault(x => x.Id == targetPort.Id) is null)
        {
            throw new InvalidOperationException(
                "Target port does not belong to the current layer. Check if port and parent have been added to the diagram.");
        }

        if (!sourcePort.CanCreateLink())
        {
            throw new InvalidOperationException(
                $"Source port cannot be linked to target port. Modify {nameof(sourcePort.CanCreateLink)}");
        }
    }
}