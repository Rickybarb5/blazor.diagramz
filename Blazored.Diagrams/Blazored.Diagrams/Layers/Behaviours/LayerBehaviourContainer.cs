using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Layers.Behaviours;

public class LayerBehaviourContainer : BehaviourContainer
{
    public LayerBehaviourContainer(ILayer layer)
    {
        Add(new EventPipelineBehaviour<ILayer>(layer));
        Add(new RedrawBehaviour<ILayer>(layer));
        Add(new NodeContainerBehaviour<ILayer>(layer));
        Add(new GroupContainerBehaviour<ILayer>(layer));
    }
}