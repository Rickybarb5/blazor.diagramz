using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Nodes.Behaviours;

/// <summary>
///     Move child models when the group moves.
/// </summary>
public class NodeMoveBehaviour : IBehaviour
{
    private readonly INode _node;

    /// <summary>
    /// Instantiates a new <see cref="NodeMoveBehaviour"/>
    /// </summary>
    /// <param name="node"></param>
    public NodeMoveBehaviour(INode node)
    {
        _node = node;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _node.OnPositionChanged -= MoveChildren;
        _node.OnSizeChanged -= MoveChildren;
    }

    private void EnableBehaviour()
    {
        _node.OnPositionChanged += MoveChildren;
        _node.OnSizeChanged += MoveChildren;
    }

    private void MoveChildren(IPosition obj)
    {
        _node.Ports.RefreshPosition();
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}