using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Nodes.Behaviours;

public class NodeBehaviourContainer : BehaviourContainer
{
    public NodeBehaviourContainer(INode node)
    {
        Add(new EventPipelineBehaviour<INode>(node));
        Add(new PortContainerBehaviour<INode>(node));
        Add(new NodeMoveBehaviour(node));
        Add(new RedrawBehaviour<INode>(node));
    }
}