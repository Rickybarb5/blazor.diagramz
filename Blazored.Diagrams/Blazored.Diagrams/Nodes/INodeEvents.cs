﻿using Blazored.Diagrams.Interfaces.Events;

namespace Blazored.Diagrams.Nodes;

/// <summary>
///     Events that can be triggered by a node.
/// </summary>
public interface INodeEvents :
    IVisibilityEvents<INode>,
    ISelectionEvents<INode>,
    IPositionEvents<INode>,
    IPortContainerEvents,
    ISizeEvents<INode>,
    IPointerEvents<INode>,
    IUiEvents
{
}