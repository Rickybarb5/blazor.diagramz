﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Nodes;

/// <summary>
///     Base interface to implement a base node class.
/// </summary>
public interface INode : 
    IId,
    IVisible,
    ISelectable,
    IUiComponentType,
    IPortParent,
    IPortContainer,
    IModel,
    INodeEvents,
    IDisposable
{
    /// <summary>
    /// Behaviour management.
    /// </summary>
    IBehaviourContainer Behaviours { get; }
}