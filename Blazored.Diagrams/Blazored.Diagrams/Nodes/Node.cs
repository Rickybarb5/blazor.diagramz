﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Nodes.Behaviours;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Nodes;

/// <summary>
///     Base class for all nodes.
/// </summary>
/// <typeparam name="TNodeComponent">Razor component to be displayed.</typeparam>
public partial class Node<TNodeComponent> : INode where TNodeComponent : IComponent
{
    private readonly ObservableList<IPort> _ports = [];
    private int _height;
    private bool _isSelected;
    private bool _isVisible = true;
    private int _positionX;
    private int _positionY;
    private int _width;

    /// <summary>
    /// Instantiates a new <see cref="Node{TNodeComponent}"/>
    /// </summary>
    public Node()
    {
        Parameters = new Dictionary<string, object> { { nameof(NodeContainer.Node), this } };
        Behaviours = new NodeBehaviourContainer(this);
    }


    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Type ComponentType { get; init; } = typeof(TNodeComponent);

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Dictionary<string, object> Parameters { get; init; }

    /// <inheritdoc />
    public virtual int Width
    {
        get => _width;
        set
        {
            if (_width != value)
            {
                _width = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int Height
    {
        get => _height;
        set
        {
            if (_height != value)
            {
                _height = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionX
    {
        get => _positionX;
        set
        {
            if (_positionX != value)
            {
                NotifyBeforePositionChanged();
                _positionX = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionY
    {
        get => _positionY;
        set
        {
            if (_positionY != value)
            {
                NotifyBeforePositionChanged();
                _positionY = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsSelected
    {
        get => _isSelected;
        set
        {
            if (_isSelected != value)
            {
                _isSelected = value;
                NotifySelectionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsVisible
    {
        get => _isVisible;
        set
        {
            if (_isVisible != value)
            {
                _isVisible = value;
                NotifyVisibilityChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<IPort> Ports
    {
        get => _ports;
        init
        {
            _ports.Clear();
            foreach (var val in value) _ports.Add(val);
        }
    }

    /// <inheritdoc />
    public virtual void Dispose()
    {
        _ports.ForEach(x => x.Dispose());
        _ports.Clear();
        Behaviours.Dispose();
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }
}