namespace Blazored.Diagrams.Nodes;

public partial class Node<TNodeComponent>
{
    /// <inheritdoc />
    public virtual void SetPosition(int x, int y)
    {
        var stateChanged = x != _positionX || y != _positionY;
        if (stateChanged)
        {
            NotifyBeforePositionChanged();
            _positionX = x;
            _positionY = y;
            NotifyPositionChanged();
        }
    }

    /// <inheritdoc />
    public virtual void SetSize(int width, int height)
    {
        var stateChanged = width != _width || _height != height;
        if (stateChanged)
        {
            _width = width;
            _height = height;
            NotifySizeChanged();
        }
    }
}