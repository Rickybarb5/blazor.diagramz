﻿using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Nodes;

/// <summary>
///     Implementation of the node events.
/// </summary>
public partial class Node<TNodeComponent>
{
    /// <inheritdoc />
    public event Action<INode, bool>? OnVisibilityChanged;

    /// <inheritdoc />
    public event Action<INode>? OnPositionChanged;

    /// <inheritdoc />
    public event Action<INode>? OnBeforePositionChanged;

    /// <inheritdoc />
    public event Action<INode, bool>? OnSelectionChanged;

    /// <inheritdoc />
    public event Action<INode>? OnSizeChanged;

    /// <inheritdoc />
    public void NotifySizeChanged([CallerFilePath] string? callerFilePath = null)
    {
        OnSizeChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public event Action<IPort>? OnPortAdded;

    /// <inheritdoc />
    public event Action<IPort>? OnPortRemoved;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortClicked;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortDoubleClicked;

    /// <inheritdoc />
    public void NotifyPortAdded(IPort port, [CallerFilePath] string? callerFilePath = null)
    {
        OnPortAdded?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortRemoved(IPort port, [CallerFilePath] string? callerFilePath = null)
    {
        OnPortRemoved?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPortClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortDoubleClicked(IPort port, MouseEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortDoubleClicked?.Invoke(port, args);
    }

    #region Pointer events

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnPointerDown;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnPointerUp;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnPointerMove;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnPointerEnter;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnPointerLeave;

    /// <inheritdoc />
    public event Action<INode, WheelEventArgs>? OnWheel;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnClick;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnDoubleClick;

    /// <inheritdoc />
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPointerDown?.Invoke(this, args);
    }


    /// <inheritdoc />
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPointerUp?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPointerMove?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPointerEnter?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPointerLeave?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnWheel?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnDoubleClick?.Invoke(this, args);
    }

    #endregion

    #region Port pointer events

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerDown;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerUp;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerMove;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerEnter;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerLeave;

    /// <inheritdoc />
    public event Action<IPort, WheelEventArgs>? OnPortWheel;

    /// <inheritdoc />
    public void NotifyPortPointerDown(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortPointerDown?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerUp(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortPointerUp?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerMove(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortPointerMove?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerEnter(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortPointerEnter?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerLeave(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerFilePath = null)
    {
        OnPortPointerLeave?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortWheel(IPort port, WheelEventArgs args, [CallerFilePath] string? callerFilePath = null)
    {
        OnPortWheel?.Invoke(port, args);
    }

    #endregion

    /// <inheritdoc />
    public void NotifyVisibilityChanged([CallerFilePath] string? callerFilePath = null)
    {
        OnVisibilityChanged?.Invoke(this, IsVisible);
    }

    /// <inheritdoc />
    public void NotifyPositionChanged([CallerFilePath] string? callerFilePath = null)
    {
        OnPositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public void NotifyBeforePositionChanged([CallerFilePath] string? callerFilePath = null)
    {
        OnBeforePositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public void NotifySelectionChanged([CallerFilePath] string? callerFilePath = null)
    {
        OnSelectionChanged?.Invoke(this, IsSelected);
    }

    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerFilePath = null)
    {
        OnRedraw?.Invoke();
    }
}