using System.Diagnostics.CodeAnalysis;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Services.Options;

namespace Blazored.Diagrams.Events;

[ExcludeFromCodeCoverage]
public static class EventInvoker
{
    public static void InvokeEvent(Delegate eventDelegate, params object[] parameters)
    {
        if (DiagramLoggingOptions.LoggingEnabled)
        {
            StaticLogger.Log($"Event: {nameof(eventDelegate)} triggered.");
        }

        // Check if there are subscribers to the event
        if (eventDelegate != null)
        {
            // Invoke the event with the provided parameters
            eventDelegate.DynamicInvoke(parameters);
        }
    }
}