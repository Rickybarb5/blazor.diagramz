﻿using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Groups;

/// <summary>
///     Interface to implement basic group features.
/// </summary>
public interface IGroup :
    IId,
    IVisible,
    INodeContainer,
    IGroupContainer,
    IPortContainer,
    ISelectable,
    IUiComponentType,
    IPortParent,
    IModel,
    IGroupEvents,
    IPadding,
    IDisposable
{
    /// <summary>
    ///     Gets all nodes and nested Nodes.
    /// </summary>
    IReadOnlyList<INode> AllNodes { get; }

    /// <summary>
    ///     Gets all groups and nested groups.
    /// </summary>
    IReadOnlyList<IGroup> AllGroups { get; }

    /// <summary>
    ///     Gets all ports and nested ports.
    /// </summary>
    IReadOnlyList<IPort> AllPorts { get; }

    /// <summary>
    ///     Behaviour management.
    /// </summary>
    IBehaviourContainer Behaviours { get; }

    /// <summary>
    ///     Selects all models within a group.
    /// </summary>
    void SelectAll();

    /// <summary>
    ///     Unselects the group and all nested models.
    /// </summary>
    void UnselectAll();

    /// <summary>
    ///     Sets the X and Y coordinates on the screen.
    ///     Does not trigger the event.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    void SetPositionInternal(int x, int y);

    /// <summary>
    ///     Sets the width and height.
    ///     Does not trigger the event.
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    void SetSizeInternal(int width, int height);

    /// <summary>
    /// Removes a group from a group, no matter how nested it is.
    /// </summary>
    /// <param name="groupToRemove"></param>
    /// <returns></returns>
    bool RemoveGroup(IGroup groupToRemove);

    /// <summary>
    /// Removes a node from a group, no matter how nested it is.
    /// </summary>
    /// <param name="nodeToRemove"></param>
    /// <returns></returns>
    bool RemoveNode(INode nodeToRemove);
}