﻿using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Groups;

/// <summary>
///     Base implementation of the group events.
/// </summary>
public partial class Group<TGroupComponent> : IGroupEvents
{
    /// <inheritdoc />
    public event Action<IGroup, bool>? OnVisibilityChanged;

    /// <inheritdoc />
    public event Action<IGroup>? OnPositionChanged;

    /// <inheritdoc />
    public event Action<IGroup>? OnBeforePositionChanged;

    /// <inheritdoc />
    public event Action<IGroup, bool>? OnSelectionChanged;

    /// <inheritdoc />
    public void NotifySelectionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSelectionChanged triggered for Group {Id} - Selected: {IsSelected}",
            callerMemberName);
        OnSelectionChanged?.Invoke(this, IsSelected);
    }

    /// <inheritdoc />
    public event Action<IGroup>? OnSizeChanged;

    /// <inheritdoc />
    public void NotifySizeChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSizeChanged triggered for Group {Id}", callerMemberName);
        OnSizeChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public event Action<INode>? OnNodeAdded;

    /// <inheritdoc />
    public event Action<INode>? OnNodeRemoved;

    /// <inheritdoc />
    public void NotifyNodeAdded(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeAdded triggered - Node {node.Id} was added to Group {Id}", callerMemberName);
        OnNodeAdded?.Invoke(node);
    }

    /// <inheritdoc />
    public void NotifyNodeRemoved(INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeRemoved triggered - Node {node.Id} was removed from Group {Id}",
            callerMemberName);
        OnNodeRemoved?.Invoke(node);
    }

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerDown;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerUp;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerMove;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerEnter;

    /// <inheritdoc />
    public event Action<INode, PointerEventArgs>? OnNodePointerLeave;

    /// <inheritdoc />
    public event Action<INode, WheelEventArgs>? OnNodeWheel;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeClicked;

    /// <inheritdoc />
    public event Action<INode, MouseEventArgs>? OnNodeDoubleClicked;

    /// <inheritdoc />
    public void NotifyNodePointerDown(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerDown triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodePointerDown?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerUp(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerUp triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodePointerUp?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerMove(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnNodePointerMove triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodePointerMove?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerEnter(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerEnter triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodePointerEnter?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodePointerLeave(INode node, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodePointerLeave triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodePointerLeave?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeWheel(INode node, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeWheel triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodeWheel?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeClicked(INode node, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeClicked triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodeClicked?.Invoke(node, args);
    }

    /// <inheritdoc />
    public void NotifyNodeDoubleClicked(INode node, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNodeDoubleClicked triggered - Node: {node.Id} in Group {Id}", callerMemberName);
        OnNodeDoubleClicked?.Invoke(node, args);
    }

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupAdded;

    /// <inheritdoc />
    public event Action<IGroup>? OnGroupRemoved;

    /// <inheritdoc />
    public void NotifyGroupAdded(IGroup addedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupAdded triggered - Group {addedGroup.Id} was added to Group {Id}",
            callerMemberName);
        OnGroupAdded?.Invoke(addedGroup);
    }

    /// <inheritdoc />
    public void NotifyGroupRemoved(IGroup removedGroup, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupRemoved triggered - Group {removedGroup.Id} was removed from Group {Id}",
            callerMemberName);
        OnGroupRemoved?.Invoke(removedGroup);
    }

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerDown;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerUp;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerMove;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerEnter;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerLeave;

    /// <inheritdoc />
    public event Action<IGroup, WheelEventArgs>? OnGroupWheel;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupClicked;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnGroupDoubleClicked;

    /// <inheritdoc />
    public void NotifyGroupPointerDown(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerDown triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupPointerDown?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerUp(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerUp triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupPointerUp?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerMove(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnGroupPointerMove triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupPointerMove?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerEnter(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerEnter triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupPointerEnter?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupPointerLeave(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupPointerLeave triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupPointerLeave?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupWheel(IGroup group, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupWheel triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupWheel?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupClicked triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupClicked?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupDoubleClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnGroupDoubleClicked triggered - Group {group.Id} in Group {Id}", callerMemberName);
        OnGroupDoubleClicked?.Invoke(group, args);
    }

    /// <inheritdoc />
    public void NotifyGroupAdded(IGroup parentGroup, IGroup addedGroup,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnGroupAdded triggered - Group {addedGroup.Id} added to parent Group {parentGroup.Id}",
            callerMemberName);
        OnGroupAdded?.Invoke(addedGroup);
    }

    /// <inheritdoc />
    public void NotifyGroupRemoved(IGroup parentGroup, IGroup removedGroup,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnGroupRemoved triggered - Group {removedGroup.Id} removed from parent Group {parentGroup.Id}",
            callerMemberName);
        OnGroupRemoved?.Invoke(removedGroup);
    }

    /// <inheritdoc />
    public event Action<IPort>? OnPortAdded;

    /// <inheritdoc />
    public event Action<IPort>? OnPortRemoved;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortClicked;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnPortDoubleClicked;

    /// <inheritdoc />
    public void NotifyPortAdded(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortAdded triggered - Port {port.Id} added to Group {Id}", callerMemberName);
        OnPortAdded?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortRemoved(IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortRemoved triggered - Port {port.Id} removed from Group {Id}", callerMemberName);
        OnPortRemoved?.Invoke(port);
    }

    /// <inheritdoc />
    public void NotifyPortClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortClicked triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortDoubleClicked(IPort port, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortDoubleClicked triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortDoubleClicked?.Invoke(port, args);
    }

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerDown;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerUp;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerMove;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerEnter;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPortPointerLeave;

    /// <inheritdoc />
    public event Action<IPort, WheelEventArgs>? OnPortWheel;

    /// <inheritdoc />
    public void NotifyPortPointerDown(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerDown triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortPointerDown?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerUp(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerUp triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortPointerUp?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerMove(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPortPointerMove triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortPointerMove?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerEnter(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerEnter triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortPointerEnter?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortPointerLeave(IPort port, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPointerLeave triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortPointerLeave?.Invoke(port, args);
    }

    /// <inheritdoc />
    public void NotifyPortWheel(IPort port, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortWheel triggered - Port {port.Id} in Group {Id}", callerMemberName);
        OnPortWheel?.Invoke(port, args);
    }

    /// <inheritdoc />
    public event Action<IGroup, IGroup>? OnNestedGroupAdded;

    /// <inheritdoc />
    public void NotifyNestedGroupAdded(IGroup parentGroup, IGroup addedGroup,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnNestedGroupAdded triggered - Group {addedGroup.Id} nested in Group {parentGroup.Id}",
            callerMemberName);
        OnNestedGroupAdded?.Invoke(parentGroup, addedGroup);
    }

    /// <inheritdoc />
    public event Action<IGroup, IGroup>? OnNestedGroupRemoved;

    /// <inheritdoc />
    public void NotifyNestedGroupRemoved(IGroup parentGroup, IGroup removedGroup,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnNestedGroupRemoved triggered - Group {removedGroup.Id} removed from nested Group {parentGroup.Id}",
            callerMemberName);
        OnNestedGroupRemoved?.Invoke(parentGroup, removedGroup);
    }

    /// <inheritdoc />
    public event Action<IGroup, INode>? OnNestedNodeAdded;

    /// <inheritdoc />
    public event Action<IGroup, INode>? OnNestedNodeRemoved;

    /// <inheritdoc />
    public void NotifyNestedNodeAdded(IGroup group, INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNestedNodeAdded triggered - Node {node.Id} nested in Group {group.Id}",
            callerMemberName);
        OnNestedNodeAdded?.Invoke(group, node);
    }

    /// <inheritdoc />
    public void NotifyNestedNodeRemoved(IGroup group, INode node, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNestedNodeRemoved triggered - Node {node.Id} removed from nested Group {group.Id}",
            callerMemberName);
        OnNestedNodeRemoved?.Invoke(group, node);
    }

    /// <inheritdoc />
    public void NotifyNestedPortAdded(IGroup parent, IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNestedPortAdded triggered - Port {port.Id} added to nested Group {parent.Id}",
            callerMemberName);
        OnNestedPortAdded?.Invoke(parent, port);
    }

    /// <inheritdoc />
    public void NotifyNestedPortRemoved(IGroup parent, IPort port, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnNestedPortRemoved triggered - Port {port.Id} removed from nested Group {parent.Id}",
            callerMemberName);
        OnNestedPortRemoved?.Invoke(parent, port);
    }

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnPointerDown;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnPointerUp;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnPointerMove;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnPointerEnter;

    /// <inheritdoc />
    public event Action<IGroup, PointerEventArgs>? OnPointerLeave;

    /// <inheritdoc />
    public event Action<IGroup, WheelEventArgs>? OnWheel;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnClick;

    /// <inheritdoc />
    public event Action<IGroup, MouseEventArgs>? OnDoubleClick;

    /// <inheritdoc />
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerDown triggered for Group {Id}", callerMemberName);
        OnPointerDown?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerUp triggered for Group {Id}", callerMemberName);
        OnPointerUp?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerMove triggered for Group {Id}", callerMemberName);
        OnPointerMove?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerEnter triggered for Group {Id}", callerMemberName);
        OnPointerEnter?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerLeave triggered for Group {Id}", callerMemberName);
        OnPointerLeave?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnWheel triggered for Group {Id}", callerMemberName);
        OnWheel?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnClick triggered for Group {Id}", callerMemberName);
        OnClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnDoubleClick triggered for Group {Id}", callerMemberName);
        OnDoubleClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyVisibilityChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnVisibilityChanged triggered for Group {Id} - Visibility: {IsVisible}",
            callerMemberName);
        OnVisibilityChanged?.Invoke(this, IsVisible);
    }

    /// <inheritdoc />
    public void NotifyPositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPositionChanged triggered for Group {Id} - Position: X={PositionX}, Y={PositionY}",
            callerMemberName);
        OnPositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public void NotifyBeforePositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnBeforePositionChanged triggered for Group {Id} - Current Position: X={PositionX}, Y={PositionY}",
            callerMemberName);
        OnBeforePositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnRedraw triggered for Group {Id}", callerMemberName);
        OnRedraw?.Invoke();
    }

    /// <inheritdoc />
    public event Action<int>? OnPaddingChanged;

    /// <inheritdoc />
    public void NotifyPaddingChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPaddingChanged triggered for Group {Id} - New Padding: {Padding}",
            callerMemberName);
        OnPaddingChanged?.Invoke(Padding);
    }

    /// <inheritdoc />
    public event Action<IGroup, IPort>? OnNestedPortAdded;

    /// <inheritdoc />
    public event Action<IGroup, IPort>? OnNestedPortRemoved;
}