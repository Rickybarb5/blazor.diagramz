﻿using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Groups.Behaviours;

/// <summary>
///     Resizes a group when necessary.
/// </summary>
public class GroupResizeBehaviour : IBehaviour
{
    private readonly IGroup _behaviourGroup;

    /// <summary>
    /// Instantiates a new <see cref="GroupResizeBehaviour"/>
    /// </summary>
    public GroupResizeBehaviour(IGroup behaviourGroup)
    {
        _behaviourGroup = behaviourGroup;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _behaviourGroup.OnNodeAdded -= NestedNodeAdded;
        _behaviourGroup.OnNodeRemoved -= NestedNodeRemoved;
        _behaviourGroup.OnGroupAdded -= NestedGroupAdded;
        _behaviourGroup.OnGroupRemoved -= NestedGroupRemoved;
        _behaviourGroup.OnPaddingChanged -= ResizeGroup;
    }

    private void EnableBehaviour()
    {
        _behaviourGroup.OnNodeAdded += NestedNodeAdded;
        _behaviourGroup.OnNodeRemoved += NestedNodeRemoved;
        _behaviourGroup.OnGroupAdded += NestedGroupAdded;
        _behaviourGroup.OnGroupRemoved += NestedGroupRemoved;
        _behaviourGroup.OnPaddingChanged += ResizeGroup;
    }

    private void ResizeGroup(int padding)
    {
        Resize(_behaviourGroup);
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void Resize(IPosition position)
    {
        if (!_behaviourGroup.Nodes.Any() && !_behaviourGroup.Groups.Any()) return;

        var minX = int.MaxValue;
        var minY = int.MaxValue;
        var maxX = int.MinValue;
        var maxY = int.MinValue;
        var minHeight = int.MaxValue;
        var maxWidth = int.MinValue;

        // Calculate bounding box for nodes
        foreach (var node in _behaviourGroup.AllNodes)
        {
            minX = Math.Min(minX, node.PositionX);
            minY = Math.Min(minY, node.PositionY - node.Height);
            maxX = Math.Max(maxX, node.PositionX + node.Width);
            maxY = Math.Max(maxY, node.PositionY);

            // Save height of the node with minX and width of the node with maxY
            if (node.PositionX == minX)
                minHeight = Math.Min(minHeight, node.Height);
            if (node.PositionY == maxY)
                maxWidth = Math.Max(maxWidth, node.Width);
        }

        // Calculate bounding box for nested groups
        foreach (var nestedGroup in _behaviourGroup.AllGroups)
        {
            minX = Math.Min(minX, nestedGroup.PositionX);
            minY = Math.Min(minY, nestedGroup.PositionY + nestedGroup.Height);
            maxX = Math.Max(maxX, nestedGroup.PositionX - nestedGroup.Width);
            maxY = Math.Max(maxY, nestedGroup.PositionY);

            // Save height of the group with minX and width of the group with maxY
            if (nestedGroup.PositionX == minX)
                minHeight = Math.Min(minHeight, nestedGroup.Height);
            if (nestedGroup.PositionY == maxY)
                maxWidth = Math.Max(maxWidth, nestedGroup.Width);
        }

        // Update group position and size
        var newWidth = maxX - minX + _behaviourGroup.Padding * 2;
        var newHeight = maxY - minY + _behaviourGroup.Padding * 2;
        var newPositionX = minX - _behaviourGroup.Padding;
        var newPositionY = minY + minHeight - _behaviourGroup.Padding;
        _behaviourGroup.SetPositionInternal(newPositionX, newPositionY);
        _behaviourGroup.SetSizeInternal(newWidth, newHeight);
        _behaviourGroup.Ports.RefreshPosition();
        _behaviourGroup.NotifyRedraw();
    }

    private void NestedNodeAdded(INode node)
    {
        node.OnPositionChanged += Resize;
        node.OnSizeChanged += Resize;
        Resize(_behaviourGroup);
    }

    private void NestedGroupAdded(IGroup group)
    {
        group.OnNodeAdded += NestedNodeAdded;
        group.OnGroupAdded += NestedGroupAdded;
        group.OnGroupRemoved += NestedGroupRemoved;
        group.OnNodeRemoved += NestedNodeRemoved;
        group.OnPositionChanged += Resize;
        group.OnSizeChanged += Resize;
        group.OnRedraw += Resize;
        Resize(_behaviourGroup);
    }

    private void Resize()
    {
        Resize(_behaviourGroup);
    }

    private void NestedGroupRemoved(IGroup group)
    {
        group.OnNodeAdded -= NestedNodeAdded;
        group.OnGroupAdded -= NestedGroupAdded;
        group.OnGroupRemoved -= NestedGroupRemoved;
        group.OnNodeRemoved -= NestedNodeRemoved;
        group.OnPositionChanged -= Resize;
        group.OnSizeChanged -= Resize;
        group.OnRedraw -= Resize;
        Resize(_behaviourGroup);
    }

    private void NestedNodeRemoved(INode node)
    {
        node.OnPositionChanged -= Resize;
        node.OnSizeChanged -= Resize;
        Resize(_behaviourGroup);
    }
}