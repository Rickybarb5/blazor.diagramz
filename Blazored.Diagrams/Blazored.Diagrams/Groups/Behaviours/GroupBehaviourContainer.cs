using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Groups.Behaviours;

/// <summary>
/// Group behaviour container with default events.
/// </summary>
public class GroupBehaviourContainer : BehaviourContainer
{
    /// <inheritdoc />
    public GroupBehaviourContainer(IGroup group)
    {
        Add(new DefaultGroupBehaviour(group));
        Add(new GroupEventPipelineBehaviour(group));
        Add(new PortContainerBehaviour<IGroup>(group));
        Add(new NodeContainerBehaviour<IGroup>(group));
        Add(new GroupContainerBehaviour<IGroup>(group));
        Add(new GroupResizeBehaviour(group));
        Add(new GroupMoveBehaviour(group));
        Add(new RedrawBehaviour<IGroup>(group));
    }
}