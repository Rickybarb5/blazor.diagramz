using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Groups.Behaviours;

/// <summary>
///     Event pipeline for classes that implement <see cref="IGroup" />
/// </summary>
public class GroupEventPipelineBehaviour : IBehaviour
{
    private readonly IGroup _parentGroup;
    private readonly Dictionary<IGroup, GroupEventHandlers> _groupEventHandlers = new();
    private readonly Dictionary<INode, NodeEventHandlers> _nodeEventHandlers = new();
    private readonly Dictionary<IPort, PortEventHandlers> _portEventHandlers = new();
    private bool _isEnabled = true;

    /// <summary>
    ///     Initializes a new <see cref="GroupEventPipelineBehaviour" />.
    /// </summary>
    /// <param name="parentGroup"></param>
    public GroupEventPipelineBehaviour(IGroup parentGroup)
    {
        _parentGroup = parentGroup;
        EnableBehaviour();
    }

    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private class GroupEventHandlers
    {
        // Existing handlers for nested notifications
        public Action<IGroup> GroupAddedHandler { get; init; }
        public Action<IGroup> GroupRemovedHandler { get; init; }
        public Action<INode> NodeAddedHandler { get; init; }
        public Action<INode> NodeRemovedHandler { get; init; }
        public Action<IPort> PortAddedHandler { get; init; }
        public Action<IPort> PortRemovedHandler { get; init; }

        // Direct event handlers for the parent group
        public Action<IGroup> DirectGroupAddedHandler { get; init; }
        public Action<IGroup> DirectGroupRemovedHandler { get; init; }
        public Action<INode> DirectNodeAddedHandler { get; init; }
        public Action<INode> DirectNodeRemovedHandler { get; init; }
        public Action<IPort> DirectPortAddedHandler { get; init; }
        public Action<IPort> DirectPortRemovedHandler { get; init; }
        public Action<IGroup, PointerEventArgs> PointerDownHandler { get; init; }
        public Action<IGroup, PointerEventArgs> PointerEnterHandler { get; init; }
        public Action<IGroup, PointerEventArgs> PointerUpHandler { get; init; }
        public Action<IGroup, PointerEventArgs> PointerMoveHandler { get; init; }
        public Action<IGroup, WheelEventArgs> WheelHandler { get; init; }
        public Action<IGroup, PointerEventArgs> PointerLeaveHandler { get; init; }
        public Action<IGroup, MouseEventArgs> ClickHandler { get; init; }
        public Action<IGroup, MouseEventArgs> DoubleClickHandler { get; init; }
    }


    private void EnableBehaviour()
    {
        SetupGroupContainer();
        SetupNodeContainer();
        SetupPortContainer();
    }

    private void DisableBehaviour()
    {
        foreach (var group in _groupEventHandlers.Keys.ToList())
        {
            StopPropagation(group);
        }

        _groupEventHandlers.Clear();

        foreach (var node in _nodeEventHandlers.Keys.ToList())
        {
            StopPropagation(node);
        }

        _nodeEventHandlers.Clear();

        foreach (var port in _portEventHandlers.Keys.ToList())
        {
            StopPropagation(port);
        }

        _portEventHandlers.Clear();

        _parentGroup.Groups.OnItemAdded -= Propagate;
        _parentGroup.Groups.OnItemRemoved -= StopPropagation;
        _parentGroup.Nodes.OnItemAdded -= Propagate;
        _parentGroup.Nodes.OnItemRemoved -= StopPropagation;
        _parentGroup.Ports.OnItemAdded -= Propagate;
        _parentGroup.Ports.OnItemRemoved -= StopPropagation;
    }

    private void SetupGroupContainer()
    {
        _parentGroup.Groups.OnItemAdded += Propagate;
        _parentGroup.Groups.OnItemRemoved += StopPropagation;
        _parentGroup.Groups.ForEach(Propagate);
    }

    private void SetupNodeContainer()
    {
        _parentGroup.Nodes.OnItemAdded += Propagate;
        _parentGroup.Nodes.OnItemRemoved += StopPropagation;
        _parentGroup.Nodes.ForEach(Propagate);
    }

    private void SetupPortContainer()
    {
        _parentGroup.Ports.OnItemAdded += Propagate;
        _parentGroup.Ports.OnItemRemoved += StopPropagation;
        _parentGroup.Ports.ForEach(Propagate);
    }

    private void Propagate(IGroup addedGroup)
    {
        var handlers = new GroupEventHandlers
        {
            // Handlers for nested notifications
            GroupAddedHandler = (newChildGroup) => { _parentGroup.NotifyNestedGroupAdded(addedGroup, newChildGroup); },
            GroupRemovedHandler = (removedChildGroup) =>
            {
                _parentGroup.NotifyNestedGroupRemoved(addedGroup, removedChildGroup);
            },
            NodeAddedHandler = (node) => { _parentGroup.NotifyNestedNodeAdded(addedGroup, node); },
            NodeRemovedHandler = (node) => { _parentGroup.NotifyNestedNodeRemoved(addedGroup, node); },
            PortAddedHandler = (port) => { _parentGroup.NotifyNestedPortAdded(addedGroup, port); },
            PortRemovedHandler = (port) => { _parentGroup.NotifyNestedPortRemoved(addedGroup, port); },
            // Direct event handlers
            DirectGroupAddedHandler = (group) => { _parentGroup.NotifyGroupAdded(group); },
            DirectGroupRemovedHandler = (group) => { _parentGroup.NotifyGroupRemoved(group); },
            DirectNodeAddedHandler = (node) => { _parentGroup.NotifyNodeAdded(node); },
            DirectNodeRemovedHandler = (node) => { _parentGroup.NotifyNodeRemoved(node); },
            DirectPortAddedHandler = (port) => { _parentGroup.NotifyPortAdded(port); },
            DirectPortRemovedHandler = (port) => { _parentGroup.NotifyPortRemoved(port); },

            PointerDownHandler = (g, args) =>
                _parentGroup.NotifyGroupPointerDown(g, args),
            PointerEnterHandler = (g, args) =>
                _parentGroup.NotifyGroupPointerEnter(g, args),
            PointerUpHandler = (g, args) => _parentGroup.NotifyGroupPointerUp(g, args),
            PointerMoveHandler = (g, args) =>
                _parentGroup.NotifyGroupPointerMove(g, args),
            WheelHandler = (g, args) => _parentGroup.NotifyGroupWheel(g, args),
            PointerLeaveHandler = (g, args) =>
                _parentGroup.NotifyGroupPointerLeave(g, args),
            ClickHandler = (g, args) => _parentGroup.NotifyGroupClicked(g, args),
            DoubleClickHandler = (g, args) =>
                _parentGroup.NotifyGroupDoubleClicked(g, args)
        };

        // Subscribe nested notification handlers
        addedGroup.OnGroupAdded += handlers.GroupAddedHandler;
        addedGroup.OnGroupRemoved += handlers.GroupRemovedHandler;
        addedGroup.OnNodeAdded += handlers.NodeAddedHandler;
        addedGroup.OnNodeRemoved += handlers.NodeRemovedHandler;
        addedGroup.OnPortAdded += handlers.PortAddedHandler;
        addedGroup.OnPortRemoved += handlers.PortRemovedHandler;

        // Subscribe direct event handlers
        addedGroup.OnGroupAdded += handlers.DirectGroupAddedHandler;
        addedGroup.OnGroupRemoved += handlers.DirectGroupRemovedHandler;
        addedGroup.OnNodeAdded += handlers.DirectNodeAddedHandler;
        addedGroup.OnNodeRemoved += handlers.DirectNodeRemovedHandler;
        addedGroup.OnPortAdded += handlers.DirectPortAddedHandler;
        addedGroup.OnPortRemoved += handlers.DirectPortRemovedHandler;

        // Existing pointer event subscriptions...
        addedGroup.OnPointerDown += handlers.PointerDownHandler;
        addedGroup.OnPointerEnter += handlers.PointerEnterHandler;
        addedGroup.OnPointerUp += handlers.PointerUpHandler;
        addedGroup.OnPointerMove += handlers.PointerMoveHandler;
        addedGroup.OnWheel += handlers.WheelHandler;
        addedGroup.OnPointerLeave += handlers.PointerLeaveHandler;
        addedGroup.OnClick += handlers.ClickHandler;
        addedGroup.OnDoubleClick += handlers.DoubleClickHandler;

        _groupEventHandlers[addedGroup] = handlers;
    }

    private void StopPropagation(IGroup removedGroup)
    {
        if (_groupEventHandlers.TryGetValue(removedGroup, out var handlers))
        {
            // Unsubscribe nested notification handlers
            removedGroup.OnGroupAdded -= handlers.GroupAddedHandler;
            removedGroup.OnGroupRemoved -= handlers.GroupRemovedHandler;
            removedGroup.OnNodeAdded -= handlers.NodeAddedHandler;
            removedGroup.OnNodeRemoved -= handlers.NodeRemovedHandler;
            removedGroup.OnPortAdded -= handlers.PortAddedHandler;
            removedGroup.OnPortRemoved -= handlers.PortRemovedHandler;

            // Unsubscribe direct event handlers
            removedGroup.OnGroupAdded -= handlers.DirectGroupAddedHandler;
            removedGroup.OnGroupRemoved -= handlers.DirectGroupRemovedHandler;
            removedGroup.OnNodeAdded -= handlers.DirectNodeAddedHandler;
            removedGroup.OnNodeRemoved -= handlers.DirectNodeRemovedHandler;
            removedGroup.OnPortAdded -= handlers.DirectPortAddedHandler;
            removedGroup.OnPortRemoved -= handlers.DirectPortRemovedHandler;

            // Existing pointer event unsubscriptions...
            removedGroup.OnPointerDown -= handlers.PointerDownHandler;
            removedGroup.OnPointerEnter -= handlers.PointerEnterHandler;
            removedGroup.OnPointerUp -= handlers.PointerUpHandler;
            removedGroup.OnPointerMove -= handlers.PointerMoveHandler;
            removedGroup.OnWheel -= handlers.WheelHandler;
            removedGroup.OnPointerLeave -= handlers.PointerLeaveHandler;
            removedGroup.OnClick -= handlers.ClickHandler;
            removedGroup.OnDoubleClick -= handlers.DoubleClickHandler;
            _groupEventHandlers.Remove(removedGroup);
        }
    }

    private void Propagate(INode node)
    {
        var handlers = new NodeEventHandlers
        {
            PortAddedHandler = port =>
                _parentGroup.NotifyNestedPortAdded(_parentGroup, port),
            PortRemovedHandler = port =>
                _parentGroup.NotifyNestedPortRemoved(_parentGroup, port),
            PointerDownHandler = (n, args) =>
                _parentGroup.NotifyNodePointerDown(n, args),
            PointerEnterHandler = (n, args) =>
                _parentGroup.NotifyNodePointerEnter(n, args),
            PointerUpHandler = (n, args) => _parentGroup.NotifyNodePointerUp(n, args),
            PointerMoveHandler = (n, args) =>
                _parentGroup.NotifyNodePointerMove(n, args),
            WheelHandler = (n, args) => _parentGroup.NotifyNodeWheel(n, args),
            PointerLeaveHandler = (n, args) =>
                _parentGroup.NotifyNodePointerLeave(n, args),
            ClickHandler = (n, args) => _parentGroup.NotifyNodeClicked(n, args),
            DoubleClickHandler = (n, args) =>
                _parentGroup.NotifyNodeDoubleClicked(n, args)
        };

        node.OnPortAdded += handlers.PortAddedHandler;
        node.OnPortRemoved += handlers.PortRemovedHandler;
        node.OnPointerDown += handlers.PointerDownHandler;
        node.OnPointerEnter += handlers.PointerEnterHandler;
        node.OnPointerUp += handlers.PointerUpHandler;
        node.OnPointerMove += handlers.PointerMoveHandler;
        node.OnWheel += handlers.WheelHandler;
        node.OnPointerLeave += handlers.PointerLeaveHandler;
        node.OnClick += handlers.ClickHandler;
        node.OnDoubleClick += handlers.DoubleClickHandler;

        _nodeEventHandlers[node] = handlers;
    }

    private void StopPropagation(INode node)
    {
        if (_nodeEventHandlers.TryGetValue(node, out var handlers))
        {
            node.OnPortAdded -= handlers.PortAddedHandler;
            node.OnPortRemoved -= handlers.PortRemovedHandler;
            node.OnPointerDown -= handlers.PointerDownHandler;
            node.OnPointerEnter -= handlers.PointerEnterHandler;
            node.OnPointerUp -= handlers.PointerUpHandler;
            node.OnPointerMove -= handlers.PointerMoveHandler;
            node.OnWheel -= handlers.WheelHandler;
            node.OnPointerLeave -= handlers.PointerLeaveHandler;
            node.OnClick -= handlers.ClickHandler;
            node.OnDoubleClick -= handlers.DoubleClickHandler;

            _nodeEventHandlers.Remove(node);
        }
    }

    private void Propagate(IPort port)
    {
        var handlers = new PortEventHandlers
        {
            ClickHandler = (p, args) => _parentGroup.NotifyPortClicked(p, args),
            DoubleClickHandler = (p, args) =>
                _parentGroup.NotifyPortDoubleClicked(p, args),
            PointerDownHandler = (p, args) =>
                _parentGroup.NotifyPortPointerDown(p, args),
            PointerEnterHandler = (p, args) =>
                _parentGroup.NotifyPortPointerEnter(p, args),
            PointerUpHandler = (p, args) => _parentGroup.NotifyPortPointerUp(p, args),
            PointerMoveHandler = (p, args) =>
                _parentGroup.NotifyPortPointerMove(p, args),
            PointerLeaveHandler = (p, args) =>
                _parentGroup.NotifyPortPointerLeave(p, args),
            WheelHandler = (p, args) => _parentGroup.NotifyPortWheel(p, args)
        };

        port.OnClick += handlers.ClickHandler;
        port.OnDoubleClick += handlers.DoubleClickHandler;
        port.OnPointerDown += handlers.PointerDownHandler;
        port.OnPointerEnter += handlers.PointerEnterHandler;
        port.OnPointerUp += handlers.PointerUpHandler;
        port.OnPointerMove += handlers.PointerMoveHandler;
        port.OnPointerLeave += handlers.PointerLeaveHandler;
        port.OnWheel += handlers.WheelHandler;

        _portEventHandlers[port] = handlers;
    }

    private void StopPropagation(IPort port)
    {
        if (_portEventHandlers.TryGetValue(port, out var handlers))
        {
            port.OnClick -= handlers.ClickHandler;
            port.OnDoubleClick -= handlers.DoubleClickHandler;
            port.OnPointerDown -= handlers.PointerDownHandler;
            port.OnPointerEnter -= handlers.PointerEnterHandler;
            port.OnPointerUp -= handlers.PointerUpHandler;
            port.OnPointerMove -= handlers.PointerMoveHandler;
            port.OnPointerLeave -= handlers.PointerLeaveHandler;
            port.OnWheel -= handlers.WheelHandler;

            _portEventHandlers.Remove(port);
        }
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}