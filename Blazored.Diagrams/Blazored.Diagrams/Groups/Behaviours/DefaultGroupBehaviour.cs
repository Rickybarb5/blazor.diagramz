using Blazored.Diagrams.Interfaces.Behaviours;

namespace Blazored.Diagrams.Groups.Behaviours;

/// <summary>
///     Adding groups has some restrictions:
///     A group cannot be added to itself.
/// </summary>
public class DefaultGroupBehaviour : IBehaviour
{
    private readonly IGroup _group;

    /// <summary>
    /// Instantiates a new <see cref="DefaultGroupBehaviour"/>
    /// </summary>
    /// <param name="group">Group instance.</param>
    public DefaultGroupBehaviour(IGroup group)
    {
        _group = group;
        EnableBehaviour();
    }

    private bool _isEnabled = true;

    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _group.Groups.OnItemAdded -= HandleGroupAdded;
    }

    private void EnableBehaviour()
    {
        _group.Groups.OnItemAdded += HandleGroupAdded;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void HandleGroupAdded(IGroup obj)
    {
        if (obj.Id != _group.Id)
            _group.NotifyGroupAdded(obj);
        else
            throw new InvalidOperationException("Cannot add group to itself.");
    }
}