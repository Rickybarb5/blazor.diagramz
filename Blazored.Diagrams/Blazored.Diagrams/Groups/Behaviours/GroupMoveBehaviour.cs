using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Groups.Behaviours;

/// <summary>
///     Move child models when the group moves.
/// </summary>
public class GroupMoveBehaviour : IBehaviour
{
    private readonly IGroup _group;
    private double _beforePositionX;
    private double _beforePositionY;

    /// <summary>
    /// Instantiates a new <see cref="GroupMoveBehaviour"/>
    /// </summary>
    /// <param name="group">Group instance.</param>
    public GroupMoveBehaviour(IGroup group)
    {
        _group = group;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _group.OnBeforePositionChanged -= SaveCoordinates;
        _group.OnPositionChanged -= MoveChildren;
        _group.OnSizeChanged -= MoveChildren;
    }

    private void EnableBehaviour()
    {
        _group.OnBeforePositionChanged += SaveCoordinates;
        _group.OnPositionChanged += MoveChildren;
        _group.OnSizeChanged += MoveChildren;
    }

    private void MoveChildren(IPosition obj)
    {
        var xDiff = obj.PositionX - _beforePositionX;
        var yDiff = obj.PositionY - _beforePositionY;
        UpdateChildrenPosition(_group.Groups, xDiff, yDiff);
        UpdateChildrenPosition(_group.Nodes, xDiff, yDiff);
        _group.Ports.RefreshPosition();
    }

    private static void UpdateChildrenPosition<TPosition>(IEnumerable<TPosition> elements, double xDiff, double yDiff)
        where TPosition : IPosition
    {
        foreach (var element in elements)
        {
            var newX = (int)(element.PositionX + xDiff);
            var newY = (int)(element.PositionY + yDiff);
            element.SetPosition(newX, newY);
        }
    }

    private void SaveCoordinates(IPosition position)
    {
        _beforePositionX = position.PositionX;
        _beforePositionY = position.PositionY;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}