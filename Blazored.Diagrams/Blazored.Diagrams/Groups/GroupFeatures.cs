using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Groups;

public partial class Group<TGroupComponent>
{
    /// <inheritdoc />
    public virtual void SetPosition(int x, int y)
    {
        var stateChanged = x != _positionX || y != _positionY;

        if (stateChanged)
        {
            NotifyBeforePositionChanged();
            _positionX = x;
            _positionY = y;
            NotifyPositionChanged();
        }
    }

    /// <inheritdoc />
    public virtual void SetSize(int width, int height)
    {
        var stateChanged = width != _width || _height != height;
        if (stateChanged)
        {
            _width = width;
            _height = height;
            NotifySizeChanged();
        }
    }

    /// <inheritdoc />
    public virtual void UnselectAll()
    {
        foreach (var group in Groups)
        {
            group.IsSelected = false;
            group.UnselectAll();
        }

        foreach (var node in Nodes) node.IsSelected = false;
    }


    /// <inheritdoc />
    public virtual void SelectAll()
    {
        foreach (var group in Groups)
        {
            group.IsSelected = true;
            group.SelectAll();
        }

        foreach (var node in Nodes) node.IsSelected = true;
    }

    /// <inheritdoc />
    void IGroup.SetPositionInternal(int x, int y)
    {
        _positionX = x;
        _positionY = y;
    }

    /// <inheritdoc />
    void IGroup.SetSizeInternal(int width, int height)
    {
        _width = width;
        _height = height;
    }

    /// <inheritdoc />
    public virtual bool RemoveGroup(IGroup groupToRemove)
    {
        var removed = Groups.Remove(groupToRemove);
        if (removed) return true;
        return Groups.Select(nestedGroup => nestedGroup.RemoveGroup(groupToRemove)).Any(nestedRemove => nestedRemove);
    }

    /// <inheritdoc />
    public virtual bool RemoveNode(INode nodeToRemove)
    {
        var removed = Nodes.Remove(nodeToRemove);
        if (removed) return true;
        return Groups.Select(nestedGroup => nestedGroup.RemoveNode(nodeToRemove)).Any(nestedRemove => nestedRemove);
    }
}