﻿using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Groups;

/// <summary>
///     Collection of events for classes that implement the <see cref="IGroup" /> interface.
/// </summary>
public interface IGroupEvents :
    IVisibilityEvents<IGroup>,
    IPositionEvents<IGroup>,
    ISelectionEvents<IGroup>,
    ISizeEvents<IGroup>,
    IPortContainerEvents,
    INodeContainerEvents,
    IGroupContainerEvents,
    IPointerEvents<IGroup>,
    IPaddingEvents,
    IUiEvents
{
    /// <summary>
    ///     Notifies that a group was added.
    /// </summary>
    /// <param name="parentGroup">Parent group to which the group was added.</param>
    /// <param name="addedGroup">The group that was added.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupAdded(IGroup parentGroup, IGroup addedGroup, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that a group was removed.
    /// </summary>
    /// <param name="parentGroup">Parent group from which the group was removed.</param>
    /// <param name="removedGroup">The group that was removed.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupRemoved(IGroup parentGroup, IGroup removedGroup,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when a group is added to a child group.
    /// </summary>
    /// <returns>The parent group and the group that was added to it.</returns>
    public event Action<IGroup, IGroup> OnNestedGroupAdded;

    /// <summary>
    ///     Triggers the <see cref="OnNestedGroupAdded" /> event.
    /// </summary>
    /// <param name="parentGroup">The group to which the group was added.</param>
    /// <param name="addedGroup">The group that was added.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedGroupAdded(IGroup parentGroup, IGroup addedGroup,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when a group is removed from a child group.
    /// </summary>
    /// <returns>The group that was removed.</returns>
    public event Action<IGroup, IGroup> OnNestedGroupRemoved;

    /// <summary>
    ///     Triggers the <see cref="OnNestedGroupRemoved" /> event.
    /// </summary>
    /// <param name="parentGroup">The group from which the node was removed.</param>
    /// <param name="removedGroup">The group that was removed.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedGroupRemoved(IGroup parentGroup, IGroup removedGroup,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when a node is added to a child group.
    /// </summary>
    /// <returns>The node that was added.</returns>
    public event Action<IGroup, INode> OnNestedNodeAdded;

    /// <summary>
    ///     Event triggered when a node is removed from a child group.
    /// </summary>
    /// <returns>The node that was removed.</returns>
    public event Action<IGroup, INode> OnNestedNodeRemoved;

    /// <summary>
    ///     Triggers the <see cref="OnNestedNodeAdded" />  event.
    /// </summary>
    /// <param name="group">The group to which the node was added.</param>
    /// <param name="addedNode">The node that was added.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedNodeAdded(IGroup group, INode addedNode, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnNestedNodeRemoved" /> event.
    /// </summary>
    /// <param name="group">The group from which the node was removed.</param>
    /// <param name="removedNode">The node that was removed.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedNodeRemoved(IGroup group, INode removedNode, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggered when a port is added to a nested child container.
    /// </summary>
    public event Action<IGroup, IPort> OnNestedPortAdded;

    /// <summary>
    ///     Triggered when a port is removed from a nested child container.
    /// </summary>
    public event Action<IGroup, IPort> OnNestedPortRemoved;

    /// <summary>
    ///     Triggers the <see cref="OnNestedPortAdded" /> event.
    /// </summary>
    /// <param name="parent">Parent group of the port.</param>
    /// <param name="port">Port that was added.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedPortAdded(IGroup parent, IPort port, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnNestedPortRemoved" /> event.
    /// </summary>
    /// <param name="parent">Parent group of the port.</param>
    /// <param name="port">Port that was removed.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNestedPortRemoved(IGroup parent, IPort port, [CallerFilePath] string? callerMemberName = null);
}