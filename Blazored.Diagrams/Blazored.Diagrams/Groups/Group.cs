﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Groups.Behaviours;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Groups;

/// <summary>
///     Base implementation of a group.
/// </summary>
/// <typeparam name="TGroupComponent">The UI component.</typeparam>
public partial class Group<TGroupComponent> : IGroup where TGroupComponent : IComponent
{
    private readonly ObservableList<IGroup> _groups = [];
    private readonly ObservableList<INode> _nodes = [];
    private readonly ObservableList<IPort> _ports = [];
    private int _height;
    private bool _isSelected;
    private bool _isVisible = true;
    private int _padding = 20;
    private int _positionX;
    private int _positionY;
    private int _width;


    /// <summary>
    ///     Initializes a new <see cref="Group{TGroupComponent}" />
    /// </summary>
    public Group()
    {
        Parameters = new Dictionary<string, object> { { nameof(GroupContainer.Group), this } };
        Behaviours = new GroupBehaviourContainer(this);
    }


    /// <inheritdoc />
    public virtual void Dispose()
    {
        _ports.ForEach(x => x.Dispose());
        _ports.Clear();
        _nodes.ForEach(x => x.Dispose());
        _nodes.Clear();
        _groups.ForEach(x => x.Dispose());
        _groups.Clear();
        Behaviours.Dispose();
    }

    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    public virtual int Width
    {
        get => _width;
        set
        {
            if (_width != value)
            {
                _width = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int Height
    {
        get => _height;
        set
        {
            if (_height != value)
            {
                _height = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionX
    {
        get => _positionX;
        set
        {
            if (_positionX != value)
            {
                _positionX = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionY
    {
        get => _positionY;
        set
        {
            if (_positionY != value)
            {
                _positionY = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsSelected
    {
        get => _isSelected;
        set
        {
            if (_isSelected != value)
            {
                _isSelected = value;
                NotifySelectionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsVisible
    {
        get => _isVisible;
        set
        {
            if (_isVisible != value)
            {
                _isVisible = value;
                NotifyVisibilityChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int Padding
    {
        get => _padding;
        set
        {
            if (_padding != value)
            {
                _padding = value;
                NotifyPaddingChanged();
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Type ComponentType { get; init; } = typeof(TGroupComponent);

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Dictionary<string, object> Parameters { get; init; }

    /// <inheritdoc />
    public virtual ObservableList<IPort> Ports
    {
        get => _ports;
        init
        {
            _ports.Clear();
            foreach (var val in value) _ports.Add(val);
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<INode> Nodes
    {
        get => _nodes;
        init
        {
            _nodes.Clear();
            foreach (var val in value) _nodes.Add(val);
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<IGroup> Groups
    {
        get => _groups;
        init
        {
            _groups.Clear();
            foreach (var val in value) _groups.Add(val);
        }
    }


    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<INode> AllNodes =>
        Nodes
            .Concat(Groups.SelectMany(g => g.AllNodes))
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<IGroup> AllGroups =>
        Groups
            .Concat(Groups.SelectMany(g => g.AllGroups))
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IReadOnlyList<IPort> AllPorts =>
        Ports
            .Concat(AllGroups.SelectMany(g => g.AllPorts))
            .Concat(AllNodes.SelectMany(n => n.Ports))
            .ToList()
            .AsReadOnly();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }
}