﻿using System.Diagnostics.CodeAnalysis;
using Blazored.Diagrams.Services.Observers;
using Blazored.Diagrams.Services.Options;
using Blazored.Diagrams.Services.Serialization;
using Blazored.Diagrams.Services.Virtualization;
using Microsoft.Extensions.DependencyInjection;

namespace Blazored.Diagrams;

/// <summary>
/// Extension class for Blazored diagrams.
/// </summary>
public static class BlazoredDiagramsExtensions
{
    /// <summary>
    /// Sets up all dependencies required for Blazored.Diagrams.
    /// </summary>
    /// <param name="services"><see cref="IServiceCollection"/>.</param>
    /// <param name="configureOptions">Options builder for the diagram options.</param>
    /// <returns></returns>
    [ExcludeFromCodeCoverage]
    public static IServiceCollection AddBlazoredDiagrams(this IServiceCollection services,
        Action<BlazoredDiagramsOptions>? configureOptions = null)
    {
        if (configureOptions is not null)
        {
            services.Configure(configureOptions);
        }

        services.AddSingleton<IResizeObserver, ResizeObserver>();
        services.AddSingleton<IDiagramSerializer, DiagramSerializer>();
        services.AddSingleton<IVirtualizationService, VirtualizationService>();
        return services;
    }
}