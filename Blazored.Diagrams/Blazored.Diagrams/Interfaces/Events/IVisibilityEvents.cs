﻿using Blazored.Diagrams.Interfaces.Properties;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Describes events related with visibility.
/// </summary>
/// <typeparam name="TVisible">Model that implements <see cref="IVisible" /> interface</typeparam>
public interface IVisibilityEvents<TVisible> where TVisible : IVisible
{
    /// <summary>
    ///     Event triggered when the <see cref="IVisible.IsVisible" /> flag is changed.
    /// </summary>
    event Action<TVisible, bool> OnVisibilityChanged;

    /// <summary>
    ///     Notifies that the visibility has changed.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyVisibilityChanged([CallerFilePath] string? callerMemberName = null);
}