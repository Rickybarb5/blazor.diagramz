﻿using Blazored.Diagrams.Links;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Event handler for classes that contain links.
/// </summary>
public interface ILinkContainerEvents
{
    /// <summary>
    ///     Event triggered when a link is added.
    /// </summary>
    event Action<ILink> OnLinkAdded;

    /// <summary>
    ///     Event triggered when a link is removed.
    /// </summary>
    event Action<ILink> OnLinkRemoved;

    /// <summary>
    ///     Notifies that a link was added.
    /// </summary>
    /// <param name="link">The link that was added</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that a link was removed.
    /// </summary>
    /// <param name="link">The link that was removed</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the pointer is down on a link container.
    /// </summary>
    public event Action<ILink, PointerEventArgs>? OnLinkPointerDown;

    /// <summary>
    ///     Event triggered when the pointer is up on a link container.
    /// </summary>
    public event Action<ILink, PointerEventArgs>? OnLinkPointerUp;

    /// <summary>
    ///     Event triggered when the pointer moves within a link container.
    /// </summary>
    public event Action<ILink, PointerEventArgs>? OnLinkPointerMove;

    /// <summary>
    ///     Event triggered when the pointer enters a link container.
    /// </summary>
    public event Action<ILink, PointerEventArgs>? OnLinkPointerEnter;

    /// <summary>
    ///     Event triggered when the pointer exits a link container.
    /// </summary>
    public event Action<ILink, PointerEventArgs>? OnLinkPointerLeave;

    /// <summary>
    ///     Event triggered when the wheel is scrolled on a link container.
    /// </summary>
    public event Action<ILink, WheelEventArgs>? OnLinkWheel;

    /// <summary>
    ///     Event triggered when a link is clicked.
    /// </summary>
    public event Action<ILink, MouseEventArgs>? OnLinkClicked;

    /// <summary>
    ///     Event triggered when a link is double-clicked.
    /// </summary>
    public event Action<ILink, MouseEventArgs>? OnLinkDoubleClicked;

    /// <summary>
    ///     Triggers the <see cref="OnLinkPointerDown" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkPointerDown(ILink link, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkPointerUp" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkPointerUp(ILink link, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkPointerMove" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkPointerMove(ILink link, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkPointerEnter" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkPointerEnter(ILink link, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkPointerLeave" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkPointerLeave(ILink link, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkWheel" /> event.
    /// </summary>
    /// <param name="link">The affected link</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkWheel(ILink link, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkClicked" /> event.
    /// </summary>
    /// <param name="link">The link that has been clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkClicked(ILink link, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnLinkDoubleClicked" /> event.
    /// </summary>
    /// <param name="link">The link that has been double-clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLinkDoubleClicked(ILink link, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when a incoming link is attached to the port.
    /// </summary>
    public event Action<ILink> OnIncomingLinkAdded;

    /// <summary>
    ///     Event triggered when a incoming link is detached to the port.
    /// </summary>
    public event Action<ILink> OnIncomingLinkRemoved;

    /// <summary>
    ///     Event triggered when a outgoing link is attached to the port.
    /// </summary>
    public event Action<ILink> OnOutgoingLinkAdded;

    /// <summary>
    ///     Event triggered when a outgoing link is detached to the port.
    /// </summary>
    public event Action<ILink> OnOutgoingLinkRemoved;

    /// <summary>
    ///     Triggers the <see cref="OnIncomingLinkAdded" /> event.
    /// </summary>
    /// <param name="link">The attached link.</param>
    /// <param name="port">The link's target port.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyIncomingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnIncomingLinkRemoved" /> event.
    /// </summary>
    /// <param name="link">The new position</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyIncomingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnOutgoingLinkAdded" /> event.
    /// </summary>
    /// <param name="link">The attached link.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyOutgoingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnOutgoingLinkRemoved" /> event.
    /// </summary>
    /// <param name="link">The new position</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyOutgoingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null);
}