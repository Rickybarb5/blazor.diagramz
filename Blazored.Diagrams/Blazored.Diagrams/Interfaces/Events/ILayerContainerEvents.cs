using Blazored.Diagrams.Layers;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
/// Set of event feature for layer containers.
/// </summary>
public interface ILayerContainerEvents
{
    /// <summary>
    ///     Event triggered when a layer is added.
    /// </summary>
    event Action<ILayer> OnLayerAdded;

    /// <summary>
    ///     Event triggered when a layer is removed.
    /// </summary>
    event Action<ILayer> OnLayerRemoved;

    /// <summary>
    ///     Notifies that a layer was added.
    /// </summary>
    /// <param name="layer">The layer that was added</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLayerAdded(ILayer layer, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that a layer was removed.
    /// </summary>
    /// <param name="layer">The layer that was removed</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyLayerRemoved(ILayer layer, [CallerFilePath] string? callerMemberName = null);
}