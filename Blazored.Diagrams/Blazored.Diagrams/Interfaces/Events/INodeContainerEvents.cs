﻿using Blazored.Diagrams.Nodes;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Handles events for classes that contain nodes.
/// </summary>
public interface INodeContainerEvents
{
    /// <summary>
    ///     Event triggered when a node is added.
    /// </summary>
    event Action<INode> OnNodeAdded;

    /// <summary>
    ///     Event triggered when a node is removed.
    /// </summary>
    event Action<INode> OnNodeRemoved;

    /// <summary>
    ///     Notifies that a node was added.
    /// </summary>
    /// <param name="node">The node that was added</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodeAdded(INode node, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that a node was removed.
    /// </summary>
    /// <param name="node">The node that was removed</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodeRemoved(INode node, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the pointer is down on a nested node.
    /// </summary>
    public event Action<INode, PointerEventArgs>? OnNodePointerDown;

    /// <summary>
    ///     Event triggered when the pointer is up on a nested node.
    /// </summary>
    public event Action<INode, PointerEventArgs>? OnNodePointerUp;

    /// <summary>
    ///     Event triggered when the pointer moves within a node.
    /// </summary>
    public event Action<INode, PointerEventArgs>? OnNodePointerMove;

    /// <summary>
    ///     Event triggered when the pointer enters a node.
    /// </summary>
    public event Action<INode, PointerEventArgs>? OnNodePointerEnter;

    /// <summary>
    ///     Event triggered when the pointer exits a node.
    /// </summary>
    public event Action<INode, PointerEventArgs>? OnNodePointerLeave;

    /// <summary>
    ///     Event triggered when the wheel is scrolled on a node.
    /// </summary>
    public event Action<INode, WheelEventArgs>? OnNodeWheel;

    /// <summary>
    ///     Event triggered when a node is clicked.
    /// </summary>
    public event Action<INode, MouseEventArgs>? OnNodeClicked;

    /// <summary>
    ///     Event triggered when a node is double-clicked.
    /// </summary>
    public event Action<INode, MouseEventArgs>? OnNodeDoubleClicked;

    /// <summary>
    ///     Notifies that the pointer is down on a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodePointerDown(INode node, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer is up on a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodePointerUp(INode node, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer is moving on a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodePointerMove(INode node, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer has entered a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodePointerEnter(INode node, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer has exited a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodePointerLeave(INode node, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the mouse wheel has scrolled within a node.
    /// </summary>
    /// <param name="node">The affected node</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodeWheel(INode node, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers  the <see cref="OnNodeClicked" /> event./>
    /// </summary>
    /// <param name="node">The node that has been clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodeClicked(INode node, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnNodeDoubleClicked" /> event./>
    /// </summary>
    /// <param name="node">The node that has been double-clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyNodeDoubleClicked(INode node, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);
}