﻿using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Events that can be triggered by ports.
/// </summary>
public interface IPortContainerEvents
{
    /// <summary>
    ///     Event triggered when a port is added.
    /// </summary>
    public event Action<IPort> OnPortAdded;

    /// <summary>
    ///     Event triggered when a port is removed.
    /// </summary>
    public event Action<IPort> OnPortRemoved;

    /// <summary>
    ///     Event triggered when a port is clicked.
    /// </summary>
    public event Action<IPort, MouseEventArgs>? OnPortClicked;

    /// <summary>
    ///     Event triggered when a port is double-clicked.
    /// </summary>
    public event Action<IPort, MouseEventArgs>? OnPortDoubleClicked;

    /// <summary>
    ///     Notifies that a port was added.
    /// </summary>
    /// <param name="port">The added port</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortAdded(IPort port, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that a port was removed.
    /// </summary>
    /// <param name="port">The removed port</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortRemoved(IPort port, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers  the <see cref="OnPortClicked" /> event./>
    /// </summary>
    /// <param name="port">The port that has been clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPortDoubleClicked" /> event./>
    /// </summary>
    /// <param name="port">The port that has been double-clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortDoubleClicked(IPort port, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the pointer is down on a port.
    /// </summary>
    public event Action<IPort, PointerEventArgs>? OnPortPointerDown;

    /// <summary>
    ///     Event triggered when the pointer is up on a port.
    /// </summary>
    public event Action<IPort, PointerEventArgs>? OnPortPointerUp;

    /// <summary>
    ///     Event triggered when the pointer moves within a port.
    /// </summary>
    public event Action<IPort, PointerEventArgs>? OnPortPointerMove;

    /// <summary>
    ///     Event triggered when the pointer enters a port.
    /// </summary>
    public event Action<IPort, PointerEventArgs>? OnPortPointerEnter;

    /// <summary>
    ///     Event triggered when the pointer exits a port.
    /// </summary>
    public event Action<IPort, PointerEventArgs>? OnPortPointerLeave;

    /// <summary>
    ///     Event triggered when the wheel is scrolled on a port.
    /// </summary>
    public event Action<IPort, WheelEventArgs>? OnPortWheel;

    /// <summary>
    ///     Notifies that the pointer is down on a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPointerDown(IPort port, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer is up on a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPointerUp(IPort port, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer is moving on a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPointerMove(IPort port, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer has entered a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPointerEnter(IPort port, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the pointer has exited a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPointerLeave(IPort port, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies that the mouse wheel has scrolled within a port.
    /// </summary>
    /// <param name="port">The affected port</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortWheel(IPort port, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null);
}