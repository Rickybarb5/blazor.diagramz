﻿using Blazored.Diagrams.Interfaces.Properties;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Describes events related with selection.
/// </summary>
/// <typeparam name="TSelectable">Model that implements the <see cref="ISelectable" /> interface.</typeparam>
public interface ISelectionEvents<TSelectable> where TSelectable : ISelectable
{
    /// <summary>
    ///     Event triggered when the <see cref="ISelectable.IsSelected" /> flag is changed.
    /// </summary>
    event Action<TSelectable, bool> OnSelectionChanged;

    /// <summary>
    ///     Notifies that the selection has changed.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifySelectionChanged([CallerFilePath] string? callerMemberName = null);
}