﻿using Blazored.Diagrams.Groups;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Event handler for classes that contain groups.
/// </summary>
public interface IGroupContainerEvents
{
    /// <summary>
    ///     Event triggered when a nested group is added.
    /// </summary>
    event Action<IGroup> OnGroupAdded;

    /// <summary>
    ///     Event triggered when a nested group is removed.
    /// </summary>
    event Action<IGroup> OnGroupRemoved;

    /// <summary>
    ///     Triggers the <see cref="OnGroupAdded" /> event.
    /// </summary>
    /// <param name="addedGroup">The group that was added.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupAdded(IGroup addedGroup, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupRemoved" /> event.
    /// </summary>
    /// <param name="removedGroup">The group that was removed.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupRemoved(IGroup removedGroup, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the pointer is down on a nested group.
    /// </summary>
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerDown;

    /// <summary>
    ///     Event triggered when the pointer is up on a nested group.
    /// </summary>
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerUp;

    /// <summary>
    ///     Event triggered when the pointer moves within a nested group.
    /// </summary>
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerMove;

    /// <summary>
    ///     Event triggered when the pointer enters a nested group.
    /// </summary>
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerEnter;

    /// <summary>
    ///     Event triggered when the pointer exits a nested group.
    /// </summary>
    public event Action<IGroup, PointerEventArgs>? OnGroupPointerLeave;

    /// <summary>
    ///     Event triggered when the wheel is scrolled on a nested group.
    /// </summary>
    public event Action<IGroup, WheelEventArgs>? OnGroupWheel;

    /// <summary>
    ///     Event triggered when a nested group is clicked.
    /// </summary>
    public event Action<IGroup, MouseEventArgs>? OnGroupClicked;

    /// <summary>
    ///     Event triggered when a nested group is double-clicked.
    /// </summary>
    public event Action<IGroup, MouseEventArgs>? OnGroupDoubleClicked;

    /// <summary>
    ///     Triggers the <see cref="OnGroupPointerDown" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupPointerDown(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupPointerUp" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupPointerUp(IGroup group, PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupPointerMove" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupPointerMove(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupPointerEnter" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupPointerEnter(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupPointerLeave" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupPointerLeave(IGroup group, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupWheel" /> event.
    /// </summary>
    /// <param name="group">The affected group</param>
    /// <param name="args">The Pointer event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupWheel(IGroup group, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupClicked" /> event.
    /// </summary>
    /// <param name="group">The group that has been clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupClicked(IGroup group, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnGroupDoubleClicked" /> event.
    /// </summary>
    /// <param name="group">The group that has been double-clicked.</param>
    /// <param name="args">The mouse event args.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyGroupDoubleClicked(IGroup group, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null);
}