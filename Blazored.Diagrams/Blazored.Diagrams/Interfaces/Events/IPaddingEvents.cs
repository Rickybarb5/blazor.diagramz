using Blazored.Diagrams.Interfaces.Properties;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
/// Events related to the <see cref="IPadding"/>
/// </summary>
public interface IPaddingEvents
{
    /// <summary>
    /// Event triggered when the padding changes.
    /// </summary>
    event Action<int> OnPaddingChanged;

    /// <summary>
    /// Triggers the <see cref="OnPaddingChanged"/>
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPaddingChanged([CallerFilePath] string? callerMemberName = null);
}