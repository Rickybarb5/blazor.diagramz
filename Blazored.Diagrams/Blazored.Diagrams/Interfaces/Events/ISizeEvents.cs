﻿using Blazored.Diagrams.Interfaces.Properties;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Describes the events triggered when the size changes.
/// </summary>
/// <typeparam name="TSize">Model that implements ISize interface</typeparam>
public interface ISizeEvents<TSize> where TSize : ISize
{
    /// <summary>
    ///     Event triggered when the size changes.
    /// </summary>
    public event Action<TSize> OnSizeChanged;

    /// <summary>
    ///     Notifies that the size has changed.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifySizeChanged([CallerFilePath] string? callerMemberName = null);
}