using Blazored.Diagrams.Interfaces.Properties;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Describes events related with pointer events.
/// </summary>
/// <typeparam name="TModelType">Model that implements <see cref="IModel" /> interface</typeparam>
public interface IPointerEvents<TModelType> where TModelType : IModel
{
    /// <summary>
    ///     Event triggered when the pointer is down on the container.
    /// </summary>
    event Action<TModelType, PointerEventArgs>? OnPointerDown;

    /// <summary>
    ///     Event triggered when the pointer is up on the container.
    /// </summary>
    event Action<TModelType, PointerEventArgs>? OnPointerUp;

    /// <summary>
    ///     Event triggered when the pointer moves within the container.
    /// </summary>
    event Action<TModelType, PointerEventArgs>? OnPointerMove;

    /// <summary>
    ///     Event triggered when the pointer enters the container.
    /// </summary>
    event Action<TModelType, PointerEventArgs>? OnPointerEnter;

    /// <summary>
    ///     Event triggered when the pointer leaves the container.
    /// </summary>
    event Action<TModelType, PointerEventArgs>? OnPointerLeave;

    /// <summary>
    ///     Event triggered when the wheel is scrolled in the container.
    /// </summary>
    event Action<TModelType, WheelEventArgs>? OnWheel;

    /// <summary>
    ///     Event triggered when the container is clicked
    /// </summary>
    event Action<TModelType, MouseEventArgs>? OnClick;

    /// <summary>
    ///     Event triggered when the container is double-clicked.
    /// </summary>
    event Action<TModelType, MouseEventArgs>? OnDoubleClick;

    /// <summary>
    ///     Triggers the <see cref="OnPointerDown" /> event.
    /// </summary>
    /// <param name="args">Information about the event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPointerUp" /> event.
    /// </summary>
    /// <param name="args">Information about the event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPointerMove" /> event.
    /// </summary>
    /// <param name="args">Information about the event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPointerEnter" /> event.
    /// </summary>
    /// <param name="args">Information about the event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPointerLeave" /> event.
    /// </summary>
    /// <param name="args">Information about the event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnWheel" /> event.
    /// </summary>
    /// <param name="args">Information about the scroll.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnClick" /> event.
    /// </summary>
    /// <param name="args">Information about the click event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnDoubleClick" /> event.
    /// </summary>
    /// <param name="args">Information about the double click event.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null);
}