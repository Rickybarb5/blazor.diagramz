using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
/// Functionality for keyboard events.
/// </summary>
public interface IKeyboardEvents
{
    /// <summary>
    /// Event triggered when a keyboard key is down.
    /// </summary>
    event Action<KeyboardEventArgs> OnKeyDown;

    /// <summary>
    /// Event triggered when a key is up.
    /// </summary>
    event Action<KeyboardEventArgs> OnKeyUp;

    /// <summary>
    /// Triggers the <see cref="OnKeyDown"/> event.
    /// </summary>
    /// <param name="args">Keyboard event arguments.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyKeyDown(KeyboardEventArgs args, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    /// Triggers the <see cref="OnKeyUp"/> event.
    /// </summary>
    /// <param name="args">Keyboard event arguments.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyKeyUp(KeyboardEventArgs args, [CallerFilePath] string? callerMemberName = null);
}