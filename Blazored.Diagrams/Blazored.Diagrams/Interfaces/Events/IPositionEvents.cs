﻿using Blazored.Diagrams.Interfaces.Properties;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Event handler for classes that implement <see cref="IPosition" />.
/// </summary>
/// <typeparam name="TPosition"></typeparam>
public interface IPositionEvents<TPosition> where TPosition : IPosition
{
    /// <summary>
    ///     Event triggered when the position is changed.
    /// </summary>
    event Action<TPosition> OnPositionChanged;

    /// <summary>
    ///     Event triggered before the position changes.
    /// </summary>
    event Action<TPosition> OnBeforePositionChanged;

    /// <summary>
    ///     Notifies that the position was changed.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPositionChanged([CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Notifies the current position before it changes.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyBeforePositionChanged([CallerFilePath] string? callerMemberName = null);
}