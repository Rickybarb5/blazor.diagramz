using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Interfaces.Events;

/// <summary>
///     Events related to the UI.
/// </summary>
public interface IUiEvents
{
    /// <summary>
    ///     Event triggered when a redraw is requested for a model.
    /// </summary>
    public event Action OnRedraw;

    /// <summary>
    ///     Triggers the <see cref="OnRedraw" /> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null);
}