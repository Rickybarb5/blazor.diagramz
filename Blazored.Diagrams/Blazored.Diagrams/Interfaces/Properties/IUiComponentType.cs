﻿using System.Text.Json.Serialization;

namespace Blazored.Diagrams.Interfaces.Properties;

/// <summary>
///     Properties that relates a model to a UI component.
/// </summary>
public interface IUiComponentType
{
    /// <summary>
    ///     Gets the UI component type.
    /// </summary>
    [JsonIgnore]
    Type ComponentType { get; }

    /// <summary>
    ///     Parameters of the component.
    /// </summary>
    [JsonIgnore]
    Dictionary<string, object> Parameters { get; }
}