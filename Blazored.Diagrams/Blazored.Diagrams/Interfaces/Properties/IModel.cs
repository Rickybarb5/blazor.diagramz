namespace Blazored.Diagrams.Interfaces.Properties;

/// <summary>
///     Interface that describes a diagram model.
/// </summary>
public interface IModel
{
}