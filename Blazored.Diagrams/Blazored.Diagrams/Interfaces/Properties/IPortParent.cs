﻿namespace Blazored.Diagrams.Interfaces.Properties;

/// <summary>
///     Describes the parent of a port.
/// </summary>
public interface IPortParent : IPosition, ISize
{
}