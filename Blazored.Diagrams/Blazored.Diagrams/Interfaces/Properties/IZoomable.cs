﻿namespace Blazored.Diagrams.Interfaces.Properties;

/// <summary>
///     Interface to describe models that are zoomable.
/// </summary>
public interface IZoomable
{
    /// <summary>
    ///     Current zoom value
    /// </summary>
    double Zoom { get; }

    /// <summary>
    ///     Sets the zoom to the specified value.
    /// </summary>
    /// <param name="zoom"></param>
    public void SetZoom(double zoom);

    /// <summary>
    ///     Increases the zoom by a fixed amount.
    /// </summary>
    public void StepZoomUp();

    /// <summary>
    ///     Decreases the zoom by a fixed amount.
    /// </summary>
    public void StepZoomDown();
}