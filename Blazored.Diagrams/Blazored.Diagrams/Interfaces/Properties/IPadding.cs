namespace Blazored.Diagrams.Interfaces.Properties;
/// <summary>
/// Padding properties.
/// </summary>
public interface IPadding
{
    /// <summary>
    /// Padding of the model.
    /// </summary>
    int Padding { get; set; }
}