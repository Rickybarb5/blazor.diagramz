﻿using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Helpers;

namespace Blazored.Diagrams.Interfaces.Containers;

/// <summary>
///     Interface that describes a model that contains groups.
/// </summary>
public interface IGroupContainer : IContainer
{
    /// <summary>
    ///     List of groups.
    /// </summary>
    ObservableList<IGroup> Groups { get; init; }
}