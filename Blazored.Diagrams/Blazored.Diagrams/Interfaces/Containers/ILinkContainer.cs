﻿using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Links;

namespace Blazored.Diagrams.Interfaces.Containers;

/// <summary>
///     Describes a model that has link connected to it.
/// </summary>
public interface ILinkContainer : IContainer
{
    /// <summary>
    ///     List of links that originate from this port.
    /// </summary>
    ObservableList<ILink> OutgoingLinks { get; init; }
    
    /// <summary>
    ///     List of links that originate from another port.
    /// </summary>
    ObservableList<ILink> IncomingLinks { get; init; }
}