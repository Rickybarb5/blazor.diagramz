﻿using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Interfaces.Containers;

/// <summary>
///     Describes a model that contains ports.
/// </summary>
public interface IPortContainer : IContainer, IPosition, ISize, IId
{
    /// <summary>
    ///     Ports associated with this model.
    /// </summary>
    ObservableList<IPort> Ports { get; init; }
}