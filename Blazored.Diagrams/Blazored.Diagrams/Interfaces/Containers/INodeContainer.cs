﻿using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Interfaces.Containers;

/// <summary>
///     Describes a model that contains nodes.
/// </summary>
public interface INodeContainer: IContainer
{
    /// <summary>
    ///     Nodes that belong to the model.
    /// </summary>
    ObservableList<INode> Nodes { get; init;}
}