﻿namespace Blazored.Diagrams.Interfaces.Behaviours;

/// <summary>
///     Base interface for a behaviour.
/// </summary>
public interface IBehaviour : IDisposable
{
    /// <summary>
    ///  Enables or disables the behaviour.
    /// </summary>
    public bool IsEnabled { get; set; }
}