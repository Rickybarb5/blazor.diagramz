﻿using System.Text.Json.Serialization;

namespace Blazored.Diagrams.Interfaces.Behaviours;

/// <summary>
///     Feature description for managing behaviours.
/// </summary>
public interface IBehaviourContainer : IDisposable
{
    /// <summary>
    ///     Behaviours executed by the container.
    /// </summary>
    [JsonIgnore]
    public IReadOnlyList<IBehaviour> All { get; }

    /// <summary>
    ///     Adds a behaviour to the model.
    ///     Only one of each type per model is allowed.
    /// </summary>
    /// <param name="behaviour"></param>
    public void Add<TBehaviour>(TBehaviour behaviour) where TBehaviour : IBehaviour;

    /// <summary>
    ///     Removes a behavior from the model.
    /// </summary>
    public void Remove<TBehaviour>() where TBehaviour : IBehaviour;

    /// <summary>
    /// Checks if a behaviour exists.
    /// </summary>
    /// <typeparam name="TBehaviour"></typeparam>
    /// <returns></returns>
    public bool Exists<TBehaviour>() where TBehaviour : IBehaviour;

    /// <summary>
    /// Gets a behaviour from the container, if it exists.
    /// </summary>
    /// <typeparam name="TBehaviour"></typeparam>
    /// <returns></returns>
    public TBehaviour? Get<TBehaviour>() where TBehaviour : class, IBehaviour;

    /// <summary>
    /// Tries to get a behaviour by its type.
    /// </summary>
    /// <param name="behaviour">Behaviour instance.</param>
    /// <typeparam name="TBehaviour">Behaviour type/</typeparam>
    /// <returns></returns>
    public bool TryGet<TBehaviour>(out TBehaviour behaviour) where TBehaviour : class, IBehaviour;
}