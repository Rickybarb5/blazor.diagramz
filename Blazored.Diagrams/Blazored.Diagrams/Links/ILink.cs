﻿using Blazored.Diagrams.Interfaces.Properties;
using Blazored.Diagrams.Ports;
using System.Text.Json.Serialization;
using Blazored.Diagrams.Interfaces.Behaviours;

namespace Blazored.Diagrams.Links;

/// <summary>
/// A link allows two ports to be connected.
/// </summary>
public interface ILink : IId,
    ISelectable,
    IVisible,
    IUiComponentType,
    IModel,
    ILinkEvents,
    ISize,
    IDisposable
{
    /// <summary>
    ///     Port where the link originates from.
    /// </summary>
    [JsonIgnore]
    public IPort SourcePort { get; set; }

    /// <summary>
    ///     End port that the link connects to, if it exists.
    /// </summary>
    [JsonIgnore]
    public IPort? TargetPort { get; set; }

    /// <summary>
    /// Behaviour management.
    /// </summary>
    IBehaviourContainer Behaviours { get; }

    /// <summary>
    ///     Gets a value indicating if the port is connected to a target port.
    /// </summary>
    public bool IsConnected { get; }

    /// <summary>
    ///     X coordinate of the target position of the end of the link.
    ///     If connected to a target port, it is the center of that element's position.
    /// </summary>
    public int TargetPositionX { get; set; }

    /// <summary>
    ///     Y coordinate of the target position of the end of the link.
    ///     If connected to a target port, it is the center of that element's position.
    /// </summary>
    public int TargetPositionY { get; set; }

    /// <summary>
    ///     Updates the end position of the link.
    /// </summary>
    /// <param name="x">X coordinate of the new posistion.</param>
    /// <param name="y">Y coordinate of the new position.</param>
    void SetTargetPosition(int x, int y);

    /// <summary>
    /// Sets the x and y position of the target position to the center of the target port.
    /// </summary>
    void SetTargetPositionToTargetPortCenterPosition();


    /// <summary>
    /// Changes the component type of the model.
    /// This is not recommended and should be used with extreme caution.
    /// </summary>
    /// <param name="componentType">Razor component type.</param>
    /// <param name="parameters">Component parameters. If, null default value will be used.</param>
    void SetComponent(Type componentType, Dictionary<string, object>? parameters = null);
}