﻿using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Ports;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Links;

/// <summary>
///     Represents events for classes that implement <see cref="ILink" />
/// </summary>
public interface ILinkEvents :
    ISelectionEvents<ILink>,
    IVisibilityEvents<ILink>,
    IPointerEvents<ILink>,
    ISizeEvents<ILink>,
    IUiEvents
{
    /// <summary>
    ///     Event triggered before a link is connected to a target port.
    /// </summary>
    public event Action<ILink, IPort?, IPort?> OnBeforeTargetPortConnected;

    /// <summary>
    ///     Event triggered before a link is assigned a source port.
    /// </summary>
    public event Action<ILink, IPort?, IPort> OnBeforeSourcePortConnected;

    /// <summary>
    ///     Triggers the <see cref="OnBeforeTargetPortConnected" />
    /// </summary>
    /// <param name="link">Link that will be connected to the port.</param>
    /// <param name="targetPort">Current target port of the link</param>
    /// <param name="newTargetPort">Target Port that will be assigned to the link.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyOnBeforeTargetPortConnected(ILink link, IPort? targetPort, IPort? newTargetPort,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnBeforeSourcePortConnected" />
    /// </summary>
    /// <param name="link">Link that will be connected to the port.</param>
    /// <param name="currentSourcePort">Current source port of the link</param>
    /// <param name="newSourcePort">Source Port that will be assigned to the link.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyOnBeforeSourcePortConnected(ILink link, IPort? currentSourcePort, IPort newSourcePort,
        [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the target position of the link changes.
    /// </summary>
    public event Action<int, int> OnTargetPositionChanged;

    /// <summary>
    ///     Triggers the <see cref="OnTargetPositionChanged" /> event.
    /// </summary>
    /// <param name="x">X coordinate of the new target position.</param>
    /// <param name="y">Y coordinate of the new target position.</param>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyTargetPositionChanged(int x, int y, [CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Event triggered when the target port is set.
    /// </summary>
    public event Action<IPort?> OnTargetPortChanged;

    /// <summary>
    ///     Event triggered when the source port is set.
    /// </summary>
    public event Action<IPort> OnSourcePortChanged;

    /// <summary>
    ///     Triggers the <see cref="OnTargetPortChanged" /> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyTargetPortChanged([CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnSourcePortChanged" /> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifySourcePortChanged([CallerFilePath] string? callerMemberName = null);
}