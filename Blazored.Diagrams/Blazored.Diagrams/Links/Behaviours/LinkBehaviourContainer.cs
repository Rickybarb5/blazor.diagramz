using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Links.Behaviours;

public class LinkBehaviourContainer : BehaviourContainer
{
    public LinkBehaviourContainer(ILink link)
    {
        Add(new DefaultLinkBehaviour(link));
        Add(new RedrawBehaviour<ILink>(link));
    }
}