using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Links.Behaviours;

/// <summary>
///     Standard link behaviour.
/// </summary>
public class DefaultLinkBehaviour : IBehaviour
{
    private readonly ILink _link;

    /// <summary>
    /// Instantiates a new <see cref="DefaultLinkBehaviour"/>
    /// </summary>
    /// <param name="link"></param>
    public DefaultLinkBehaviour(ILink link)
    {
        _link = link;
        EnableBehaviour();
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _link.OnBeforeTargetPortConnected -= HandleOnBeforeTargetPortConnected;
        _link.OnTargetPortChanged -= HandleTargetPortChanged;
        _link.OnBeforeSourcePortConnected -= HandleOnBeforeSourcePortConnected;
    }

    private void EnableBehaviour()
    {
        _link.OnBeforeTargetPortConnected += HandleOnBeforeTargetPortConnected;
        _link.OnTargetPortChanged += HandleTargetPortChanged;
        _link.OnBeforeSourcePortConnected += HandleOnBeforeSourcePortConnected;
    }

    private void HandleTargetPortChanged(IPort? obj)
    {
        if (obj is not null) _link.SetTargetPositionToTargetPortCenterPosition();
    }

    private void HandleOnBeforeSourcePortConnected(ILink link, IPort? currentSourcePort, IPort newSourcePort)
    {
        currentSourcePort?.OutgoingLinks.Remove(_link);
        newSourcePort.OutgoingLinks.Add(_link);
    }

    private void HandleOnBeforeTargetPortConnected(ILink link, IPort? currentTargetPort, IPort? newTargetPort)
    {
        currentTargetPort?.IncomingLinks.Remove(link);
        newTargetPort?.IncomingLinks.Add(_link);
    }
}