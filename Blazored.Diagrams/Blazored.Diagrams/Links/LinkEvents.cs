﻿using Blazored.Diagrams.Ports;
using Blazored.Diagrams.Helpers;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Links;

public partial class Link<TLinkComponent>
{
    /// <inheritdoc />
    public event Action<ILink, IPort?, IPort?>? OnBeforeTargetPortConnected;

    /// <inheritdoc />
    public event Action<ILink, IPort?, IPort>? OnBeforeSourcePortConnected;

    /// <inheritdoc />
    public void NotifyOnBeforeTargetPortConnected(ILink link, IPort? targetPort, IPort? newTargetPort,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnBeforeTargetPortConnected triggered - Link: {link.Id}, Current Target: {targetPort?.Id.ToString() ?? "none"}, New Target: {newTargetPort?.Id.ToString() ?? "none"}",
            callerMemberName);
        OnBeforeTargetPortConnected?.Invoke(link, targetPort, newTargetPort);
    }

    /// <inheritdoc />
    public void NotifyOnBeforeSourcePortConnected(ILink link, IPort? currentSourcePort, IPort newSourcePort,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnBeforeSourcePortConnected triggered - Link: {link.Id}, Current Source: {currentSourcePort?.Id.ToString() ?? "none"}, New Source: {newSourcePort.Id}",
            callerMemberName);
        OnBeforeSourcePortConnected?.Invoke(link, currentSourcePort, newSourcePort);
    }

    /// <inheritdoc />
    public event Action<int, int>? OnTargetPositionChanged;

    /// <inheritdoc />
    public void NotifyTargetPositionChanged(int x, int y, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnTargetPositionChanged triggered - Link: {Id}, New Position: X={x}, Y={y}",
            callerMemberName);
        OnTargetPositionChanged?.Invoke(x, y);
    }

    /// <inheritdoc />
    public event Action<IPort?>? OnTargetPortChanged;

    /// <inheritdoc />
    public event Action<IPort>? OnSourcePortChanged;

    /// <inheritdoc />
    public void NotifyTargetPortChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnTargetPortChanged triggered - Link: {Id}, Target Port: {_targetPort?.Id.ToString() ?? "none"}",
            callerMemberName);
        OnTargetPortChanged?.Invoke(_targetPort);
    }

    /// <inheritdoc />
    public void NotifySourcePortChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSourcePortChanged triggered - Link: {Id}, Source Port: {_sourcePort.Id}",
            callerMemberName);
        OnSourcePortChanged?.Invoke(_sourcePort);
    }

    /// <inheritdoc />
    public event Action<ILink, bool>? OnSelectionChanged;

    /// <inheritdoc />
    public void NotifySelectionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSelectionChanged triggered - Link: {Id}, Selected: {IsSelected}", callerMemberName);
        OnSelectionChanged?.Invoke(this, IsSelected);
    }

    /// <inheritdoc />
    public event Action<ILink, bool>? OnVisibilityChanged;

    /// <inheritdoc />
    public void NotifyVisibilityChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnVisibilityChanged triggered - Link: {Id}, Visible: {IsVisible}", callerMemberName);
        OnVisibilityChanged?.Invoke(this, IsVisible);
    }

    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnRedraw triggered for Link {Id}", callerMemberName);
        OnRedraw?.Invoke();
    }

    #region pointer events

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnPointerDown;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnPointerUp;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnPointerMove;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnPointerEnter;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnPointerLeave;

    /// <inheritdoc />
    public event Action<ILink, WheelEventArgs>? OnWheel;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnClick;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnDoubleClick;

    /// <inheritdoc />
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerDown triggered - Link: {Id}", callerMemberName);
        OnPointerDown?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerUp triggered - Link: {Id}", callerMemberName);
        OnPointerUp?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerMove triggered - Link: {Id}", callerMemberName);
        OnPointerMove?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerEnter triggered - Link: {Id}", callerMemberName);
        OnPointerEnter?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerLeave triggered - Link: {Id}", callerMemberName);
        OnPointerLeave?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnWheel triggered - Link: {Id}", callerMemberName);
        OnWheel?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnClick triggered - Link: {Id}", callerMemberName);
        OnClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnDoubleClick triggered - Link: {Id}", callerMemberName);
        OnDoubleClick?.Invoke(this, args);
    }

    #endregion

    /// <inheritdoc />
    public event Action<ILink>? OnSizeChanged;

    /// <inheritdoc />
    public void NotifySizeChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSizeChanged triggered - Link: {Id}", callerMemberName);
        OnSizeChanged?.Invoke(this);
    }
}