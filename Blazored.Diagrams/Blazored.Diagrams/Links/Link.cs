﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Links.Behaviours;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Links;

/// <inheritdoc />
public partial class Link<TLinkComponent> : ILink
{
    private bool _isSelected;
    private bool _isVisible = true;
    private IPort _sourcePort;
    private IPort? _targetPort;
    private int _targetPositionX;
    private int _targetPositionY;
    private int _width;
    private int _height;

    /// <summary>
    /// Instantiates a new <see cref="Link{TLinkComponent}"/>
    /// </summary>
    public Link()
    {
        Parameters = new Dictionary<string, object> { { nameof(LinkContainer.Link), this } };
        Behaviours = new LinkBehaviourContainer(this);
    }

    /// Severs the connection between link and ports.
    public virtual void Dispose()
    {
        _sourcePort.OutgoingLinks.Remove(this);
        _targetPort?.IncomingLinks.Remove(this);
        Behaviours.Dispose();
    }

    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IPort SourcePort
    {
        get => _sourcePort;
        set
        {
            ArgumentNullException.ThrowIfNull(value);
            if (_sourcePort != value)
            {
                NotifyOnBeforeSourcePortConnected(this, _sourcePort, value);
                _sourcePort = value;
                NotifySourcePortChanged();
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IPort? TargetPort
    {
        get => _targetPort;
        set
        {
            if (_targetPort != value && value is not null)
            {
                NotifyOnBeforeTargetPortConnected(this, _targetPort, value);
                _targetPort = value;
                NotifyTargetPortChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int TargetPositionX
    {
        get => _targetPositionX;
        set
        {
            if (_targetPositionX != value)
            {
                _targetPositionX = value;
                NotifyTargetPositionChanged(value, _targetPositionY);
            }
        }
    }

    /// <inheritdoc />
    public virtual int TargetPositionY
    {
        get => _targetPositionY;
        set
        {
            if (_targetPositionY != value)
            {
                _targetPositionY = value;
                NotifyTargetPositionChanged(_targetPositionX, value);
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual bool IsConnected => TargetPort is not null;

    /// <inheritdoc />
    public virtual bool IsSelected
    {
        get => _isSelected;
        set
        {
            if (_isSelected != value)
            {
                _isSelected = value;
                NotifySelectionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsVisible
    {
        get => _isVisible;
        set
        {
            if (_isVisible != value)
            {
                _isVisible = value;
                NotifyVisibilityChanged();
            }
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Type ComponentType { get; private set; } = typeof(TLinkComponent);

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Dictionary<string, object> Parameters { get; private set; }

    /// <inheritdoc />
    public virtual int Width
    {
        get => _width;
        set
        {
            if (_width != value)
            {
                _width = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int Height
    {
        get => _height;
        set
        {
            if (_height != value)
            {
                _height = value;
                NotifySizeChanged();
            }
        }
    }
}