using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Extensions;

namespace Blazored.Diagrams.Links;

public partial class Link<TLinkComponent>
{
    /// <inheritdoc cref="ILink.SetTargetPosition" />
    public virtual void SetTargetPosition(int x, int y)
    {
        var stateChanged = _targetPositionX != x || _targetPositionY != y;
        if (stateChanged)
        {
            _targetPositionX = x;
            _targetPositionY = y;
            NotifyTargetPositionChanged(_targetPositionX, _targetPositionY);
        }
    }

    /// <inheritdoc />
    public virtual void SetSize(int width, int height)
    {
        var stateChanged = width != _width || _height != height;
        if (stateChanged)
        {
            _width = width;
            _height = height;
            NotifySizeChanged();
        }
    }

    /// <summary>
    /// Sets the links target position to the target port's center.
    /// </summary>
    /// TODO:Make this more customizable
    public virtual void SetTargetPositionToTargetPortCenterPosition()
    {
        ArgumentNullException.ThrowIfNull(TargetPort);
        var centerCoordinates = TargetPort.GetCenterCoordinates();
        SetTargetPosition(centerCoordinates.CenterX, centerCoordinates.CenterY);
    }

    /// <inheritdoc />
    public void SetComponent(Type componentType, Dictionary<string, object>? parameters = null)
    {
        ComponentType = componentType;
        Parameters = parameters ?? new Dictionary<string, object> { { nameof(LinkContainer.Link), this } };
        NotifyRedraw();
    }
}