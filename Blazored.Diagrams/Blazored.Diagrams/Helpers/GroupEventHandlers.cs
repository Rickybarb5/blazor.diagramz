using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Handler storage for group events
/// </summary>
internal class GroupEventHandlers
{
    public Action<IGroup> GroupAddedHandler { get; init; }
    public Action<IGroup> GroupRemovedHandler { get; init; }
    public Action<INode> NodeAddedHandler { get; init; }
    public Action<INode> NodeRemovedHandler { get; init; }
    public Action<IPort> PortAddedHandler { get; init; }
    public Action<IPort> PortRemovedHandler { get; init; }
    public Action<IGroup, MouseEventArgs> ClickHandler { get; init; }
    public Action<IGroup, MouseEventArgs> DoubleClickHandler { get; init; }
    public Action<IGroup, PointerEventArgs> PointerDownHandler { get; init; }
    public Action<IGroup, PointerEventArgs> PointerEnterHandler { get; init; }
    public Action<IGroup, PointerEventArgs> PointerUpHandler { get; init; }
    public Action<IGroup, PointerEventArgs> PointerMoveHandler { get; init; }
    public Action<IGroup, PointerEventArgs> PointerLeaveHandler { get; init; }
    public Action<IGroup, WheelEventArgs> WheelHandler { get; init; }
}