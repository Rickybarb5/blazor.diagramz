using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Handler storage for port events
/// </summary>
internal class PortEventHandlers
{
    public Action<IPort, MouseEventArgs> ClickHandler { get; init; }
    public Action<IPort, MouseEventArgs> DoubleClickHandler { get; init; }
    public Action<IPort, PointerEventArgs> PointerDownHandler { get; init; }
    public Action<IPort, PointerEventArgs> PointerEnterHandler { get; init; }
    public Action<IPort, PointerEventArgs> PointerUpHandler { get; init; }
    public Action<IPort, PointerEventArgs> PointerMoveHandler { get; init; }
    public Action<IPort, PointerEventArgs> PointerLeaveHandler { get; init; }
    public Action<IPort, WheelEventArgs> WheelHandler { get; init; }
}