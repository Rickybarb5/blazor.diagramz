using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Handler storage for node events
/// </summary>
internal class NodeEventHandlers
{
    public Action<IPort> PortAddedHandler { get; init; }
    public Action<IPort> PortRemovedHandler { get; init; }
    public Action<INode, MouseEventArgs> ClickHandler { get; init; }
    public Action<INode, MouseEventArgs> DoubleClickHandler { get; init; }
    public Action<INode, PointerEventArgs> PointerDownHandler { get; init; }
    public Action<INode, PointerEventArgs> PointerEnterHandler { get; init; }
    public Action<INode, PointerEventArgs> PointerUpHandler { get; init; }
    public Action<INode, PointerEventArgs> PointerMoveHandler { get; init; }
    public Action<INode, PointerEventArgs> PointerLeaveHandler { get; init; }
    public Action<INode, WheelEventArgs> WheelHandler { get; init; }
}