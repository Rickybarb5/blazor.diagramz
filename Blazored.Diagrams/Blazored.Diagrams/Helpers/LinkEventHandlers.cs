using Blazored.Diagrams.Links;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Handler storage for link events
/// </summary>
internal class LinkEventHandlers
{
    public Action<ILink, MouseEventArgs> ClickHandler { get; init; }
    public Action<ILink, MouseEventArgs> DoubleClickHandler { get; init; }
    public Action<ILink, PointerEventArgs> PointerDownHandler { get; init; }
    public Action<ILink, PointerEventArgs> PointerUpHandler { get; init; }
    public Action<ILink, PointerEventArgs> PointerMoveHandler { get; init; }
    public Action<ILink, PointerEventArgs> PointerEnterHandler { get; init; }
    public Action<ILink, PointerEventArgs> PointerLeaveHandler { get; init; }
    public Action<ILink, WheelEventArgs> WheelHandler { get; init; }
}