﻿using System.Diagnostics.CodeAnalysis;

namespace Blazored.Diagrams.Helpers;

/// <summary>
///     Helper class for GetBoundClientRect
/// </summary>
[ExcludeFromCodeCoverage]
public class Rect
{
    /// <summary>
    /// Width of the rectangle
    /// </summary>
    public double Width { get; set; }
        
    /// <summary>
    /// Height of the rectangle
    /// </summary>
    public double Height { get; set; }
    /// <summary>
    /// Top coordinate of the rectangle
    /// </summary>
    public double Top { get; set; }
    /// <summary>
    /// Right coordinate of the rectangle
    /// </summary>
    public double Right { get; set; }
    /// <summary>
    /// Bottom coordinate of the rectangle
    /// </summary>
    public double Bottom { get; set; }
    /// <summary>
    /// Left coordinate of the rectangle
    /// </summary>
    public double Left { get; set; }
}