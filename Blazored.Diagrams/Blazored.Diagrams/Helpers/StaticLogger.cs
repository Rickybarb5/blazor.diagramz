﻿using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using System.Text;
using Blazored.Diagrams.Services.Options;

namespace Blazored.Diagrams.Helpers;

/// <summary>
///     Simple static logger.
/// </summary>
[ExcludeFromCodeCoverage]
public static class StaticLogger
{
    /// <summary>
    /// Logs useful content to the console.
    /// Only available in debug mode.
    /// </summary>
    /// <param name="message">Message to print to the console.</param>
    public static void Log(string message,
        [CallerFilePath] string? callerName = null)
    {
        StringBuilder sb = new();
        if (!DiagramLoggingOptions.LoggingEnabled) return;
        if (DiagramLoggingOptions.ShowCallerInformation && callerName is not null)
        {
            // Split by the directory separator and get the last element
            var parts = callerName.Split('\\');
            var fileName = parts[^1]; // ^1 gets the last element in the array
            sb.Append($"Caller: {fileName}. ");
        }

        sb.Append(message);
        Console.WriteLine(sb.ToString());
        sb.Clear();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="eventDelegate"></param>
    /// <param name="callerName"></param>
    /// <param name="parameters"></param>
    public static void LogEvent(Delegate? eventDelegate, [CallerFilePath] string? callerName = null,
        params object?[] parameters)
    {
        StringBuilder sb = new();
        var eventName = "UnknownEvent";
        if (!DiagramLoggingOptions.LoggingEnabled) return;
        // Split by the directory separator and get the last element.
        if (callerName is not null)
        {
            var parts = callerName.Split('\\');
            if (parts.Length >= 1)
            {
                var fileName = parts[^1]; // ^1 gets the last element in the array
                sb.Append($"Caller: {fileName}. ");
            }
        }

        // Get the event name from the delegate
        if (eventDelegate != null)
        {
            // Assuming you want the name of the event represented by the delegate
            var invocationList = eventDelegate.GetInvocationList();
            if (invocationList.Length > 0)
            {
                // Retrieve the name of the method of the first delegate
                var eventNames = eventDelegate.GetType().GetEvents();
            }
        }

        sb.Append($"Event: {eventName} triggered.");
        Console.WriteLine(sb.ToString());
        sb.Clear();
    }
}