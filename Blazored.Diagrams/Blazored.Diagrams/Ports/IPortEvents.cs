﻿using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Ports;

/// <summary>
///     Events that are triggered by the ports.
/// </summary>
public interface IPortEvents :
    IPositionEvents<IPort>,
    IVisibilityEvents<IPort>,
    IPointerEvents<IPort>,
    ISizeEvents<IPort>,
    ILinkContainerEvents,
    IUiEvents
{
    /// <summary>
    ///     Event triggered when the port alignment changes.
    /// </summary>
    public event Action<PortAlignment> OnPortAlignmentChanged;

    /// <summary>
    ///     Event triggered when PortPosition changes.
    /// </summary>
    public event Action<PortPosition> OnPortPositionChanged;

    /// <summary>
    ///     Triggers the <see cref="OnPortAlignmentChanged" /> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyAlignmentChanged([CallerFilePath] string? callerMemberName = null);

    /// <summary>
    ///     Triggers the <see cref="OnPortPositionChanged" /> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortPositionChanged([CallerFilePath] string? callerMemberName = null);

    /// <summary>
    /// Event triggered when the port is assigned to a container.
    /// </summary>
    public event Action<IPortContainer?>? OnPortParentChanged;

    /// <summary>
    /// Triggers the <see cref="OnPortParentChanged"/> event.
    /// </summary>
    /// <param name="callerMemberName">Name of the calling method.</param>
    void NotifyPortParentChanged([CallerFilePath] string? callerMemberName = null);
}