using Blazored.Diagrams.Extensions;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Ports.Behaviours;

/// <summary>
///     Move child models when the port moves
/// </summary>
public class PortMoveBehaviour : IBehaviour
{
    private readonly IPort _port;

    /// <summary>
    /// Instantiates a new <see cref="PortMoveBehaviour"/>
    /// </summary>
    /// <param name="port"></param>
    public PortMoveBehaviour(IPort port)
    {
        _port = port;
        EnableBehaviour();
    }

    private void MoveChildren(IPosition obj)
    {
        _port.IncomingLinks.ForEach(l =>
        {
            if (l.TargetPort is not null)
            {
                var centerCoordinates = l.TargetPort.GetCenterCoordinates();
                l.SetTargetPosition(centerCoordinates.CenterX, centerCoordinates.CenterY);
            }
        });
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _port.OnPositionChanged -= MoveChildren;
        _port.OnSizeChanged -= MoveChildren;
    }

    private void EnableBehaviour()
    {
        _port.OnPositionChanged += MoveChildren;
        _port.OnSizeChanged += MoveChildren;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}