using Blazored.Diagrams.Behaviours;

namespace Blazored.Diagrams.Ports.Behaviours;

public class PortBehaviourContainer : BehaviourContainer
{
    public PortBehaviourContainer(IPort port)
    {
        Add(new EventPipelineBehaviour<IPort>(port));
        Add(new LinkContainerBehaviour(port));
        Add(new PortMoveBehaviour(port));
        Add(new RedrawBehaviour<IPort>(port));
    }
}