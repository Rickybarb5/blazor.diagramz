﻿using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Helpers;
using Microsoft.AspNetCore.Components.Web;
using System.Runtime.CompilerServices;

namespace Blazored.Diagrams.Ports;

/// <summary>
///     Base implementation of the port events.
/// </summary>
public partial class Port<TComponent>
{
    /// <inheritdoc />
    public event Action<IPort, bool>? OnVisibilityChanged;

    /// <inheritdoc />
    public event Action<IPort>? OnPositionChanged;

    /// <inheritdoc />
    public event Action<IPort>? OnBeforePositionChanged;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPointerDown;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPointerUp;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPointerMove;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPointerEnter;

    /// <inheritdoc />
    public event Action<IPort, PointerEventArgs>? OnPointerLeave;

    /// <inheritdoc />
    public event Action<IPort, WheelEventArgs>? OnWheel;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnClick;

    /// <inheritdoc />
    public event Action<IPort, MouseEventArgs>? OnDoubleClick;

    /// <inheritdoc />
    public void NotifyPointerDown(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerDown triggered for Port {Id}", callerMemberName);
        OnPointerDown?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerUp(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerUp triggered for Port {Id}", callerMemberName);
        OnPointerUp?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerMove(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerMove triggered for Port {Id}", callerMemberName);
        OnPointerMove?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerEnter(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnPointerEnter triggered for Port {Id}", callerMemberName);
        OnPointerEnter?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyPointerLeave(PointerEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPointerLeave triggered for Port {Id}", callerMemberName);
        OnPointerLeave?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyWheel(WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnWheel triggered for Port {Id}", callerMemberName);
        OnWheel?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnClick triggered for Port {Id}", callerMemberName);
        OnClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public void NotifyDoubleClick(MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnDoubleClick triggered for Port {Id}", callerMemberName);
        OnDoubleClick?.Invoke(this, args);
    }

    /// <inheritdoc />
    public event Action<PortAlignment>? OnPortAlignmentChanged;

    /// <inheritdoc />
    public event Action<PortPosition>? OnPortPositionChanged;

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnIncomingLinkRemoved;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnOutgoingLinkRemoved;

    /// <inheritdoc />
    public void NotifyIncomingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnIncomingLinkAdded triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnIncomingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyIncomingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        OnIncomingLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnOutgoingLinkAdded triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnOutgoingLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyOutgoingLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnOutgoingLinkRemoved triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnOutgoingLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public event Action<IPortContainer?>? OnPortParentChanged;

    /// <inheritdoc />
    public void NotifyPortParentChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log(
            $"Event: OnPortParentChanged triggered for Port {Id} - Parent: {_parent?.Id.ToString() ?? "none"}",
            callerMemberName);
        OnPortParentChanged?.Invoke(_parent);
    }

    /// <inheritdoc />
    public void NotifyAlignmentChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortAlignmentChanged triggered for Port {Id} - Alignment: {Alignment}",
            callerMemberName);
        OnPortAlignmentChanged?.Invoke(Alignment);
    }

    /// <inheritdoc />
    public void NotifyPortPositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPortPositionChanged triggered for Port {Id} - Position: {Position}",
            callerMemberName);
        OnPortPositionChanged?.Invoke(Position);
    }

    /// <inheritdoc />
    public void NotifyVisibilityChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnVisibilityChanged triggered for Port {Id} - Visibility: {IsVisible}",
            callerMemberName);
        OnVisibilityChanged?.Invoke(this, IsVisible);
    }

    /// <inheritdoc />
    public void NotifyPositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnPositionChanged triggered for Port {Id}", callerMemberName);
        OnPositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public void NotifyBeforePositionChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnBeforePositionChanged triggered for Port {Id}", callerMemberName);
        OnBeforePositionChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public event Action? OnRedraw;

    /// <inheritdoc />
    public void NotifyRedraw([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnRedraw triggered for Port {Id}", callerMemberName);
        OnRedraw?.Invoke();
    }

    /// <inheritdoc />
    public event Action<IPort>? OnSizeChanged;

    /// <inheritdoc />
    public void NotifySizeChanged([CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnSizeChanged triggered for Port {Id}", callerMemberName);
        OnSizeChanged?.Invoke(this);
    }

    /// <inheritdoc />
    public event Action<ILink>? OnLinkAdded;

    /// <inheritdoc />
    public event Action<ILink>? OnLinkRemoved;

    /// <inheritdoc />
    public void NotifyLinkAdded(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkAdded triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkAdded?.Invoke(link);
    }

    /// <inheritdoc />
    public void NotifyLinkRemoved(ILink link, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkRemoved triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkRemoved?.Invoke(link);
    }

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerDown;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerUp;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerMove;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerEnter;

    /// <inheritdoc />
    public event Action<ILink, PointerEventArgs>? OnLinkPointerLeave;

    /// <inheritdoc />
    public event Action<ILink, WheelEventArgs>? OnLinkWheel;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkClicked;

    /// <inheritdoc />
    public event Action<ILink, MouseEventArgs>? OnLinkDoubleClicked;

    /// <inheritdoc />
    public void NotifyLinkPointerDown(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerDown triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkPointerDown?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerUp(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerUp triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkPointerUp?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerMove(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        // StaticLogger.Log($"Event: OnLinkPointerMove triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkPointerMove?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerEnter(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerEnter triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkPointerEnter?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkPointerLeave(ILink link, PointerEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkPointerLeave triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkPointerLeave?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkWheel(ILink link, WheelEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkWheel triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkWheel?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkClicked(ILink link, MouseEventArgs args, [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkClicked triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkClicked?.Invoke(link, args);
    }

    /// <inheritdoc />
    public void NotifyLinkDoubleClicked(ILink link, MouseEventArgs args,
        [CallerFilePath] string? callerMemberName = null)
    {
        StaticLogger.Log($"Event: OnLinkDoubleClicked triggered for Port {Id} - Link: {link.Id}", callerMemberName);
        OnLinkDoubleClicked?.Invoke(link, args);
    }
}