﻿using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Properties;
using System.Text.Json.Serialization;
using Blazored.Diagrams.Interfaces.Behaviours;

namespace Blazored.Diagrams.Ports;

/// <summary>
///     A port is what allows a model to connect/be connected to another model.
/// </summary>
public interface IPort :
    IId,
    IVisible,
    IUiComponentType,
    IPortParent,
    ISize,
    IModel,
    ILinkContainer,
    IPortEvents,
    IDisposable
{
    /// <summary>
    ///     Where the port will be aligned
    /// </summary>
    PortAlignment Alignment { get; set; }

    /// <summary>
    /// Where the port will be placed.
    /// </summary>
    PortPosition Position { get; set; }

    /// <summary>
    ///     Parent of the port.
    /// </summary>
    [JsonIgnore]
    IPortContainer? Parent { get; set; }

    /// <summary>
    /// Behaviour management.
    /// </summary>
    IBehaviourContainer Behaviours { get; }

    /// <summary>
    ///     Indicates if the port has one or more links attached.
    /// </summary>
    bool HasLinks { get; }

    /// <summary>
    /// Indicates if link creation is possible from this port.
    /// </summary>
    bool CanCreateLink();

    /// <summary>
    /// Calculates the port position using <see cref="PortAlignment"/> and see <see cref="PortPosition"/>
    /// </summary>
    /// <returns></returns>
    (int PositionX, int PositionY) CalculatePosition();

    /// <summary>
    /// Recalculates the port position.
    /// </summary>
    void RefreshPosition();

    /// <summary>
    ///     Indicates if this port can connect to another port.
    /// </summary>
    /// <param name="port">Port to evaluate if connection is possible</param>
    /// <returns>True if the link can connect to the input port, false otherwise.</returns>
    bool CanConnectTo(IPort port);
}