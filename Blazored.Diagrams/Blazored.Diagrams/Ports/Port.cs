﻿using System.Text.Json.Serialization;
using Blazored.Diagrams.Behaviours;
using Blazored.Diagrams.Components.Containers;
using Blazored.Diagrams.Helpers;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Ports.Behaviours;

namespace Blazored.Diagrams.Ports;

/// <summary>
///     Base class for a port.
/// </summary>
/// <typeparam name="TComponent">The UI component</typeparam>
public partial class Port<TComponent> : IPort
{
    private readonly ObservableList<ILink> _incomingLinks = [];
    private readonly ObservableList<ILink> _outgoingLinks = [];
    private PortAlignment _alignment = PortAlignment.Center;
    private int _height;
    private bool _isVisible = true;
    private IPortContainer? _parent;
    private PortPosition _position = PortPosition.Left;
    private int _positionX;
    private int _positionY;
    private int _width;

    /// <summary>
    ///     Instantiates a new <see cref="Port{TComponent}" />
    /// </summary>
    public Port()
    {
        Parameters = new Dictionary<string, object> { { nameof(PortContainer.Port), this } };
        Behaviours = new PortBehaviourContainer(this);
    }

    /// <inheritdoc />
    public virtual Guid Id { get; init; } = Guid.NewGuid();

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Type ComponentType { get; init; } = typeof(TComponent);

    /// <inheritdoc />
    [JsonIgnore]
    public virtual Dictionary<string, object> Parameters { get; init; }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IBehaviourContainer Behaviours { get; init; }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual bool HasLinks => OutgoingLinks.Any();

    /// <inheritdoc />
    public virtual int Width
    {
        get => _width;
        set
        {
            if (_width != value)
            {
                _width = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int Height
    {
        get => _height;
        set
        {
            if (_height != value)
            {
                _height = value;
                NotifySizeChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionX
    {
        get => _positionX;
        set
        {
            if (_positionX != value)
            {
                _positionX = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual int PositionY
    {
        get => _positionY;
        set
        {
            if (_positionY != value)
            {
                _positionY = value;
                NotifyPositionChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual bool IsVisible
    {
        get => _isVisible;
        set
        {
            if (_isVisible != value)
            {
                _isVisible = value;
                NotifyVisibilityChanged();
            }
        }
    }

    /// <inheritdoc />
    public virtual PortAlignment Alignment
    {
        get => _alignment;
        set
        {
            if (_alignment != value)
            {
                _alignment = value;
                NotifyAlignmentChanged();
                RefreshPosition();
            }
        }
    }

    /// <inheritdoc />
    public virtual PortPosition Position
    {
        get => _position;
        set
        {
            if (_position != value)
            {
                _position = value;
                NotifyPortPositionChanged();
                RefreshPosition();
            }
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<ILink> OutgoingLinks
    {
        get => _outgoingLinks;
        init
        {
            _outgoingLinks.Clear();
            foreach (var val in value) _outgoingLinks.Add(val);
        }
    }

    /// <inheritdoc />
    public virtual ObservableList<ILink> IncomingLinks
    {
        get => _incomingLinks;
        init
        {
            _incomingLinks.Clear();
            foreach (var val in value) _incomingLinks.Add(val);
        }
    }

    /// <inheritdoc />
    [JsonIgnore]
    public virtual IPortContainer? Parent
    {
        get => _parent;
        set
        {
            if (_parent != value)
            {
                _parent = value;
                NotifyPortParentChanged();
            }

            RefreshPosition();
        }
    }


    /// <inheritdoc />
    public virtual void Dispose()
    {
        _incomingLinks.ForEach(x => x.Dispose());
        _outgoingLinks.ForEach(x => x.Dispose());
        _incomingLinks.Clear();
        _outgoingLinks.Clear();
        Behaviours.Dispose();
    }
}