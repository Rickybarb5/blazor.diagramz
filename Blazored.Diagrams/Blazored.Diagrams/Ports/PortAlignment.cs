﻿namespace Blazored.Diagrams.Ports;

/// <summary>
///     Indicates where the port will be aligned.
/// </summary>
public enum PortAlignment
{
    /// <summary>
    ///     Center of the defined position.
    /// </summary>
    Center,

    /// <summary>
    ///     Start of the defined position.
    /// </summary>
    Start,

    /// <summary>
    ///     End of the defined position.
    /// </summary>
    End
}