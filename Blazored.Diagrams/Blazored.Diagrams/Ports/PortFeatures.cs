namespace Blazored.Diagrams.Ports;

public partial class Port<TComponent>
{
    /// <inheritdoc />
    public virtual void SetPosition(int x, int y)
    {
        var stateChanged = _positionX != x || _positionY != y;
        if (stateChanged)
        {
            NotifyBeforePositionChanged();
            _positionX = x;
            _positionY = y;
            NotifyPositionChanged();
        }
    }

    /// <inheritdoc />
    public virtual bool CanCreateLink()
    {
        return true;
    }

    /// <inheritdoc />
    public virtual void SetSize(int width, int height)
    {
        var stateChanged = width != _width || _height != height;
        if (stateChanged)
        {
            _width = width;
            _height = height;
            RefreshPosition();
        }
    }

    /// <summary>
    ///     Calculates the x and y coordinates based on the Postion and alignment.
    /// </summary>
    /// <returns>The x and y coordinates</returns>
    public virtual (int PositionX, int PositionY) CalculatePosition()
    {
        if (Parent is null) return (0, 0);

        var (x, y) = (Position, Alignment) switch
        {
            (PortPosition.Left, PortAlignment.Start) => (Parent.PositionX - Width / 2,
                Parent.PositionY - Height / 2),
            (PortPosition.Left, PortAlignment.Center) => (Parent.PositionX - Width / 2,
                Parent.PositionY + Parent.Height / 2 - Height / 2),
            (PortPosition.Left, PortAlignment.End) => (Parent.PositionX - Width / 2,
                Parent.PositionY + Parent.Height - Height / 2),

            (PortPosition.Right, PortAlignment.Start) => (Parent.PositionX + Parent.Width - Width / 2,
                Parent.PositionY - Height / 2),
            (PortPosition.Right, PortAlignment.Center) => (Parent.PositionX + Parent.Width - Width / 2,
                Parent.PositionY + Parent.Height / 2 - Height / 2),
            (PortPosition.Right, PortAlignment.End) => (Parent.PositionX + Parent.Width - Width / 2,
                Parent.PositionY + Parent.Height - Height / 2),

            (PortPosition.Top, PortAlignment.Start) =>
                (Parent.PositionX - Width / 2, Parent.PositionY - Height / 2),
            (PortPosition.Top, PortAlignment.Center) => (Parent.PositionX + Parent.Width / 2 - Width / 2,
                Parent.PositionY - Height / 2),
            (PortPosition.Top, PortAlignment.End) => (Parent.PositionX + Parent.Width - Width / 2,
                Parent.PositionY - Height / 2),

            (PortPosition.Bottom, PortAlignment.Start) => (Parent.PositionX - Width / 2,
                Parent.PositionY + Parent.Height - Height / 2),
            (PortPosition.Bottom, PortAlignment.Center) => (Parent.PositionX + Parent.Width / 2 - Width / 2,
                Parent.PositionY + Parent.Height - Height / 2),
            (PortPosition.Bottom, PortAlignment.End) => (Parent.PositionX + Parent.Width - Width / 2,
                Parent.PositionY + Parent.Height - Height / 2),
            (PortPosition.Custom, _) => (PositionX, PositionY),
            (PortPosition.CenterParent, _) => (
                Parent.PositionX + (Parent.Width / 2) - (Width / 2),
                Parent.PositionY + (Parent.Height / 2) - (Height / 2)
            ),
            _ => (PositionX, PositionY)
        };
        return (x, y);
    }

    /// <inheritdoc />
    public virtual void RefreshPosition()
    {
        var newPosition = CalculatePosition();
        SetPosition(newPosition.PositionX, newPosition.PositionY);
    }

    /// <inheritdoc />
    public virtual bool CanConnectTo(IPort port)
    {
        var canConnect =
            Id != port.Id &&
            Parent is not null &&
            port.Parent is not null &&
            Parent?.Id != port?.Parent?.Id;
        return canConnect;
    }
}