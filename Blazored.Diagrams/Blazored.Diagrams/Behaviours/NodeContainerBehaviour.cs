using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Nodes;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///     Manages child nodes.
/// </summary>
/// <typeparam name="NodeContainer"></typeparam>
public class NodeContainerBehaviour<NodeContainer> : IBehaviour
    where NodeContainer : INodeContainer, INodeContainerEvents
{
    private NodeContainer _container;

    /// <summary>
    ///     Instantiates a new <see cref="NodeContainerBehaviour{NodeContainer}"/>
    /// </summary>
    /// <param name="container"></param>
    public NodeContainerBehaviour(NodeContainer container)
    {
        _container = container;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _container.Nodes.OnItemAdded -= HandleAdded;
        _container.Nodes.OnItemRemoved -= HandleRemoved;
    }

    private void EnableBehaviour()
    {
        _container.Nodes.OnItemAdded += HandleAdded;
        _container.Nodes.OnItemRemoved += HandleRemoved;
    }

    private void HandleRemoved(INode obj)
    {
        _container.NotifyNodeRemoved(obj);
    }

    private void HandleAdded(INode obj)
    {
        _container.NotifyNodeAdded(obj);
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}