using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Ports;
using Microsoft.AspNetCore.Components.Web;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///  Ports only appear when a link hovers over a container.
/// </summary>
public class PortPopBehaviour : IBehaviour
{
    private IDiagram _diagram;

    public PortPopBehaviour(IDiagram diagram)
    {
        _diagram = diagram;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _diagram.OnPortAdded -= HandlePortAdded;
        _diagram.OnNodePointerEnter -= ShowPorts;
        _diagram.OnGroupPointerEnter -= ShowPorts;
        _diagram.OnNodePointerLeave -= HidePorts;
        _diagram.OnGroupPointerLeave -= HidePorts;
    }

    private void HidePorts(IPortContainer arg1, PointerEventArgs arg2)
    {
        arg1.Ports.ForEach(p => p.IsVisible = false);
    }

    private void ShowPorts(IPortContainer arg1, PointerEventArgs arg2)
    {
        arg1.Ports.ForEach(p => p.IsVisible = true);
    }

    private void EnableBehaviour()
    {
        _diagram.OnPortAdded += HandlePortAdded;
        _diagram.OnNodePointerEnter += ShowPorts;
        _diagram.OnGroupPointerEnter += ShowPorts;
        _diagram.OnNodePointerLeave += HidePorts;
        _diagram.OnGroupPointerLeave += HidePorts;
    }

    private void HandlePortAdded(IPort obj)
    {
        obj.IsVisible = false;
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}