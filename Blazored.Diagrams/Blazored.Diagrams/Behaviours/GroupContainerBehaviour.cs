using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///     Manages child groups.
/// </summary>
/// <typeparam name="GroupContainer"></typeparam>
public class GroupContainerBehaviour<GroupContainer> : IBehaviour
    where GroupContainer : IGroupContainer, IGroupContainerEvents
{
    private GroupContainer _container;
    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _container.Groups.OnItemAdded -= HandleAdded;
        _container.Groups.OnItemRemoved -= HandleRemoved;
    }

    private void EnableBehaviour()
    {
        _container.Groups.OnItemAdded += HandleAdded;
        _container.Groups.OnItemRemoved += HandleRemoved;
    }

    /// <summary>
    ///     Instantiates a new <see cref="GroupContainerBehaviour{GroupContainer}"/>
    /// </summary>
    /// <param name="container"></param>
    public GroupContainerBehaviour(GroupContainer container)
    {
        _container = container;
        EnableBehaviour();
    }

    private void HandleRemoved(IGroup obj)
    {
        _container.NotifyGroupRemoved(obj);
    }

    private void HandleAdded(IGroup obj)
    {
        _container.NotifyGroupAdded(obj);
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}