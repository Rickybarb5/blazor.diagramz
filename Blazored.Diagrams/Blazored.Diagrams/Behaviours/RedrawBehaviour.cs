﻿using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///     Some UI changes don't happen if they come from an external component.
///     This behaviour forces triggers the redraw event when necessary.
/// </summary>
/// <typeparam name="T"></typeparam>
public class RedrawBehaviour<T> : IBehaviour
    where T : IUiEvents
{
    private readonly T _model;

    /// <summary>
    /// Instantiates a new RedrawBehaviour{T}
    /// </summary>
    /// <param name="model">Model to be redrawn.</param>
    public RedrawBehaviour(T model)
    {
        _model = model;
        EnableBehaviour();
    }


    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        switch (_model)
        {
            case IDiagram diagram:
                diagram.OnNodeAdded -= NotifyRedraw;
                diagram.OnNodeRemoved -= NotifyRedraw;
                diagram.OnGroupAdded -= NotifyRedraw;
                diagram.OnGroupRemoved -= NotifyRedraw;
                diagram.OnPortAdded -= NotifyRedraw;
                diagram.OnPortRemoved -= NotifyRedraw;
                break;
            case ILayer layer:
                layer.OnNodeAdded += NotifyRedraw;
                layer.OnNodeRemoved += NotifyRedraw;
                layer.OnGroupAdded += NotifyRedraw;
                layer.OnGroupRemoved += NotifyRedraw;
                layer.OnPortAdded += NotifyRedraw;
                layer.OnPortRemoved += NotifyRedraw;
                break;
            case INode node:
                node.OnPortAdded -= NotifyRedraw;
                node.OnPortRemoved -= NotifyRedraw;
                node.OnSizeChanged -= NotifyRedraw;
                break;
            case IGroup group:
                group.OnPortAdded -= NotifyRedraw;
                group.OnPortRemoved -= NotifyRedraw;
                group.OnNodeAdded -= NotifyRedraw;
                group.OnNodeRemoved -= NotifyRedraw;
                group.OnGroupAdded -= NotifyRedraw;
                group.OnGroupRemoved -= NotifyRedraw;
                group.OnSizeChanged -= NotifyRedraw;
                group.OnPaddingChanged -= _ => NotifyRedraw(group);
                group.OnNestedGroupAdded -= NotifyRedraw;
                group.OnNestedGroupRemoved -= NotifyRedraw;
                group.OnNestedNodeAdded -= NotifyRedraw;
                group.OnNestedNodeRemoved -= NotifyRedraw;
                break;
            case IPort port:
                port.OnSizeChanged -= NotifyRedraw;
                port.OnPortPositionChanged -= p => RedrawPort(port, p);
                port.OnPortAlignmentChanged -= a => RedrawPort(port, a);
                break;
            case ILink link:
                link.OnSourcePortChanged -= NotifyRedraw;
                link.OnTargetPortChanged -= NotifyRedraw;
                break;
        }
    }

    private void EnableBehaviour()
    {
        switch (_model)
        {
            case IDiagram diagram:
                diagram.OnNodeAdded += NotifyRedraw;
                diagram.OnNodeRemoved += NotifyRedraw;
                diagram.OnGroupAdded += NotifyRedraw;
                diagram.OnGroupRemoved += NotifyRedraw;
                diagram.OnPortAdded += NotifyRedraw;
                diagram.OnPortRemoved += NotifyRedraw;
                break;
            case ILayer layer:
                layer.OnNodeAdded += NotifyRedraw;
                layer.OnNodeRemoved += NotifyRedraw;
                layer.OnGroupAdded += NotifyRedraw;
                layer.OnGroupRemoved += NotifyRedraw;
                layer.OnPortAdded += NotifyRedraw;
                layer.OnPortRemoved += NotifyRedraw;
                break;
            case INode node:
                node.OnPortAdded += NotifyRedraw;
                node.OnPortRemoved += NotifyRedraw;
                node.OnSizeChanged += NotifyRedraw;
                node.OnPositionChanged += NotifyRedraw;
                break;
            case IGroup group:
                group.OnPortAdded += NotifyRedraw;
                group.OnPortRemoved += NotifyRedraw;
                group.OnNodeAdded += NotifyRedraw;
                group.OnNodeRemoved += NotifyRedraw;
                group.OnGroupAdded += NotifyRedraw;
                group.OnGroupRemoved += NotifyRedraw;
                group.OnSizeChanged += NotifyRedraw;
                group.OnPaddingChanged += _ => NotifyRedraw(group);
                group.OnPositionChanged += NotifyRedraw;
                group.OnNestedGroupAdded += NotifyRedraw;
                group.OnNestedGroupRemoved += NotifyRedraw;
                group.OnNestedNodeAdded += NotifyRedraw;
                group.OnNestedNodeRemoved += NotifyRedraw;
                break;
            case IPort port:
                port.OnSizeChanged += RedrawPort;
                port.OnPositionChanged += RedrawPort;
                port.OnPortPositionChanged += p => RedrawPort(port, p);
                port.OnPortAlignmentChanged += a => RedrawPort(port, a);
                break;
            case ILink link:
                link.OnSourcePortChanged += NotifyRedraw;
                link.OnTargetPortChanged += NotifyRedraw;
                break;
        }
    }

    private void NotifyRedraw(IGroup arg1, INode arg2)
    {
        _model.NotifyRedraw();
        arg1.NotifyRedraw();
        arg2.NotifyRedraw();
    }

    private void NotifyRedraw(IGroup arg1, IGroup arg2)
    {
        _model.NotifyRedraw();
        arg1.NotifyRedraw();
        arg2.NotifyRedraw();
    }

    /// <summary>
    ///     Ports have links associated to them. Special logic is required.
    /// </summary>
    /// <param name="port"></param>
    private void RedrawPort(IPort port)
    {
        NotifyRedraw(port);
        foreach (var link in port.OutgoingLinks) NotifyRedraw(link);
    }

    private void RedrawPort(IPort port, PortPosition portPosition)
    {
        NotifyRedraw(port);
        foreach (var link in port.OutgoingLinks) link.NotifyRedraw();
        foreach (var link in port.IncomingLinks) link.NotifyRedraw();
    }

    private void RedrawPort(IPort port, PortAlignment alignment)
    {
        NotifyRedraw(port);
    }

    private void NotifyRedraw(IUiEvents? obj)
    {
        _model.NotifyRedraw();
        obj?.NotifyRedraw();
    }
}