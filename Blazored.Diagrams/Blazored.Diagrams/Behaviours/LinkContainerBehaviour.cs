using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///     Manages child links.
/// </summary>
public class LinkContainerBehaviour : IBehaviour
{
    private readonly IPort _port;

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _port.IncomingLinks.OnItemAdded -= HandleIncomingAdded;
        _port.OutgoingLinks.OnItemAdded -= HandleOutgoingAdded;
        _port.IncomingLinks.OnItemRemoved -= HandleIncomingRemoved;
        _port.OutgoingLinks.OnItemRemoved -= HandleOutgoingRemoved;
    }

    private void EnableBehaviour()
    {
        _port.IncomingLinks.OnItemAdded += HandleIncomingAdded;
        _port.OutgoingLinks.OnItemAdded += HandleOutgoingAdded;
        _port.IncomingLinks.OnItemRemoved += HandleIncomingRemoved;
        _port.OutgoingLinks.OnItemRemoved += HandleOutgoingRemoved;
        _port.IncomingLinks.ForEach(HandleIncomingAdded);
        _port.OutgoingLinks.ForEach(HandleOutgoingAdded);
    }

    /// <summary>
    ///     Instantiates a new <see cref="LinkContainerBehaviour"/>
    /// </summary>
    /// <param name="port">The port instance.</param>
    public LinkContainerBehaviour(IPort port)
    {
        _port = port;
        EnableBehaviour();
    }


    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void HandleOutgoingRemoved(ILink obj)
    {
        // Link without a source port should not exist
        obj.Dispose();
        _port.NotifyIncomingLinkRemoved(obj);
        _port.NotifyLinkRemoved(obj);
    }

    private void HandleIncomingRemoved(ILink obj)
    {
        obj.TargetPort = null;
        _port.NotifyOutgoingLinkRemoved(obj);
        _port.NotifyLinkRemoved(obj);
    }

    private void HandleOutgoingAdded(ILink obj)
    {
        obj.SourcePort = _port;
        _port.NotifyOutgoingLinkAdded(obj);
        _port.NotifyLinkAdded(obj);
    }

    private void HandleIncomingAdded(ILink obj)
    {
        obj.TargetPort = _port;
        _port.NotifyIncomingLinkAdded(obj);
        _port.NotifyLinkAdded(obj);
    }
}