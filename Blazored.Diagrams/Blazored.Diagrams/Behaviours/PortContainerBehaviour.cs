using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Manages child ports.
/// </summary>
/// <typeparam name="PortContainer"></typeparam>
public class PortContainerBehaviour<PortContainer> : IBehaviour
    where PortContainer : IPortContainer, IPortContainerEvents
{
    private PortContainer _container;

    /// <summary>
    ///     Instantiates a new <see cref="PortContainerBehaviour{PortContainer}"/>
    /// </summary>
    /// <param name="container">The port container instance.</param>
    public PortContainerBehaviour(PortContainer container)
    {
        _container = container;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        _container.Ports.OnItemAdded -= HandlePortAdded;
        _container.Ports.OnItemRemoved -= HandlePortRemoved;
    }

    private void EnableBehaviour()
    {
        _container.Ports.OnItemAdded += HandlePortAdded;
        _container.Ports.OnItemRemoved += HandlePortRemoved;
    }

    private void HandlePortRemoved(IPort obj)
    {
        obj.Parent = null;
        _container.NotifyPortRemoved(obj);
    }

    private void HandlePortAdded(IPort obj)
    {
        obj.Parent = _container;
        _container.NotifyPortAdded(obj);
    }

    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }
}