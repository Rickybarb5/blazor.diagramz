using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Interfaces.Behaviours;
using Blazored.Diagrams.Interfaces.Containers;
using Blazored.Diagrams.Interfaces.Events;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
///     Handles event propagation for a container and it's children.
/// </summary>
/// <typeparam name="Container"></typeparam>
public class EventPipelineBehaviour<Container> : IBehaviour
{
    private readonly Container _masterContainer;

    /// <summary>
    ///     Instantiates a new <see cref="EventPipelineBehaviour{Container}"/>
    /// </summary>
    /// <param name="masterContainer"></param>
    public EventPipelineBehaviour(Container masterContainer)
    {
        _masterContainer = masterContainer;
        EnableBehaviour();
    }

    private bool _isEnabled = true;


    /// <inheritdoc />
    public bool IsEnabled
    {
        get => _isEnabled;
        set
        {
            if (value == _isEnabled) return;
            _isEnabled = value;
            if (value)
            {
                EnableBehaviour();
            }
            else
            {
                DisableBehaviour();
            }
        }
    }

    private void DisableBehaviour()
    {
        if (_masterContainer is ILayerContainer layerContainer) StopLayerContainerPropagation(layerContainer);
        if (_masterContainer is IPortContainer portContainer) StopPortContainerPropagation(portContainer);
        if (_masterContainer is INodeContainer nodeContainer) StopNodeContainerPropagation(nodeContainer);
        if (_masterContainer is IGroupContainer groupContainer) StopGroupContainerPropagation(groupContainer);
        if (_masterContainer is ILinkContainer linkContainer) StopLinkContainerPropagation(linkContainer);
    }

    private void EnableBehaviour()
    {
        SetupPropagation();
    }


    /// <inheritdoc />
    public void Dispose()
    {
        DisableBehaviour();
    }

    private void SetupPropagation()
    {
        if (_masterContainer is ILayerContainer layerContainer) SetupLayerContainerPropagation(layerContainer);
        if (_masterContainer is IPortContainer portContainer) SetupPortContainerPropagation(portContainer);
        if (_masterContainer is INodeContainer nodeContainer) SetupNodeContainerPropagation(nodeContainer);
        if (_masterContainer is IGroupContainer groupContainer) SetupGroupContainerPropagation(groupContainer);
        if (_masterContainer is ILinkContainer linkContainer) SetupLinkContainerPropagation(linkContainer);
    }

    private void SetupLayerContainerPropagation(ILayerContainer layerContainer)
    {
        layerContainer.Layers.OnItemAdded += Propagate;
        layerContainer.Layers.OnItemRemoved += StopPropagation;
        layerContainer.Layers.ForEach(Propagate);
    }

    private void StopLayerContainerPropagation(ILayerContainer layerContainer)
    {
        layerContainer.Layers.OnItemAdded -= Propagate;
        layerContainer.Layers.OnItemRemoved -= StopPropagation;
        layerContainer.Layers.ForEach(StopPropagation);
    }

    private void StopPropagation(ILayer obj)
    {
        StopGroupContainerPropagation(obj);
        StopNodeContainerPropagation(obj);
    }

    private void Propagate(ILayer obj)
    {
        SetupGroupContainerPropagation(obj);
        SetupNodeContainerPropagation(obj);
    }

    private void SetupLinkContainerPropagation(ILinkContainer linkContainer)
    {
        linkContainer.IncomingLinks.OnItemAdded += Propagate;
        linkContainer.IncomingLinks.OnItemRemoved += StopPropagation;
        linkContainer.OutgoingLinks.OnItemAdded += Propagate;
        linkContainer.OutgoingLinks.OnItemAdded += StopPropagation;

        linkContainer.IncomingLinks.ForEach(Propagate);
        linkContainer.OutgoingLinks.ForEach(Propagate);
        if (linkContainer is ILinkContainerEvents lce && _masterContainer is ILinkContainerEvents mce &&
            !_masterContainer.Equals(linkContainer))
        {
            lce.OnLinkAdded += link => mce.NotifyLinkAdded(link);
            lce.OnLinkRemoved += link => mce.NotifyLinkRemoved(link);

            lce.OnIncomingLinkAdded += (link) => mce.NotifyIncomingLinkAdded(link);
            lce.OnIncomingLinkRemoved += (link) => mce.NotifyIncomingLinkRemoved(link);
            lce.OnOutgoingLinkAdded += (link) => mce.NotifyOutgoingLinkAdded(link);
            lce.OnOutgoingLinkRemoved += (link) => mce.NotifyOutgoingLinkRemoved(link);
        }
    }

    private void SetupGroupContainerPropagation(IGroupContainer groupContainer)
    {
        groupContainer.Groups.OnItemAdded += Propagate;
        groupContainer.Groups.OnItemRemoved += StopPropagation;
        groupContainer.Groups.ForEach(Propagate);
        if (groupContainer is IGroupContainerEvents pce && _masterContainer is IGroupContainerEvents mce &&
            !_masterContainer.Equals(groupContainer))
        {
            pce.OnGroupAdded += group => mce.NotifyGroupAdded(group);
            pce.OnGroupRemoved += group => mce.NotifyGroupRemoved(group);
        }
    }

    private void SetupNodeContainerPropagation(INodeContainer nodeContainer)
    {
        nodeContainer.Nodes.OnItemAdded += Propagate;
        nodeContainer.Nodes.OnItemRemoved += StopPropagation;
        nodeContainer.Nodes.ForEach(Propagate);
        if (nodeContainer is INodeContainerEvents nce && _masterContainer is INodeContainerEvents mce &&
            !_masterContainer.Equals(nodeContainer))
        {
            nce.OnNodeAdded += node => mce.NotifyNodeAdded(node);
            nce.OnNodeRemoved += node => mce.NotifyNodeRemoved(node);
        }
    }

    private void SetupPortContainerPropagation(IPortContainer portContainer)
    {
        portContainer.Ports.OnItemAdded += Propagate;
        portContainer.Ports.OnItemRemoved += StopPropagation;
        portContainer.Ports.ForEach(Propagate);
        if (portContainer is IPortContainerEvents pce && _masterContainer is IPortContainerEvents mce &&
            !_masterContainer.Equals(portContainer))
        {
            pce.OnPortAdded += port => mce.NotifyPortAdded(port);
            pce.OnPortRemoved += port => mce.NotifyPortRemoved(port);
        }
    }

    private void StopNodeContainerPropagation(INodeContainer nodeContainer)
    {
        nodeContainer.Nodes.OnItemAdded -= Propagate;
        nodeContainer.Nodes.OnItemRemoved -= StopPropagation;
        nodeContainer.Nodes.ForEach(StopPropagation);
        if (nodeContainer is INodeContainerEvents nce && _masterContainer is INodeContainerEvents mce &&
            !_masterContainer.Equals(nodeContainer))
        {
            nce.OnNodeAdded -= node => mce.NotifyNodeAdded(node);
            nce.OnNodeRemoved -= node => mce.NotifyNodeRemoved(node);
        }
    }

    private void StopGroupContainerPropagation(IGroupContainer groupContainer)
    {
        groupContainer.Groups.OnItemAdded -= Propagate;
        groupContainer.Groups.OnItemRemoved -= StopPropagation;
        groupContainer.Groups.ForEach(StopPropagation);
        if (groupContainer is IGroupContainerEvents pce && _masterContainer is IGroupContainerEvents mce &&
            !_masterContainer.Equals(groupContainer))
        {
            pce.OnGroupAdded -= group => mce.NotifyGroupAdded(group);
            pce.OnGroupRemoved -= group => mce.NotifyGroupRemoved(group);
        }
    }

    private void StopPortContainerPropagation(IPortContainer portContainer)
    {
        portContainer.Ports.OnItemAdded -= Propagate;
        portContainer.Ports.OnItemRemoved -= StopPropagation;
        portContainer.Ports.ForEach(StopPropagation);
        if (portContainer is IPortContainerEvents pce && _masterContainer is IPortContainerEvents mce &&
            !_masterContainer.Equals(portContainer))
        {
            pce.OnPortAdded -= port => mce.NotifyPortAdded(port);
            pce.OnPortRemoved -= port => mce.NotifyPortRemoved(port);
        }
    }

    private void StopLinkContainerPropagation(ILinkContainer linkContainer)
    {
        linkContainer.IncomingLinks.OnItemAdded -= Propagate;
        linkContainer.IncomingLinks.OnItemRemoved -= StopPropagation;
        linkContainer.OutgoingLinks.OnItemAdded -= Propagate;
        linkContainer.OutgoingLinks.OnItemAdded -= StopPropagation;

        linkContainer.IncomingLinks.ForEach(StopPropagation);
        linkContainer.OutgoingLinks.ForEach(StopPropagation);
        if (linkContainer is ILinkContainerEvents lce && _masterContainer is ILinkContainerEvents mce &&
            !_masterContainer.Equals(linkContainer))
        {
            lce.OnLinkAdded -= link => mce.NotifyLinkAdded(link);
            lce.OnLinkRemoved -= link => mce.NotifyLinkRemoved(link);

            lce.OnIncomingLinkAdded -= (link) => mce.NotifyIncomingLinkAdded(link);
            lce.OnIncomingLinkRemoved -= (link) => mce.NotifyIncomingLinkRemoved(link);
            lce.OnOutgoingLinkAdded -= (link) => mce.NotifyOutgoingLinkAdded(link);
            lce.OnOutgoingLinkRemoved -= (link) => mce.NotifyOutgoingLinkRemoved(link);
        }
    }

    private void StopPropagation(IGroup group)
    {
        if (_masterContainer is IGroupContainerEvents parentEvents)
        {
            group.OnGroupAdded -= g => parentEvents.NotifyGroupAdded(g);
            group.OnGroupRemoved -= g => parentEvents.NotifyGroupRemoved(g);

            group.OnPointerDown -= (g, args) => parentEvents.NotifyGroupPointerDown(g, args);
            group.OnPointerEnter -= (g, args) => parentEvents.NotifyGroupPointerEnter(g, args);
            group.OnPointerUp -= (g, args) => parentEvents.NotifyGroupPointerUp(g, args);
            group.OnPointerMove -= (g, args) => parentEvents.NotifyGroupPointerMove(g, args);
            group.OnWheel -= (g, args) => parentEvents.NotifyGroupWheel(g, args);
            group.OnPointerLeave -= (g, args) => parentEvents.NotifyGroupPointerLeave(g, args);
            group.OnClick -= (g, args) => parentEvents.NotifyGroupClicked(g, args);
            group.OnDoubleClick -= (g, args) => parentEvents.NotifyGroupDoubleClicked(g, args);

            StopNodeContainerPropagation(group);
            StopPortContainerPropagation(group);
        }
    }

    private void Propagate(IGroup group)
    {
        if (_masterContainer is not IGroupContainerEvents parentEvents) return;

        group.OnGroupAdded += (g) => parentEvents.NotifyGroupAdded(g);
        group.OnGroupRemoved += (g) => parentEvents.NotifyGroupRemoved(g);

        group.OnPointerDown += (g, args) => parentEvents.NotifyGroupPointerDown(g, args);
        group.OnPointerEnter += (g, args) => parentEvents.NotifyGroupPointerEnter(g, args);
        group.OnPointerUp += (g, args) => parentEvents.NotifyGroupPointerUp(g, args);
        group.OnPointerMove += (g, args) => parentEvents.NotifyGroupPointerMove(g, args);
        group.OnWheel += (g, args) => parentEvents.NotifyGroupWheel(g, args);
        group.OnPointerLeave += (g, args) => parentEvents.NotifyGroupPointerLeave(g, args);
        group.OnClick += (g, args) => parentEvents.NotifyGroupClicked(g, args);
        group.OnDoubleClick += (g, args) => parentEvents.NotifyGroupDoubleClicked(g, args);

        // propagate child events to mater container.
        SetupGroupContainerPropagation(group);
        SetupNodeContainerPropagation(group);
        SetupPortContainerPropagation(group);
    }

    private void StopPropagation(IPort port)
    {
        if (_masterContainer is not IPortContainerEvents parentEvents) return;
        port.OnClick -= (p, args) => parentEvents.NotifyPortClicked(port, args);
        port.OnDoubleClick -= (p, args) => parentEvents.NotifyPortDoubleClicked(port, args);
        port.OnPointerUp -= (p, args) => parentEvents.NotifyPortPointerUp(port, args);
        port.OnPointerDown -= (p, args) => parentEvents.NotifyPortPointerDown(port, args);
        port.OnPointerMove -= (p, args) => parentEvents.NotifyPortPointerMove(port, args);
        port.OnPointerLeave -= (p, args) => parentEvents.NotifyPortPointerLeave(port, args);
        port.OnPointerEnter -= (p, args) => parentEvents.NotifyPortPointerEnter(port, args);
        port.OnWheel -= (p, args) => parentEvents.NotifyPortWheel(port, args);

        StopLinkContainerPropagation(port);
    }

    private void Propagate(IPort port)
    {
        if (_masterContainer is not IPortContainerEvents parentEvents) return;
        port.OnClick += (p, args) => parentEvents.NotifyPortClicked(port, args);
        port.OnDoubleClick += (p, args) => parentEvents.NotifyPortDoubleClicked(port, args);
        port.OnPointerUp += (p, args) => parentEvents.NotifyPortPointerUp(port, args);
        port.OnPointerDown += (p, args) => parentEvents.NotifyPortPointerDown(port, args);
        port.OnPointerMove += (p, args) => parentEvents.NotifyPortPointerMove(port, args);
        port.OnPointerLeave += (p, args) => parentEvents.NotifyPortPointerLeave(port, args);
        port.OnPointerEnter += (p, args) => parentEvents.NotifyPortPointerEnter(port, args);
        port.OnWheel += (p, args) => parentEvents.NotifyPortWheel(port, args);

        SetupLinkContainerPropagation(port);
    }

    private void Propagate(INode node)
    {
        if (_masterContainer is not INodeContainerEvents parentEvents) return;
        node.OnClick += (n, args) => parentEvents.NotifyNodeClicked(node, args);
        node.OnDoubleClick += (n, args) => parentEvents.NotifyNodeDoubleClicked(node, args);
        node.OnPointerUp += (n, args) => parentEvents.NotifyNodePointerUp(node, args);
        node.OnPointerDown += (n, args) => parentEvents.NotifyNodePointerDown(node, args);
        node.OnPointerMove += (n, args) => parentEvents.NotifyNodePointerMove(node, args);
        node.OnPointerLeave += (n, args) => parentEvents.NotifyNodePointerLeave(node, args);
        node.OnPointerEnter += (n, args) => parentEvents.NotifyNodePointerEnter(node, args);
        node.OnWheel += (n, args) => parentEvents.NotifyNodeWheel(node, args);

        SetupPortContainerPropagation(node);
    }

    private void StopPropagation(INode node)
    {
        if (_masterContainer is not INodeContainerEvents parentEvents) return;
        node.OnPointerDown -= (n, args) => parentEvents.NotifyNodePointerDown(node, args);
        node.OnPointerEnter -= (n, args) => parentEvents.NotifyNodePointerEnter(node, args);
        node.OnPointerUp -= (n, args) => parentEvents.NotifyNodePointerUp(node, args);
        node.OnPointerMove -= (n, args) => parentEvents.NotifyNodePointerMove(node, args);
        node.OnWheel -= (n, args) => parentEvents.NotifyNodeWheel(node, args);
        node.OnPointerLeave -= (n, args) => parentEvents.NotifyNodePointerLeave(node, args);
        node.OnClick -= (n, args) => parentEvents.NotifyNodeClicked(node, args);
        node.OnDoubleClick -= (n, args) => parentEvents.NotifyNodeDoubleClicked(node, args);

        StopPortContainerPropagation(node);
    }

    private void StopPropagation(ILink link)
    {
        if (_masterContainer is not ILinkContainerEvents parentEvents) return;
        link.OnClick -= (l, args) => parentEvents.NotifyLinkClicked(l, args);
        link.OnDoubleClick -= (l, args) => parentEvents.NotifyLinkDoubleClicked(l, args);
        link.OnPointerDown -= (l, args) => parentEvents.NotifyLinkPointerDown(l, args);
        link.OnPointerUp -= (l, args) => parentEvents.NotifyLinkPointerUp(l, args);
        link.OnPointerUp -= (l, args) => parentEvents.NotifyLinkPointerMove(l, args);
        link.OnPointerEnter -= (l, args) => parentEvents.NotifyLinkPointerEnter(l, args);
        link.OnPointerLeave -= (l, args) => parentEvents.NotifyLinkPointerLeave(l, args);
        link.OnWheel -= (l, args) => parentEvents.NotifyLinkWheel(l, args);
    }

    private void Propagate(ILink link)
    {
        if (_masterContainer is not ILinkContainerEvents parentEvents) return;
        link.OnClick += (l, args) => parentEvents.NotifyLinkClicked(l, args);
        link.OnDoubleClick += (l, args) => parentEvents.NotifyLinkDoubleClicked(l, args);
        link.OnPointerDown += (l, args) => parentEvents.NotifyLinkPointerDown(l, args);
        link.OnPointerUp += (l, args) => parentEvents.NotifyLinkPointerUp(l, args);
        link.OnPointerUp += (l, args) => parentEvents.NotifyLinkPointerMove(l, args);
        link.OnPointerEnter += (l, args) => parentEvents.NotifyLinkPointerEnter(l, args);
        link.OnPointerLeave += (l, args) => parentEvents.NotifyLinkPointerLeave(l, args);
        link.OnWheel += (l, args) => parentEvents.NotifyLinkWheel(l, args);
    }
}