using Blazored.Diagrams.Interfaces.Behaviours;

namespace Blazored.Diagrams.Behaviours;

/// <summary>
/// Base functionality for classes that have behaviours.
/// </summary>
public partial class BehaviourContainer : IBehaviourContainer
{
    private readonly List<IBehaviour> _behaviours = [];

    /// <inheritdoc />
    public IReadOnlyList<IBehaviour> All => _behaviours;

    /// <inheritdoc />
    public void Add<TBehaviour>(TBehaviour behaviour) where TBehaviour : IBehaviour
    {
        if (_behaviours.All(x => x is not TBehaviour)) _behaviours.Add(behaviour);
    }

    /// <inheritdoc />
    public void Remove<TBehaviour>() where TBehaviour : IBehaviour
    {
        var instance = _behaviours.FirstOrDefault(x => x is TBehaviour);
        if (instance is not null)
        {
            _behaviours.Remove(instance);
            instance.Dispose();
        }
    }

    /// <inheritdoc />
    public bool Exists<TBehaviour>() where TBehaviour : IBehaviour
    {
        return _behaviours.Exists(x => x.GetType() == typeof(TBehaviour));
    }

    /// <inheritdoc />
    public TBehaviour? Get<TBehaviour>() where TBehaviour : class, IBehaviour
    {
        var behaviour = _behaviours.FirstOrDefault(x => x.GetType() == typeof(TBehaviour));
        return behaviour as TBehaviour;
    }

    /// <inheritdoc />
    public bool TryGet<TBehaviour>(out TBehaviour behaviour) where TBehaviour : class, IBehaviour
    {
#pragma warning disable CS8601 // Possible null reference assignment.
        behaviour = Get<TBehaviour>();
#pragma warning restore CS8601 // Possible null reference assignment.
        return behaviour is not null;
    }


    /// <inheritdoc />
    public void Dispose()
    {
        foreach (var behaviour in _behaviours)
        {
            behaviour.Dispose();
        }
    }
}