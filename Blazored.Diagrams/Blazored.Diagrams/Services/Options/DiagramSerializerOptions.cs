using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Text.Json;
using Blazored.Diagrams.Services.Serialization;

namespace Blazored.Diagrams.Services.Options;

/// <summary>
/// Options class for the <see cref="DiagramSerializer"/>
/// </summary>
[ExcludeFromCodeCoverage]
public class DiagramSerializerOptions
{
    /// <summary>
    ///     Assemblies to use during the serialization process.
    ///     Used to find the matching components.
    /// </summary>
    public List<Assembly> Assemblies { get; set; } = [];

    /// <summary>
    ///     Options used for Json serialization.
    /// </summary>
    public JsonSerializerOptions JsonSerializerOptions = new()
    {
        WriteIndented = true,
        PropertyNameCaseInsensitive = true,
        ReferenceHandler = new CustomReferenceHandler(),
    };
}