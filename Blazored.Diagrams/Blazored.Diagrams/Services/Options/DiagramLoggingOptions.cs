using System.Diagnostics.CodeAnalysis;

namespace Blazored.Diagrams.Services.Options;

/// <summary>
/// Set of options regarding logging.
/// </summary>
[ExcludeFromCodeCoverage]
public class DiagramLoggingOptions
{
    /// <summary>
    /// Global variable that prints things to the console.
    /// </summary>
    public static bool LoggingEnabled = true;

    /// <summary>
    /// If enabled, it will print information about the caller to the console.
    /// </summary>
    public static bool ShowCallerInformation = true;
}