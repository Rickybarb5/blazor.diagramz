using System.Diagnostics.CodeAnalysis;

namespace Blazored.Diagrams.Services.Options;

/// <summary>
/// Options used to customize the diagram library.
/// </summary>
[ExcludeFromCodeCoverage]
public class BlazoredDiagramsOptions
{
    /// <summary>
    /// See <see cref="DiagramSerializerOptions"/>
    ///     If not set, default settings will be used.
    /// </summary>
    public DiagramSerializerOptions SerializerOptions { get; set; } = new();

    /// <summary>
    /// <see cref="DiagramLoggingOptions"/>
    /// </summary>
    public DiagramLoggingOptions LoggingOptions { get; set; } = new();
}