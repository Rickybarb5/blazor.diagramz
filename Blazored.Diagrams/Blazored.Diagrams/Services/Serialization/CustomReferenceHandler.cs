using System.Text.Json.Serialization;

namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
/// Custom reference handler to manage cycles and repeated references.
/// </summary>
public class CustomReferenceHandler : ReferenceHandler
{
    private readonly CustomReferenceResolver _resolver = new();

    /// <inheritdoc />
    public override CustomReferenceResolver CreateResolver()
    {
        return _resolver;
    }
}