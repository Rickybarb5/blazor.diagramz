﻿using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using Blazored.Diagrams.Interfaces.Properties;
using Microsoft.AspNetCore.Components;

namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
/// Generic converter for diagram models.
/// </summary>
/// <typeparam name="TInterface">Can be groups, nodes, ports, etc...</typeparam>
public class PolymorphicJsonConverter<TInterface> : JsonConverter<TInterface> where TInterface : class
{
    private readonly Dictionary<string, Type> _typeMap = [];
    private readonly Dictionary<string, Type> _componentTypeMap = [];

    /// <summary>
    /// Registers a type that implements TInterface.
    /// </summary>
    /// <param name="type"></param>
    /// <exception cref="ArgumentException"></exception>
    public void RegisterType(Type type)
    {
        if (!typeof(TInterface).IsAssignableFrom(type))
        {
            throw new ArgumentException($"Type {type.Name} does not implement {typeof(TInterface).Name}");
        }

        _typeMap.TryAdd(type.Name!, type);
    }

    /// <summary>
    /// Registers a component by its type name.
    /// </summary>
    /// <param name="type"></param>
    private void RegisterComponentType(Type type)
    {
        _componentTypeMap.TryAdd(type.Name!, type);
    }

    /// <summary>
    /// Checks if a component is valid for registration.
    /// </summary>
    /// <param name="type">Component type</param>
    /// <returns></returns>
    private static bool ComponentTypeIsValid(Type type)
    {
        // Only components are allowed
        if (!typeof(IComponent).IsAssignableFrom(type))
        {
            return false;
        }

        foreach (var property in type.GetProperties())
        {
            // Must be a [Parameter]
            var hasParameterAttr = property.GetCustomAttributes(false)
                .Any(attr => attr is ParameterAttribute);
            if (!hasParameterAttr)
            {
                continue;
            }

            // Has to be compatible with the calling interface
            if (typeof(TInterface).IsAssignableFrom(property.PropertyType) ||
                (property.PropertyType.IsGenericType &&
                 property.PropertyType.GetGenericTypeDefinition() == typeof(TInterface)))
            {
                return true;
            }

            // If it's not compatible with the property, it might inherit it
            var baseType = property.PropertyType.BaseType;
            while (baseType != null)
            {
                if (typeof(TInterface).IsAssignableFrom(baseType))
                {
                    return true;
                }

                baseType = baseType.BaseType;
            }
        }

        return false;
    }

    private static IEnumerable<Type> GetComponentTypes(Assembly assembly)
    {
        var componentTypes = assembly.GetTypes()
            .Where(ComponentTypeIsValid);

        return componentTypes;
    }

    /// <summary>
    /// Scans an assembly in search of classes that implement TInterface
    /// </summary>
    /// <param name="assembly"></param>
    public void RegisterTypesFromAssembly(Assembly assembly)
    {
        var types = GetTypes(assembly);
        var componentTypes = GetComponentTypes(assembly);
        foreach (var type in types)
        {
            RegisterType(type);
        }

        foreach (var type in componentTypes)
        {
            RegisterComponentType(type);
        }
    }

    private static IEnumerable<Type> GetTypes(Assembly assembly)
    {
        return assembly.GetTypes()
            .Where(t => typeof(TInterface).IsAssignableFrom(t) && t is { IsInterface: false, IsAbstract: false });
    }

    /// <summary>
    /// Transforms a string into an object.
    /// </summary>
    /// <param name="reader"></param>
    /// <param name="typeToConvert"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    /// <exception cref="JsonException"></exception>
    public override TInterface? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var resolver = (CustomReferenceResolver)options.ReferenceHandler!.CreateResolver();
        using var doc = JsonDocument.ParseValue(ref reader);
        var rootElement = doc.RootElement;

        // Check if the object has a $ref
        if (rootElement.TryGetProperty(SerializationConstants.RefKey, out var refElement))
        {
            return HandleReferenceObject(refElement, resolver);
        }

        reader.Read();

        // Handle object normally.
        if (rootElement.TryGetProperty(SerializationConstants.DiscriminatorKey, out var typeElement))
        {
            var typeName = typeElement.GetString();
            if (typeName == null || !_typeMap.TryGetValue(typeName, out var concreteType))
            {
                throw new JsonException(
                    $"Unknown type discriminator {typeName}. Make sure assemblies are registered properly.");
            }

            // Check if the $component-type exists for generic deserialization
            if (rootElement.TryGetProperty(SerializationConstants.ComponentTypeKey, out var componentTypeElement))
            {
                var componentTypeName = componentTypeElement.GetString();
                if (componentTypeName != null &&
                    _componentTypeMap.TryGetValue(componentTypeName, out var componentType))
                {
                    // Handle deserialization of generic types
                    var typeWithComponent = concreteType.MakeGenericType(componentType);
                    var obj = (TInterface?)JsonSerializer.Deserialize(rootElement.GetRawText(), typeWithComponent,
                        options);
                    return obj;
                }
            }

            // If it's not a generic type, deserialize the object normally
            var obj2 = (TInterface?)JsonSerializer.Deserialize(rootElement.GetRawText(), concreteType, options);
            return obj2;
        }

        // We don't know what the object is, deserialization is not possible.
        throw new JsonException("Unable to determine object type. Make sure assemblies are registered properly.");
    }

    private static TInterface? HandleReferenceObject(JsonElement refElement, CustomReferenceResolver resolver)
    {
        var referenceId = refElement.GetString()!;
        var resolvedObject = resolver.ResolveReference(referenceId);

        // If we try to get a reference that doesn't exist yet, we return a placeholder object.
        if (resolvedObject is UnresolvedReference)
        {
            // Create a placeholder object and set up resolution later
            var placeholder = new object() as TInterface;
            resolver.AddUnresolvedReference(referenceId, obj => placeholder = (TInterface)obj);
            return placeholder;
        }

        return (TInterface)resolvedObject;
    }

    /// <summary>
    /// Writes the object to JSON format.
    /// </summary>
    /// <param name="writer"></param>
    /// <param name="value"></param>
    /// <param name="options"></param>
    public override void Write(Utf8JsonWriter writer, TInterface value, JsonSerializerOptions options)
    {
        var resolver = options.ReferenceHandler?.CreateResolver();

        if (resolver != null)
        {
            var reference = resolver.GetReference(value, out var alreadyExists);
            if (alreadyExists)
            {
                writer.WriteStartObject();
                writer.WriteString(SerializationConstants.RefKey, reference);
                writer.WriteEndObject();
                return;
            }
        }

        writer.WriteStartObject();

        if (value is IId idValue)
        {
            writer.WriteString(SerializationConstants.IdKey, idValue.Id.ToString());
        }

        writer.WriteString(SerializationConstants.DiscriminatorKey, value.GetType().Name);
        if (value.GetType().IsGenericType)
        {
            var genericArguments = value.GetType().GetGenericArguments();
            var genericTypeArgument = genericArguments[0];

            if (typeof(IComponent).IsAssignableFrom(genericTypeArgument))
            {
                writer.WriteString(SerializationConstants.ComponentTypeKey, genericTypeArgument.Name);
            }
        }

        var members = GetJsonMembers(value.GetType())
            .OrderBy(m => m.JsonPropertyOrder);

        foreach (var member in members)
        {
            writer.WritePropertyName(member.JsonPropertyName);
            var memberValue = member.Member switch
            {
                PropertyInfo property => property.GetValue(value),
                FieldInfo field => field.GetValue(value),
                _ => throw new InvalidOperationException("Unsupported member type. ")
            };

            JsonSerializer.Serialize(writer, memberValue, memberValue?.GetType() ?? member.Member.GetType(), options);
        }

        writer.WriteEndObject();
    }

    private IEnumerable<JsonMemberMetadata> GetJsonMembers(Type type)
    {
        var types = type
            .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(f => f.GetCustomAttribute<JsonIncludeAttribute>() != null);
        return type
            .GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(p => p.GetCustomAttribute<JsonIgnoreAttribute>() == null ||
                        p.GetCustomAttribute<JsonIncludeAttribute>() != null)
            .Select(p => new JsonMemberMetadata
            {
                Member = p,
                JsonPropertyName = p.GetCustomAttribute<JsonPropertyNameAttribute>()?.Name ?? p.Name,
                JsonPropertyOrder = p.GetCustomAttribute<JsonPropertyOrderAttribute>()?.Order ?? int.MaxValue
            })
            .Concat(
                type
                    .GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                    .Where(f => f.GetCustomAttribute<JsonIncludeAttribute>() != null)
                    .Select(f => new JsonMemberMetadata
                    {
                        Member = f,
                        JsonPropertyName = f.GetCustomAttribute<JsonPropertyNameAttribute>()?.Name ?? f.Name,
                        JsonPropertyOrder = f.GetCustomAttribute<JsonPropertyOrderAttribute>()?.Order ??
                                            int.MaxValue
                    })
            );
    }

    private record JsonMemberMetadata
    {
        public MemberInfo Member { get; init; }
        public string JsonPropertyName { get; init; }
        public int JsonPropertyOrder { get; init; }
    }
}