using System.Reflection;
using System.Text.Json;
using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Services.Options;
using Microsoft.Extensions.Options;

namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
///     Provides functionality to save/load diagrams
/// </summary>
public class DiagramSerializer : IDiagramSerializer
{
    private readonly List<Assembly> _assemblies = [];
    private readonly JsonSerializerOptions _options;

    /// <summary>
    ///     Initializes a new instance of <see cref="DiagramSerializer"/>
    /// </summary>
    /// <param name="options"></param>
    public DiagramSerializer(IOptions<DiagramSerializerOptions>? options)
    {
        var serializationOptions = options?.Value;
        _options = new JsonSerializerOptions
        {
            WriteIndented = serializationOptions?.JsonSerializerOptions.WriteIndented ?? true,
            PropertyNameCaseInsensitive =
                serializationOptions?.JsonSerializerOptions.PropertyNameCaseInsensitive ?? true,
            ReferenceHandler = serializationOptions?.JsonSerializerOptions.ReferenceHandler ??
                               new CustomReferenceHandler(),
        };
        RegisterAssemblies(serializationOptions);
        RegisterConverters();
    }

    private void RegisterConverters()
    {
        var diagramConverter = new DiagramConverter();
        var nodeConverter = new NodeConverter();
        var layerConverter = new LayerConverter();
        var groupConverter = new GroupConverter();
        var portConverter = new PortConverter();
        var linkConverter = new LinkConverter();
        var listConverter = new ObservableListConverter();
        foreach (var assembly in _assemblies)
        {
            diagramConverter.RegisterTypesFromAssembly(assembly);
            nodeConverter.RegisterTypesFromAssembly(assembly);
            layerConverter.RegisterTypesFromAssembly(assembly);
            groupConverter.RegisterTypesFromAssembly(assembly);
            portConverter.RegisterTypesFromAssembly(assembly);
            linkConverter.RegisterTypesFromAssembly(assembly);
        }

        _options.Converters.Add(listConverter);
        _options.Converters.Add(diagramConverter);
        _options.Converters.Add(layerConverter);
        _options.Converters.Add(nodeConverter);
        _options.Converters.Add(groupConverter);
        _options.Converters.Add(portConverter);
        _options.Converters.Add(linkConverter);
    }

    private void RegisterAssemblies(DiagramSerializerOptions? serializationOptions)
    {
        if (serializationOptions is not null)
        {
            _assemblies.AddRange(serializationOptions.Assemblies);
        }

        var entryAssembly = Assembly.GetEntryAssembly();
        if (entryAssembly is not null && !_assemblies.Contains(entryAssembly))
        {
            _assemblies.Add(entryAssembly);
        }

        if (!_assemblies.Contains(Assembly.GetExecutingAssembly()))
        {
            _assemblies.Add(Assembly.GetExecutingAssembly());
        }
    }

    /// <inheritdoc />
    public TDiagram? LoadFromJson<TDiagram>(string json, JsonSerializerOptions options) where TDiagram : IDiagram
    {
        ResetResolver(options);
        var result = JsonSerializer.Deserialize<TDiagram>(json, options);
        ResetResolver(options);
        return result;
    }

    /// <inheritdoc />
    public string SaveAsJson(IDiagram diagram)
    {
        return SaveAsJson(diagram, _options);
    }

    /// <inheritdoc />
    public string SaveAsJson(IDiagram diagram, JsonSerializerOptions options)
    {
        ResetResolver(options);

        var result = JsonSerializer.Serialize(diagram, _options);
        ResetResolver(options);
        return result;
    }

    private static void ResetResolver(JsonSerializerOptions options)
    {
        if (options.ReferenceHandler?.CreateResolver() is CustomReferenceResolver customResolver)
        {
            customResolver.Reset();
        }
    }

    /// <inheritdoc />
    public T? LoadFromJson<T>(string json) where T : IDiagram
    {
        var result = LoadFromJson<T>(json, _options);
        return result;
    }
}