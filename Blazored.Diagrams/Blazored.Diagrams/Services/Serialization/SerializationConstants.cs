namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
/// Constants used for serialization.
/// </summary>
public static class SerializationConstants
{
    /// <summary>
    /// Key for id metadata.
    /// </summary>
    public const string IdKey = "$id";
    /// <summary>
    /// Key for reference metadata.
    /// </summary>
    public const string RefKey = "$ref";
    /// <summary>
    /// Json key for the object type.
    /// </summary>
    public const string DiscriminatorKey = "object-type";
    /// <summary>
    /// Json key for the component type.
    /// </summary>
    public const string ComponentTypeKey = "component-type";
}