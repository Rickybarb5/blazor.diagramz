﻿using Blazored.Diagrams.Diagrams;
using Blazored.Diagrams.Groups;
using Blazored.Diagrams.Layers;
using Blazored.Diagrams.Links;
using Blazored.Diagrams.Nodes;
using Blazored.Diagrams.Ports;

namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
/// Generic diagram converter
/// </summary>
public class DiagramConverter : PolymorphicJsonConverter<IDiagram>;

/// <summary>
/// Generic layer converter.
/// </summary>
public class LayerConverter : PolymorphicJsonConverter<ILayer>;

/// <summary>
/// Generic node converter.
/// </summary>
public class NodeConverter : PolymorphicJsonConverter<INode>;

/// <summary>
/// Generic group converter.
/// </summary>
public class GroupConverter : PolymorphicJsonConverter<IGroup>;

/// <summary>
/// Generic port converter
/// </summary>
public class PortConverter : PolymorphicJsonConverter<IPort>;

/// <summary>
/// Generic link converter.
/// </summary>
public class LinkConverter : PolymorphicJsonConverter<ILink>;