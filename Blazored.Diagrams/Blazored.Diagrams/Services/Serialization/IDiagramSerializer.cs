using System.Text.Json;
using Blazored.Diagrams.Diagrams;

namespace Blazored.Diagrams.Services.Serialization;

/// <summary>
/// Provides functionality for loading and saving diagrams
/// </summary>
public interface IDiagramSerializer
{
    /// <summary>
    /// Loads a diagram from a json string.
    /// </summary>
    /// <param name="json"></param>
    /// <typeparam name="TDiagram">Diagram type(must implement <see cref="IDiagram"/></typeparam>
    /// <returns></returns>
    TDiagram? LoadFromJson<TDiagram>(string json) where TDiagram : IDiagram;

    /// <summary>
    /// Loads a diagram from a json string with custom options.
    /// </summary>
    /// <param name="json"></param>
    /// <param name="options"></param>
    /// <typeparam name="TDiagram"></typeparam>
    /// <returns></returns>
    TDiagram? LoadFromJson<TDiagram>(string json, JsonSerializerOptions options) where TDiagram : IDiagram;

    /// <summary>
    /// Saves a diagram to Json format.
    /// </summary>
    /// <param name="diagram"></param>
    /// <returns></returns>
    string SaveAsJson(IDiagram diagram);

    /// <summary>
    /// Saves a diagram to Json format using custom json options.
    /// </summary>
    /// <param name="diagram"></param>
    /// <param name="options"></param>
    /// <returns></returns>
    string SaveAsJson(IDiagram diagram, JsonSerializerOptions options);
}