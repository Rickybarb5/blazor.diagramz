using System.Text;
using Blazored.Diagrams.Interfaces.Properties;

namespace Blazored.Diagrams.Services.Styling;

public partial class StyleBuilder
{
    private StringBuilder _stringBuilder = new();

    public string Build<T>(T model, string baseStyle)
    {
        _stringBuilder.Clear();
        if (!string.IsNullOrWhiteSpace(baseStyle))
        {
            _stringBuilder.Append(baseStyle);
        }

        if (model is IVisible { IsVisible: false })
        {
            _stringBuilder.Append("display:none;");
        }
        return _stringBuilder.ToString();
    }
}